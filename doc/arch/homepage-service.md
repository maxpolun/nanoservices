# Homepage service

```yml
status: proposed
```

## Background

Nanoservices can host many apps. There should be a central way to navigate to each app.

## Constraints

* Each app should be a microfrontend registered in `front`.

## Design

`sweet` service is the homepage service. Each app registers

* name -- user facing name of the app
* front_module -- the `front` module_identifier
* permission_url_template -- the url that the homepage service should hit to see if the user has access to the app. The replacement strings {user_uuid} and {subject} can be used. A 2xx response is considered to be allowed. This can be null in which case all users can access.
* data_items_url -- the url to query for data items

The data_items url must have the following interface

`/` <- the url at data_items_url
`/?q=xxx` <- searching for `xxx`
`/?c=abcxyz` <- fetching more items with a cursor `abcxyz` (the cursor is returned in the API response)

The response should be of the form

```json
{
  "cursor": "abcxyz",
  "data_items": [
    {
      "id": "some identifier. Should be unique at least for all of the data_items in this app.",
      "name": "user facing name of the item",
    }
  ]
}
```

The homepage app will then mount the correct front module and pass in the chosen data item
