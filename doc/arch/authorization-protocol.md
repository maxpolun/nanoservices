# Authorization

```yml
status: proposed
```

## Background

Authentication (proving the user is who they claim to be) is defined by the auth protocol, using JWTs.

It is

* Scalable: services don't need to hit a centralized authentication service to verify the identity of the user accessing them
* Secure: Uses industry standard technologies (JWTs) in a secure mode.
* Extensible: More data can be attached to the JWT as needed

We need a solution for authorization that fits those same criteria

### Scalable Authorization

Centralized authorization is hard to scale, so each service should manage it's own authorization. However this creates the possibility of non-standard authorization -- each service implementing it's own authorization in a away each integrator needs to understand. It's proposed to have a standard authorization protocol, that each service imnplements. Some services may have a very simple implementation -- those that do not have complex authorization schemes may not need to store any information in their database, while others may have a complex implementation due to requirements.

### Secure

All requests must be authorized using the standard protocol

### Extensible

Services must be able to support a wide variety of authorization schemes, all using the standard protocol.

## Authorization protocol definition

Authorization is defined by the triple of

`(subject, action, object)`

The subject is of the form `[type]:[id]` where the type is:

* `u` for a user -- in this case the id is their credential uuid
* `a` for an application or service (they are equivalent for authorization purposes) -- in this case the id is their service name (allowing alphanumeric characters + `_`)
* `s` for system/superuser -- for this the id is purely for human readible purposes. All `s` subjects have equal access, but a descriptive id will help debugging. The id for `s` subjects can consist of ascii alphanumeric characters and `_`,

The action is of the form `[service_name]:[permission_string]`.

both the service name and permission string are human-readible strings consisting of only aplhanumeric characters and `_`.

The object is of the form `[type]:[id]` where the type is a unique identifier for the type of the object, and id is a unique identifier for the object. Type can consist of only ascii alphanumeric characters and `_`, ID can consist of ascii alphanumeric characters and `_` or `-` (to allow for uuids in their usual representation) and it will usually be a uuid, but can be any valid string as long as it is unique between objects.

The Authentication JWT will contain the subject. The service receiving the call will verify that the subject can perform the action on the object. If the subject does not have permission to perform the action the service will return a result of the form

```json
{
  "errors": [
    {
      "type": "authentication",
      "subject": "[subject]",
      "action": "[action]",
      "object": "[object]",
      "message": "Some developer facing message explaining why the [subject] cannot perform [action] on [object]
    }
  ]
}
```

This gives consuming applications all the context they need to handle the error. The error can be forwarded onto consuming services, but any user-facing application must filter any forwarded errors of their message.

## Querying for available permissions

It's useful to be able to collect what permissions exist for different services and:

1. display them in a user-facing way (i.e. translated)
2. have a developer-facing description of exactly what these permissions do

Each service will serve a route at `<base_url>/well-known/nano/permissions.json`. This route can be served statically, or dynamically. This file has no authorization requirements. The format of this file is:

```json
{
  "[service_name]/[permission]": {
    "name" {
      "[locale]": "The name for this permission in that locale"
    },
    "description": "A description of what this permission means",
    "documentation_url": "an *optional* url pointing to more in-depth documentation around this permission."
  },
  "a_different_service_name/permission": {
    "#remote": "URL of the permissions.json that this permission is found in. Can be used when this service depends on another service."
  }
}
```

## Querying for subject access

One way to query for subject access is to just try some action and see if it fails. Other times it may be valuable to query if a user has permission before taking action (to hide or disable actions that the user doesn't have access to).

Each service will serve a route at `<base_url>/well-known/nano/query_permission?permission=[permission]&type=[object-type]&id=[object-id]`. The subject is determined by the JWT used to access the route, and the service is also implicit from the service being accessed. If the subject has permission, the route will return a 204 response (success but no body). Otherwise the route will return an error as described above.
