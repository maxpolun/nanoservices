import React from 'react';
import { Table, Empty, Row, Col, Cell } from './Table';
import faker from 'faker'
import { action } from '@storybook/addon-actions';

export default {
  title: 'Table',
  component: Table,
};

const header = <><Col sort='asc' onClick={action('click')}>Name</Col><Col>Email</Col><Col>Verified</Col></>

const fakeUsers = [...Array(100)].map((_ignore, i) => ({
  id: i,
  name: faker.name.firstName() + ' ' + faker.name.lastName(),
  email: faker.internet.email(),
  verified: Math.random() > 0.25
}))

export const EmptyTable = () => <Table header={header}>
  <Empty>There is no content in this table!</Empty>
</Table>

export const TableWithRows = () => <Table header={header}>
  {fakeUsers.map(u => <Row key={u.id}>
    <Cell>{u.name}</Cell>
    <Cell>{u.email}</Cell>
    <Cell>{u.verified ? '✔️' : '❌'}</Cell>
  </Row>)}
</Table>
