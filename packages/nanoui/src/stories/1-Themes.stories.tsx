import React from 'react';
import { ThemeSwatch } from './Theme-swatch';
import { allThemes } from '../themes';

export default {
  title: 'Themes',
  component: ThemeSwatch,
};

export const All = () => (
  <>
    {allThemes.map(theme => <ThemeSwatch theme={theme} />)}
  </>
)
