/** @jsx jsx */
import { Theme } from 'themes';
import { css, jsx } from '@emotion/core';
import { readableColor } from 'polished'

export const ThemeSwatch = ({ theme }: {theme: Theme}) => {
  return <div css={css`
    text-align: center;
    margin: 0 auto;
  `}>
    <h1>{theme.name}</h1>
    <div css={css`width: 200px`}></div>
    {Object.entries(theme.colors).map(([name, val]) => (
      <div css={css`
        display: inline-block;
        margin: 20px;
        width: 100px;
        height: 100px;
        background-color: ${val};
        color: ${readableColor(val)}
      `}>
        {name}
      </div>
    ))}

  </div>
}
