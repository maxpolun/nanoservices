import React from 'react';
import { action } from '@storybook/addon-actions';
import {Button} from './Button';

export default {
  title: 'Button',
  component: Button,
};

export const Text = () => <Button variety='primary' onClick={action('clicked')}>Hello Button</Button>;

export const All = () => <>
  {(['primary', 'secondary', 'default', 'link'] as const).map(v => <>
    <Button variety={v} onClick={action('clicked')}>{v}</Button>
    <br /><br />
  </>)}
</>
