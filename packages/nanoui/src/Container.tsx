/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import { FC } from 'react'
import { Breakpoint, minWidths, breakpoints, queries } from './breakpoints'

export const Container: FC<{ breakpoint?: Breakpoint }> = ({ breakpoint, children }) => {
  let maxWidth = ''
  if (typeof breakpoint !== 'undefined') {
    const nextBP = breakpoints.findIndex(b => b === breakpoint) + 1
    const maxPixels = minWidths[breakpoints[nextBP]]
    if (maxPixels) maxWidth = `${queries[breakpoint]} { max-width: ${maxPixels}px;}`
  }
  return <div css={css`
    width: 100%;
    padding: 10px 20px;
    ${maxWidth};
  `}>
    {children}
  </div>
}
