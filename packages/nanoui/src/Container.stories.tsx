import React, { FC } from 'react';
import { Container } from './Container';
import { Breakpoint, breakpoints } from './breakpoints';
import { Global } from '@emotion/core';

export default {
  title: 'Container',
  component: Container,
};

const Example: FC<{ bp?: Breakpoint }> = ({ bp, children }) =>
  <>
    <Global styles={`
      /* This is pretty hacky, but it's the simplest way to add outlines for the storybook:-) */
      [class$=Container] {
        outline: 1px dashed red;
      }
    `} />
    <Container breakpoint={bp}>
      <div>{children}</div>
    </Container>
  </>

export const Normal = () => <Example>normal</Example>

export const Sized = () => <>{
  breakpoints.map(bp => <Example bp={bp}>{Breakpoint[bp]}</Example>)
}</>
