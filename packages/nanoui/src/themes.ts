export interface Theme {
  name: string
  colors: {
    primary: string,
    secondary: string,
    background: string,
    foreground: string,
    text: string,
    textInverse: string,
    link: string,
    seperator: string
  }
}

export const light: Theme = {
  name: 'Light',
  colors: {
    primary: 'rgb(22, 244, 208)',
    secondary: 'rgb(202, 243, 255)',
    background: '#fff',
    foreground: 'rgba(183, 79, 111, 1)',
    text: '#333',
    textInverse: '#ddd',
    link: 'rgb(87, 113, 221)',
    seperator: '#ccc'
  }
}

export const dark: Theme = {
  name: 'Dark',
  colors: {
    primary: '#6fffe9',
    secondary: '#5bc0be',
    background: '#0b132b',
    foreground: '#4c5982',
    text: '#ddd',
    textInverse: '#333',
    link: 'rgba(143, 159, 225, 1)',
    seperator: '#333'
  }
}

export const allThemes = [light, dark];
