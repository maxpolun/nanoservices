/** @jsx jsx */
import React, { memo } from 'react'
import { useTheme } from 'emotion-theming'
import { css, jsx } from '@emotion/core'
import { Theme } from 'themes'
import { readableColor, darken, parseToRgb, mix, transparentize } from 'polished'

interface ButtonProps {
  variety: 'primary' | 'secondary' | 'default' | 'link',
  elem?: string
  [props: string]: any
}

const isTranparent = (color: string) => {
  if (color === 'transparent') return true
  const parsed = parseToRgb(color)
  if ('alpha' in parsed) return parsed.alpha === 1
  return false
}

export const Button: React.FC<ButtonProps> = memo(({ variety = 'default', children, elem, ...props }) => {
  const theme: Theme = useTheme()
  const bgColor = ({
    primary: theme.colors.primary,
    secondary: theme.colors.secondary,
    default: theme.colors.background,
    link: 'transparent'
  } as const)[variety]
  const borderColor = ({
    primary: theme.colors.primary,
    secondary: theme.colors.secondary,
    default: theme.colors.foreground,
    link: 'transparent'
  } as const)[variety]
  const textColor = variety === 'link' ? theme.colors.link : readableColor(bgColor, theme.colors.text, theme.colors.textInverse, true)
  const hover = (variety !== 'link' ) ? darken(0.1, bgColor) : bgColor
  const hoverBorder = (variety === 'primary' || variety === 'secondary') ? hover : borderColor
  const focus = (variety !== 'link') ? darken(0.14, bgColor) : bgColor
  const focusBorder = (variety === 'primary' || variety === 'secondary') ? focus : borderColor

  const elemType = elem || (variety === 'link' ? 'a' : 'button')

  return jsx(elemType, {
      css: css`
        background-color: ${bgColor};
        border: 1px solid ${borderColor};
        border-radius: 3px;
        padding: 10px 20px;
        font-size: 18px;
        color: ${textColor};
        ${variety !== 'link' && `box-shadow: 0 1px 3px ${transparentize(0.5, mix(0.15, textColor, borderColor))}`};
        &:hover {
          background-color: ${hover};
          border-color: ${hoverBorder};
          ${variety === 'link' && 'text-decoration: underline'};
        }
        &:active {
          background-color: ${focus};
          border-color: ${focusBorder};
          ${variety === 'link' && 'text-decoration: underline'};
        }
        &:focus {
          outline: 1px solid ${theme.colors.foreground};
        }
      `,
      children,
      ...props
    })
})

export default Button
