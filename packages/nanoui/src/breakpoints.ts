export enum Breakpoint {
  NarrowMobile,
  WideMobile,
  Tablet,
  Laptop,
  Desktop
}

export const xs = Breakpoint.NarrowMobile
export const sm = Breakpoint.WideMobile
export const md = Breakpoint.Tablet
export const lg = Breakpoint.Laptop
export const xl = Breakpoint.Desktop

export const minWidths = {
  [Breakpoint.NarrowMobile]: 0,
  [Breakpoint.WideMobile]: 576,
  [Breakpoint.Tablet]: 780,
  [Breakpoint.Laptop]: 1080,
  [Breakpoint.Desktop]: 1440
}

export const breakpoints = [xs, sm, md, lg, xl]
export const queries = breakpoints.map (bp => `@media (min-width: ${minWidths[bp]}px)`)
