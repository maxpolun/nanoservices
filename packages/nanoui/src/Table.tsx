/** @jsx jsx */
import { css, jsx } from '@emotion/core'
import { FC, Fragment } from 'react'
import { useTheme } from 'emotion-theming'
import { Theme } from './themes'
import { readableColor, hideVisually, triangle } from 'polished'
import { Button } from './Button'

export const Table: FC<{ header: JSX.Element }> = ({ children, header }) => {
  const theme: Theme = useTheme()
  const headerColor = theme.colors.foreground
  const headerText = readableColor(headerColor, theme.colors.text, theme.colors.textInverse, true)
  return <table css={css`
    width: 100%;
    border: 1px solid ${theme.colors.seperator};
    & > thead > tr {
      border: 1px solid ${theme.colors.seperator};
      background-color: ${headerColor};
      color: ${headerText};
    }
    & > tbody > tr { border: 1px solid ${theme.colors.seperator};}
    & > thead > tr > th { padding: 10px 20px; text-align: left;}
    & > thead > tr > th > a { color: ${headerText}; cursor: pointer; text-decoration: underline; }
    & > tbody > tr > td { padding: 10px 20px;}
  `}>
    <thead><tr>{header}</tr></thead>
    <tbody>{children}</tbody>
  </table>
}
type SortDir = 'asc' | 'desc'

export const SortArrow: FC<{ dir: SortDir }> = ({ dir }) => {
  const theme: Theme = useTheme()
  const headerText = readableColor(theme.colors.foreground, theme.colors.text, theme.colors.textInverse, true)
  const triangleCSS = triangle({
    width: '12px',
    height: '12px',
    pointingDirection: dir === 'asc' ? 'top' : 'bottom',
    foregroundColor: headerText
  })
  return <span
    css={css`
      ${triangleCSS};
      display: inline-block;
    `}
  >
    <span css={css`${hideVisually()}`}>{dir === 'asc' ? 'Sort Ascending' : 'Sort Decending'}</span>
  </span>
}
export const Col: FC<{ sort?: SortDir, onClick?: () => void }> = ({ children, sort, onClick }) => {
  const arrow = sort && <SortArrow dir={sort} />
  return <th>
    {onClick ? <a onClick={onClick}>{children}</a> : children}
    {' '}
    {arrow}
  </th>
}
export const Row: FC<{}> = ({ children }) => <tr>{children}</tr>
export const Cell: FC<{}> = ({ children }) => <td>{children}</td>
export const Empty: FC<{ columns?: number }> = ({ columns = 999, children }) => <tr>
  <td
    colSpan={columns}
    css={css`
      text-align: center;
      vertical-align: middle;
      height: 200px;
    `}
  >
    <div>{children}</div>
  </td>
</tr>
