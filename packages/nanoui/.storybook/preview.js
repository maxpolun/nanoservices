import { addDecorator, addParameters } from '@storybook/react'
import { withA11y } from '@storybook/addon-a11y'
import { DocsPage } from 'storybook-addon-deps/blocks'
import { withInfo } from '@storybook/addon-info'
import { withThemesProvider } from 'storybook-addon-emotion-theme';
import { allThemes } from '../src/themes'
import { GlobalStyles } from '../src/Global'
import React from 'react'

addParameters({
  options: {
    showRoots: true
  },
  docs: { page: DocsPage },
  dependencies: {
    // display only dependencies/dependents that have a story in storybook
    // by default this is false
    withStoriesOnly: true,

    // completely hide a dependency/dependents block if it has no elements
    // by default this is false
    hideEmpty: true
  }
})

addDecorator(withInfo)
addDecorator(withA11y)
addDecorator(story => <>
  <GlobalStyles />
  <div style={{padding: 20}}>{story()}</div>
</>)
addDecorator(withThemesProvider(allThemes))
