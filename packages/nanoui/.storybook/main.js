const path = require('path')
const ForkTsCheckerWebpackPlugin = require('fork-ts-checker-webpack-plugin')

module.exports = {
  stories: ['../src/**/*.stories.tsx'],
  presets: [{
    name: 'storybook-addon-deps/preset',
    options: {
      exclude: /^@babel/
    }
  }],
  addons: [
    '@storybook/addon-actions',
    '@storybook/addon-links',
    '@storybook/addon-docs',
    '@storybook/addon-a11y',
    '@storybook/addon-storysource',
    'storybook-addon-emotion-theme/dist/register',
    '@storybook/addon-viewport/register'
  ],
  webpackFinal: async config => {
    config.module.rules.push({
      test: /\.(ts|tsx)$/,
      use: [
        {
          loader: require.resolve('babel-loader'),
          options: {
            presets: ['@babel/preset-react', '@babel/preset-typescript']
          },
        },
        {
          loader: require.resolve('react-docgen-typescript-loader'),
          options: {
            tsconfigPath: path.join(__dirname, "../tsconfig.json"),
          },
        }
      ]
    })
    config.resolve.extensions.push('.ts', '.tsx')
    config.plugins.push(new ForkTsCheckerWebpackPlugin())
    return config
  }
}
