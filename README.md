# Nanoservices

[![pipeline status](https://gitlab.com/maxpolun/nanoservices/badges/master/pipeline.svg)](https://gitlab.com/maxpolun/nanoservices/commits/master)

This repo is a collection of tiny services and UIs -- it's an experiment to implement microservices for my own use, as well as micro-frontends.

Current Services:

* auth -- authentication
* disco -- service discovery
* users -- user profiles
* front -- microfrontend registrar
* sweet -- homepage service

Current frontends

* loader -- loads microfrontends
* profile -- user profile management
