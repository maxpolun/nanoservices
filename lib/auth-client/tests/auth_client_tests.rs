use auth_client::{
  keys::{InMemoryKeyStorage, KeyFetcher},
  request::{AccessTokenParams, AuthorizedRequest},
  revocation::NetworkRequestRevocationCheck,
  AuthClient, RealAuthClient,
};
use chrono::Duration;
use support::{fake_server, make_key};
use url::Url;

mod support;

#[actix_rt::test]
async fn can_authorize_request() {
  tracing_subscriber::fmt::init();
  let (private_key, public_key) = make_key();
  let server = fake_server("test_service", "test_key", &public_key);
  let mut client = RealAuthClient::new(
    KeyFetcher {
      auth_service_base_url: Url::parse(&server.url("/")).unwrap(),
    },
    InMemoryKeyStorage::new(),
    NetworkRequestRevocationCheck { url: server.url("/") },
  );
  let req = AuthorizedRequest::new_raw(
    &private_key,
    "test_key".to_owned(),
    "test_service".to_owned(),
    server.addr().to_string(),
  );

  let jwt = req
    .generate_jwt(AccessTokenParams {
      duration: Duration::minutes(15),
    })
    .unwrap();

  client.validate_jwt(&jwt).await.unwrap();
}
