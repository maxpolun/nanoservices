use std::sync::Arc;

use actix_http::http::StatusCode;
use actix_web::{
  test::{self, TestServer},
  web, App, HttpResponse,
};
use base64::encode as encodeb64;
use ring::{
  rand,
  signature::{self, KeyPair},
};
use tracing::debug;

pub fn make_key() -> (Vec<u8>, Vec<u8>) {
  let rng = rand::SystemRandom::new();
  let key_bytes = signature::EcdsaKeyPair::generate_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, &rng).unwrap();
  let pair =
    signature::EcdsaKeyPair::from_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, key_bytes.as_ref()).unwrap();
  (key_bytes.as_ref().to_owned(), pair.public_key().as_ref().to_owned())
}

// pub fn make_jwt(key_id: &str, key: &[u8], access_token: AccessToken) -> String {
//     let mut header = Header::new(Algorithm::ES256);
//     header.kid = Some(key_id.to_owned());

//     encode(&header, &access_token, &EncodingKey::from_ec_der(&key)).unwrap()
// }

pub fn fake_server(service_name: &'static str, key_id: &'static str, pubkey: &[u8]) -> TestServer {
  let enc_pubkey = Arc::new(format!("BEGIN PUBLIC KEY\n{}\nEND PUBLIC KEY", encodeb64(&pubkey)));
  test::start(move || {
    let pubkey_clone = enc_pubkey.clone();
    App::new().service(web::resource("/services/{servicename}/keys/{keyid}").to(
      move |params: web::Path<(String, String)>| {
        let (servicename, keyid) = params.into_inner();
        debug!("got request for {}::{}", servicename, keyid);
        if servicename != service_name || keyid != key_id {
          return HttpResponse::Ok().status(StatusCode::NOT_FOUND).finish();
        }
        let json = serde_json::json!(
            {
                "key_id": key_id.clone(),
                "public_key": pubkey_clone.as_str().to_owned(),
                "service_name": service_name.clone()
            }
        );
        HttpResponse::Ok()
          .header(actix_web::http::header::CONTENT_TYPE, "application/json")
          .body(json)
      },
    ))
  })
}
