use chrono::{Duration, Utc};
use futures::{future::LocalBoxFuture, FutureExt};
use keys::{KeyError, ServiceKey};
use std::convert::TryFrom;
use thiserror::Error;
use tracing::{debug, info};
use url::Url;
use uuid::Uuid;

pub mod generate;
pub mod keys;
pub mod request;
pub mod revocation;
pub mod subject;
#[cfg(test)]
pub mod test_support;
pub mod token;
pub mod validate;

pub use subject::Subject;
pub use token::{AccessToken, Timestamp};

pub fn default_token_duration() -> Duration {
  Duration::minutes(15)
}

/// A simple auth client trait -- only supporting the basics needed for services to validate JWTs
pub trait AuthClient: std::fmt::Debug {
  /// validate jwt, asynchronously fetching new keys from the auth server if needed
  /// This only mutates internal state, but will return an error if unable to fetch keys
  fn validate_jwt(&mut self, jwt: &str) -> LocalBoxFuture<Result<AccessToken, AuthError>>;
}

/// All of the possible authentication errors
#[derive(Debug, PartialEq, Error)]
pub enum AuthError {
  #[error(transparent)]
  Jwt(#[from] JwtError),
  #[error(transparent)]
  Key(#[from] KeyError),
}

/// The errors that can happen when parsing a JWT
#[derive(Debug, PartialEq, Error)]
pub enum JwtError {
  #[error("JWT is malformed")]
  ParsingError,
  #[error("JWT is expired (the `exp` claim was in the past)")]
  Expired,
  #[error("JWT should not be ready yet (`nbf` time is in the future)")]
  BeforeValidity,
  #[error("JWT does not have a key id (`kid` claim)")]
  MissingKey,
  #[error("JWT has invalid `sub` claim")]
  CredentialError,
  #[error("JWT key id (the `kid` claim) for {service_name}::{key_id} is not known to the client -- you can try fetching new keys")]
  UnknownKey { service_name: String, key_id: String },
  #[error("User or System subject for a token signed by a key from a service other then `auth`")]
  InvalidSubject { subject: Subject, service_name: String },
  #[error("App subject token, signed by a different service")]
  ImpersonatedApp { subject: Subject, service_name: String },
  #[error("Token needs to be checked against revocation list")]
  NeedsRevocationCheck(AccessToken),
  #[error("Token has been revoked")]
  Revoked(AccessToken),
  #[error("JWT is invalid for an unknown reason")]
  Invalid,
}

impl From<jsonwebtoken::errors::Error> for JwtError {
  fn from(err: jsonwebtoken::errors::Error) -> JwtError {
    use jsonwebtoken::errors::ErrorKind;
    match err.kind() {
      ErrorKind::InvalidToken | ErrorKind::Json(_) | ErrorKind::Utf8(_) | ErrorKind::Base64(_) => {
        JwtError::ParsingError
      }
      ErrorKind::ExpiredSignature => JwtError::Expired,
      ErrorKind::ImmatureSignature => JwtError::BeforeValidity,
      _ => JwtError::Invalid,
    }
  }
}

/// The real auth client -- use this in production
#[derive(Debug)]
pub struct RealAuthClient<KeyStor: keys::KeyStorage, RevCheck: revocation::RevocationCheck> {
  /// the URL of the auth server
  key_fetch: keys::KeyFetcher,
  keys: KeyStor,
  rev: RevCheck,
}

fn decode_pubkey(encoded: String) -> Result<Vec<u8>, KeyError> {
  let mut buf = String::new();
  for line in encoded.lines() {
    if !(line.starts_with("BEGIN PUBLIC KEY") || line.starts_with("END PUBLIC KEY")) {
      buf += line.trim()
    }
  }
  base64::decode(&buf).map_err(|_| KeyError::ParseError)
}

impl<KeyStor: keys::KeyStorage, RevCheck: revocation::RevocationCheck> RealAuthClient<KeyStor, RevCheck> {
  /// Create a new RealAuthClient from the base URL
  pub fn new(key_fetch: keys::KeyFetcher, keys: KeyStor, rev: RevCheck) -> Self {
    Self { key_fetch, keys, rev }
  }

  async fn internal_validate_jwt(&mut self, jwt: String) -> Result<AccessToken, AuthError> {
    match validate::validate_token(&self.keys, &jwt) {
      Ok(tok) => {
        debug!("successfully parsed jwt");
        Ok(tok)
      }
      Err(JwtError::NeedsRevocationCheck(token)) => {
        debug!("checking token {} for revocation", token.jti);
        if self.rev.is_token_revoked(&jwt).await {
          Err(AuthError::Jwt(JwtError::Revoked(token)))
        } else {
          Ok(token)
        }
      }
      Err(JwtError::UnknownKey { service_name, key_id }) => {
        debug!("got unknown key once, fetching new keys");
        let key = self.key_fetch.fetch_key(&service_name, &key_id).await?;
        self.keys.put(ServiceKey {
          service_name,
          key_id,
          public_key: decode_pubkey(key.public_key)?,
        })?;
        debug!("successfully fetched new keys");
        match validate::validate_token(&self.keys, &jwt) {
          Err(JwtError::NeedsRevocationCheck(token)) => {
            debug!("checking token {} for revocation", token.jti);
            if self.rev.is_token_revoked(&jwt).await {
              Err(AuthError::Jwt(JwtError::Revoked(token)))
            } else {
              Ok(token)
            }
          }
          res @ _ => {
            debug!("result of validating token after fetching keys: {:?}", res);
            Ok(res?)
          }
        }
      }
      Err(e) => {
        debug!("error parsing jwt: {}", e);
        Err(AuthError::Jwt(e))
      }
    }
  }
}

impl<KeyStor: keys::KeyStorage, RevCheck: revocation::RevocationCheck> AuthClient
  for RealAuthClient<KeyStor, RevCheck>
{
  fn validate_jwt(&mut self, jwt: &str) -> LocalBoxFuture<Result<AccessToken, AuthError>> {
    self.internal_validate_jwt(jwt.to_owned()).boxed_local()
  }
}

/// This is a Fake auth client, suitable for tests and development
/// it hardcodes a certain credential, and always succeeds.
/// For tests that want to test actual jwt validation, use the real client
/// and a fake key server
#[derive(Debug)]
pub struct FakeAuthClient {
  pub subject: Subject,
}

impl AuthClient for FakeAuthClient {
  fn validate_jwt(&mut self, _jwt: &str) -> LocalBoxFuture<Result<AccessToken, AuthError>> {
    tracing::debug!("using mock credentials");
    futures::future::ok(AccessToken {
      iss: "https://mock-service.example.com".to_owned(),
      sub: self.subject.clone(),
      exp: Utc::now() + Duration::hours(1),
      nbf: Utc::now(),
      iat: Utc::now(),
      jti: Uuid::new_v4(),
    })
    .boxed_local()
  }
}

// Box<dyn Trait> doesn't implicitly impl Trait, so add this impl
impl AuthClient for Box<dyn AuthClient> {
  fn validate_jwt(&mut self, jwt: &str) -> LocalBoxFuture<Result<AccessToken, AuthError>> {
    (**self).validate_jwt(jwt)
  }
}

pub fn get_auth_client() -> Box<dyn AuthClient> {
  match std::env::var("NANO_AUTH_MOCK_SUBJECT") {
    Err(_) => {
      let auth_url = Url::parse(&std::env::var("AUTH_URL").expect("AUTH_URL")).expect("invalid auth url");
      info!("not mocking authentication, using AUTH_URL = {}", auth_url);
      Box::new(RealAuthClient::new(
        keys::KeyFetcher {
          auth_service_base_url: auth_url.clone(),
        },
        keys::InMemoryKeyStorage::new(),
        revocation::NetworkRequestRevocationCheck {
          url: auth_url.join("/access_tokens/validate").unwrap().to_string(),
        },
      ))
    }
    Ok(sub) => {
      info!("mocking authentication, using subject = {}", sub);
      Box::new(FakeAuthClient {
        subject: Subject::try_from(&*sub).expect("bad format on subject"),
      })
    }
  }
}
