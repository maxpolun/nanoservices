use std::fmt::Debug;

use awc::Client;
use futures::future::LocalBoxFuture;

/// A trait for checking if tokens are on a revocation list.
pub trait RevocationCheck: Debug {
  /// is the access_token jwt revoked? This may be async so it happens in a promise.
  fn is_token_revoked(&self, token: &str) -> LocalBoxFuture<bool>;
}

/// This RevocationCheck impl always checks against the central auth server
#[derive(Debug)]
pub struct NetworkRequestRevocationCheck {
  /// url of the auth server. Should be <auth base url>/access_tokens/validate
  pub url: String,
}

impl RevocationCheck for NetworkRequestRevocationCheck {
  fn is_token_revoked(&self, token: &str) -> LocalBoxFuture<bool> {
    let url = self.url.clone();
    let token_copy = token.to_owned();
    Box::pin(async {
      !(Client::new()
        .post(url)
        .send_body(token_copy)
        .await
        .unwrap()
        .status()
        .is_success())
    })
  }
}
