use std::fmt::Debug;

use chrono::Utc;
use jsonwebtoken::{encode, Algorithm, EncodingKey, Header};
use uuid::Uuid;

use crate::{AccessToken, Subject};

pub struct AuthorizedRequest {
  private_key: EncodingKey,
  key_id: String,
  service_name: String,
  hostname: String,
  subject: Subject,
}

pub struct AccessTokenParams {
  pub duration: chrono::Duration,
}

impl Debug for AuthorizedRequest {
  // custom impl to hide the private key so we don't accidentally log it
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.debug_struct("AuthorizedRequest")
      .field("private_key", &"[private key hidden]")
      .field("key_id", &self.key_id)
      .field("service_name", &self.service_name)
      .field("hostname", &self.hostname)
      .field("subject", &self.subject)
      .finish()
  }
}

impl Default for AuthorizedRequest {
  fn default() -> Self {
    Self::new_from_env_var()
  }
}

impl AuthorizedRequest {
  pub fn new(
    key: &str,
    key_id: String,
    service_name: String,
    hostname: String,
  ) -> Result<Self, jsonwebtoken::errors::Error> {
    let subject = Subject::Application(service_name.clone());
    Ok(Self {
      private_key: EncodingKey::from_ec_pem(key.as_bytes())?,
      key_id,
      service_name,
      hostname,
      subject,
    })
  }

  pub fn new_raw(key: &[u8], key_id: String, service_name: String, hostname: String) -> Self {
    let subject = Subject::Application(service_name.clone());
    Self {
      private_key: EncodingKey::from_ec_der(key),
      key_id,
      service_name,
      hostname,
      subject,
    }
  }

  pub fn new_from_env_var() -> Self {
    Self::new(
      &std::env::var("AUTH_PRIVATE_KEY").expect("AUTH_PRIVATE_KEY"),
      std::env::var("AUTH_KEY_ID").expect("AUTH_KEY_ID"),
      std::env::var("SERVICE_NAME").expect("SERVICE_NAME"),
      std::env::var("HOSTNAME").expect("HOSTNAME"),
    )
    .expect("Invalid key")
  }

  pub fn new_for_test(key: &[u8], subject: Subject) -> Self {
    let service_name = if let Subject::Application(name) = &subject {
      name.clone()
    } else {
      "auth".to_owned()
    };
    Self {
      private_key: EncodingKey::from_ec_der(key),
      key_id: "test_key".to_owned(),
      service_name,
      hostname: "http://example.com".to_owned(),
      subject: subject,
    }
  }

  pub fn generate_jwt(&self, token_params: AccessTokenParams) -> Result<String, jsonwebtoken::errors::Error> {
    let now = Utc::now();
    let access_token = AccessToken {
      iss: self.hostname.clone(),
      sub: Subject::Application(self.service_name.clone()),
      exp: now + token_params.duration,
      nbf: now,
      iat: now,
      jti: Uuid::new_v4(),
    };
    let mut header = Header::new(Algorithm::ES256);
    header.kid = Some(format!("{}::{}", self.service_name, self.key_id));
    encode(&header, &access_token, &self.private_key)
  }

  pub fn get_awc_client(&self, token_params: AccessTokenParams) -> Result<awc::Client, jsonwebtoken::errors::Error> {
    Ok(
      awc::Client::build()
        .bearer_auth(self.generate_jwt(token_params)?)
        .finish(),
    )
  }
}
