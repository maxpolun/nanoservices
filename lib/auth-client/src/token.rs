use crate::Subject;
use chrono::serde::ts_seconds;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

/// JWT timestamp (seconds since the epoch in UTC timezone)
pub type Timestamp = chrono::DateTime<chrono::Utc>;
/// All of the optional claims of the JWT.
/// all names of fields come from the jwt standard
#[derive(Debug, Serialize, Deserialize, PartialEq, Eq)]
pub struct AccessToken {
  /// issuer -- hostname
  pub iss: String,
  /// subject -- the credential uuid
  pub sub: Subject,
  /// expiration timestamp
  #[serde(with = "ts_seconds")]
  pub exp: Timestamp,
  /// "not before" -- the token is invalid before this date
  #[serde(with = "ts_seconds")]
  pub nbf: Timestamp,
  /// issued at
  #[serde(with = "ts_seconds")]
  pub iat: Timestamp,
  /// jwt unique id
  pub jti: Uuid,
}
