use std::env;

use chrono::{Duration, Utc};
use jsonwebtoken::{encode, Algorithm, EncodingKey, Header};
use tracing::warn;
use uuid::Uuid;

use crate::{default_token_duration, AccessToken, Subject};

/// All of the information for the service to generate tokens.
pub struct TokenGenerator {
  /// the name this service is registered under in `auth`
  pub service_name: String,
  /// The DER encoded private signing key currently being used for this service
  pub private_key: Vec<u8>,
  /// the key id for the signing key
  pub key_identifier: String,
}

fn decode_privkey(encoded: String) -> Option<Vec<u8>> {
  let mut buf = String::new();
  for line in encoded.lines() {
    if !(line.contains("BEGIN PRIVATE KEY") || line.contains("END PRIVATE KEY")) {
      buf += line.trim()
    }
  }
  base64::decode(&buf).ok()
}

impl TokenGenerator {
  pub fn from_env() -> Self {
    let service_name = env::var("SERVICE_NAME").expect("SERVICE_NAME");
    let private_key = decode_privkey(env::var("PRIVATE_KEY").expect("PRIVATE_KEY")).expect("invalid private key");
    let key_identifier = env::var("PRIVATE_KEY_ID").expect("PRIVATE_KEY_ID");
    Self {
      service_name,
      private_key,
      key_identifier,
    }
  }
}

/// TokenConfig defines theparameters you can specify when generating access tokens
/// All params are optional, with the defaults tuned to an application generating tokens for it's own use
#[derive(Debug, Default)]
pub struct TokenConfig {
  /// Lifetime of the token -- used to set expiration time. If duration > 15 minutes the token needs to be
  /// checked against the revokation list
  pub duration: Option<Duration>,
  /// The subject of the access token
  pub subject: Option<Subject>,
}

pub fn auth_service_name() -> String {
  std::env::var("AUTH_SERVICE_NAME").unwrap_or("auth".to_owned())
}

impl TokenConfig {
  pub fn into_access_token(self, generator: &TokenGenerator) -> AccessToken {
    let duration = self.duration.unwrap_or(default_token_duration());
    let subject = self
      .subject
      .unwrap_or_else(|| Subject::Application(generator.service_name.clone()));
    let now = Utc::now();
    let exp = now + duration;
    if generator.service_name != auth_service_name() && subject != Subject::Application(generator.service_name.clone())
    {
      warn!("An application other than `{}` can only generate application tokens, this token is for {:?} and will be ignored by other services", auth_service_name(), subject)
    }
    AccessToken {
      iss: std::env::var("HOSTNAME").expect("HOSTNAME"),
      sub: subject,
      exp,
      nbf: now,
      iat: now,
      jti: Uuid::new_v4(),
    }
  }
}

impl TokenGenerator {
  pub fn generate_token(&self, config: TokenConfig) -> Result<String, jsonwebtoken::errors::Error> {
    let mut header = Header::new(Algorithm::ES256);
    header.kid = Some(format!("{}::{}", self.service_name, self.key_identifier));
    encode(
      &header,
      &config.into_access_token(&self),
      &EncodingKey::from_ec_der(&self.private_key),
    )
  }

  pub fn generate_token_default(&self) -> Result<String, jsonwebtoken::errors::Error> {
    self.generate_token(TokenConfig::default())
  }
}
