use jsonwebtoken::{encode, Algorithm, EncodingKey, Header};
use ring::{
  rand,
  signature::{self, KeyPair},
};

use crate::AccessToken;

pub fn make_key() -> (Vec<u8>, Vec<u8>) {
  let rng = rand::SystemRandom::new();
  let key_bytes = signature::EcdsaKeyPair::generate_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, &rng).unwrap();
  let pair =
    signature::EcdsaKeyPair::from_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, key_bytes.as_ref()).unwrap();
  (key_bytes.as_ref().to_owned(), pair.public_key().as_ref().to_owned())
}

pub fn make_jwt(key_id: &str, key: &[u8], access_token: AccessToken) -> String {
  let mut header = Header::new(Algorithm::ES256);
  header.kid = Some(key_id.to_owned());

  encode(&header, &access_token, &EncodingKey::from_ec_der(&key)).unwrap()
}
