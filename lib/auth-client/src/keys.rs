use awc::Client;
use chrono::{Duration, Utc};
use serde::Deserialize;
use std::{cell::RefCell, collections::HashMap, fmt::Debug};
use thiserror::Error;
use tracing::{debug, info, log::trace};
use url::Url;

use crate::Timestamp;

/// The errors that can happen when fetching keys from the auth server
#[derive(Debug, PartialEq, Error)]
pub enum KeyError {
  #[error("unable to fetch the keys from the network.")]
  FetchError,
  #[error("Unable to parse the response from the server")]
  ParseError,
  #[error("Unable to fetch/save the key to local storage")]
  StorageError,
}

impl From<url::ParseError> for KeyError {
  fn from(_: url::ParseError) -> Self {
    KeyError::FetchError
  }
}

impl From<awc::error::JsonPayloadError> for KeyError {
  fn from(_: awc::error::JsonPayloadError) -> Self {
    KeyError::ParseError
  }
}

impl From<awc::error::SendRequestError> for KeyError {
  fn from(_: awc::error::SendRequestError) -> Self {
    KeyError::FetchError
  }
}

#[derive(Debug, Deserialize)]
pub struct Key {
  pub key_id: String,
  pub public_key: String,
}

#[derive(Debug, Deserialize)]
pub struct ServiceKey {
  pub key_id: String,
  pub service_name: String,
  pub public_key: Vec<u8>,
}

#[derive(Debug)]
pub struct KeyFetcher {
  pub auth_service_base_url: Url,
}

impl KeyFetcher {
  pub async fn fetch_key(&self, service_name: &str, key_id: &str) -> Result<Key, KeyError> {
    let client = Client::default();
    let mut path = self.auth_service_base_url.clone();
    path
      .path_segments_mut()
      .unwrap()
      .push("services")
      .push(service_name)
      .push("keys")
      .push(key_id);
    debug!("fetching key {} for service {}: {}", key_id, service_name, path);
    let mut response = client.get(path.as_str()).send().await?;
    trace!(
      "got status when fetching key {}::{}: {}",
      service_name,
      key_id,
      response.status()
    );
    let key = response.json::<Key>().await;
    trace!("got key {:?}", key);
    Ok(key?)
  }
  pub async fn fetch_all_keys(&self, service_name: &str) -> Result<Vec<Key>, KeyError> {
    let client = Client::default();
    let mut path = self.auth_service_base_url.clone();
    path
      .path_segments_mut()
      .unwrap()
      .push("services")
      .push(service_name)
      .push("keys");
    info!("fetching keys for service {}: {}", service_name, path);
    Ok(client.get(path.as_str()).send().await?.json::<Vec<Key>>().await?)
  }
}

pub trait KeyStorage: Debug {
  fn get(&self, key_id: &str, service_name: &str) -> Result<Option<ServiceKey>, KeyError>;
  fn put(&self, key: ServiceKey) -> Result<(), KeyError>;
}

#[derive(Debug)]
struct TimestampedKey {
  expires_at: Timestamp,
  pubkey: Vec<u8>,
}

#[derive(Debug, Hash, Eq, PartialEq)]
struct InMemoryKeyStorageHashkey {
  service_name: String,
  key_id: String,
}

/// An in memory key storage. Not as efficient as an in-memory key storage could be (does lots of copying) to keep the same
/// interface as a cache server (e.g. redis) implementation
pub struct InMemoryKeyStorage<Timesource: Fn() -> Timestamp> {
  keys: RefCell<HashMap<InMemoryKeyStorageHashkey, TimestampedKey>>,
  timesource: Timesource,
}

impl<Timesource: Fn() -> Timestamp> Debug for InMemoryKeyStorage<Timesource> {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.debug_struct("InMemoryKeyStorage").field("keys", &self.keys).finish()
  }
}

impl InMemoryKeyStorage<fn() -> Timestamp> {
  pub fn new() -> Self {
    Self::new_with_timesource(Utc::now)
  }
}

impl<Timesource: Fn() -> Timestamp> InMemoryKeyStorage<Timesource> {
  pub fn new_with_timesource(timesource: Timesource) -> Self {
    Self {
      keys: RefCell::new(HashMap::new()),
      timesource,
    }
  }
}

impl<Timesource: Fn() -> Timestamp> KeyStorage for InMemoryKeyStorage<Timesource> {
  fn get(&self, key_id: &str, service_name: &str) -> Result<Option<ServiceKey>, KeyError> {
    let hashkey = InMemoryKeyStorageHashkey {
      service_name: service_name.to_owned(),
      key_id: key_id.to_owned(),
    };
    let keys = self.keys.borrow();
    let pubkey = keys.get(&hashkey);
    if let Some(TimestampedKey {
      pubkey: public_key,
      expires_at,
    }) = pubkey
    {
      if expires_at < &(self.timesource)() {
        return Ok(None);
      }
      Ok(Some(ServiceKey {
        key_id: hashkey.key_id,
        service_name: hashkey.service_name,
        public_key: public_key.clone(),
      }))
    } else {
      Ok(None)
    }
  }

  fn put(&self, key: ServiceKey) -> Result<(), KeyError> {
    let hashkey = InMemoryKeyStorageHashkey {
      service_name: key.service_name,
      key_id: key.key_id,
    };
    self.keys.borrow_mut().insert(
      hashkey,
      TimestampedKey {
        expires_at: (self.timesource)() + Duration::hours(1),
        pubkey: key.public_key,
      },
    );
    Ok(())
  }
}

/// A key storage that doesn't actually store keys, always needs to refetch them
#[derive(Debug)]
pub struct NullKeyStorage;

impl KeyStorage for NullKeyStorage {
  fn get(&self, _key_id: &str, _service_name: &str) -> Result<Option<ServiceKey>, KeyError> {
    Ok(None)
  }

  fn put(&self, _key: ServiceKey) -> Result<(), KeyError> {
    Ok(())
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use std::rc::Rc;

  #[test]
  fn in_memory_key_storage_stores_keys() {
    let storage = InMemoryKeyStorage::new();

    storage
      .put(ServiceKey {
        key_id: "key1".to_owned(),
        service_name: "service".to_owned(),
        public_key: vec![0, 1, 2, 3, 4, 5],
      })
      .unwrap();

    storage
      .put(ServiceKey {
        key_id: "key2".to_owned(),
        service_name: "service2".to_owned(),
        public_key: vec![6, 7, 8, 9, 10],
      })
      .unwrap();

    pretty_assertions::assert_eq!(
      storage.get("key1", "service").unwrap().unwrap().public_key,
      vec![0, 1, 2, 3, 4, 5]
    );
    assert!(storage.get("key2", "service").unwrap().is_none());
    assert!(storage.get("key1", "service2").unwrap().is_none());
  }

  #[test]
  fn in_memory_key_storage_checks_expiration() {
    let current_timestamp = Rc::new(RefCell::new(Utc::now()));
    let storage = InMemoryKeyStorage::new_with_timesource(|| current_timestamp.borrow().clone());

    storage
      .put(ServiceKey {
        key_id: "key1".to_owned(),
        service_name: "service".to_owned(),
        public_key: vec![0, 1, 2, 3, 4, 5],
      })
      .unwrap();
    current_timestamp.replace_with(|&mut old| old + Duration::hours(2));
    assert!(storage.get("key1", "service").unwrap().is_none());
  }

  #[test]
  fn null_key_storage_always_does_nothing() {
    let null_storage = NullKeyStorage;

    null_storage
      .put(ServiceKey {
        key_id: "key".to_owned(),
        service_name: "service".to_owned(),
        public_key: vec![6, 7, 8, 9, 10],
      })
      .unwrap();

    assert!(null_storage.get("key", "service").unwrap().is_none());
  }
}
