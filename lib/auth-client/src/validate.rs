use chrono::Duration;
use jsonwebtoken::{decode, decode_header, Algorithm, DecodingKey, Validation};

use crate::{generate::auth_service_name, keys::KeyStorage, AccessToken, JwtError, Subject};

pub fn validate_token(keystore: &impl KeyStorage, jwt: &str) -> Result<AccessToken, JwtError> {
  let header = decode_header(jwt).map_err(|_| JwtError::ParsingError)?;
  let kid = header.kid.ok_or(JwtError::MissingKey)?;
  let mut it = kid.split("::");
  let service_name = it.next().ok_or(JwtError::MissingKey)?;
  let key_id = it.next().ok_or(JwtError::MissingKey)?;
  let key = keystore
    .get(key_id, service_name)
    .map_err(|_| JwtError::UnknownKey {
      service_name: service_name.to_owned(),
      key_id: key_id.to_owned(),
    })?
    .ok_or_else(|| JwtError::UnknownKey {
      service_name: service_name.to_owned(),
      key_id: key_id.to_owned(),
    })?;
  let tok = decode::<AccessToken>(&jwt, &DecodingKey::from_ec_der(&key.public_key), &validation())?.claims;

  match &tok.sub {
    Subject::Application(name) => {
      if name != service_name {
        return Err(JwtError::ImpersonatedApp {
          subject: tok.sub,
          service_name: service_name.to_owned(),
        });
      }
    }
    Subject::User(_) | Subject::System(_) => {
      if service_name != auth_service_name() {
        return Err(JwtError::InvalidSubject {
          subject: tok.sub,
          service_name: service_name.to_owned(),
        });
      }
    }
  }

  if tok.exp - tok.iat > Duration::minutes(15) {
    return Err(JwtError::NeedsRevocationCheck(tok));
  }

  Ok(tok)
}

fn validation() -> Validation {
  Validation {
    leeway: 60,
    validate_nbf: true,
    validate_exp: true,
    ..Validation::new(Algorithm::ES256)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use uuid::Uuid;

  use crate::{
    keys::{InMemoryKeyStorage, ServiceKey},
    test_support::{make_jwt, make_key},
    AccessToken, Subject,
  };

  #[test]
  fn valid_token_succeeds() {
    let (private_key, pub_key) = make_key();
    let kid = "auth::test_id".to_owned();
    let now = chrono::Utc::now();
    let exp = now + chrono::Duration::minutes(15);
    let tok = make_jwt(
      &kid,
      &private_key,
      AccessToken {
        iss: "http://example.com".to_owned(),
        sub: Subject::User(Uuid::new_v4()),
        exp,
        nbf: now,
        iat: now,
        jti: Uuid::new_v4(),
      },
    );
    let keystore = InMemoryKeyStorage::new();
    keystore
      .put(ServiceKey {
        key_id: "test_id".to_owned(),
        service_name: "auth".to_owned(),
        public_key: pub_key,
      })
      .unwrap();
    assert!(validate_token(&keystore, &tok).is_ok())
  }

  #[test]
  fn rejects_bad_tokens() {
    let (private_key, pub_key) = make_key();
    let kid = "not_auth::test_id".to_owned();
    let now = chrono::Utc::now();
    let exp = now + chrono::Duration::minutes(15);
    let keystore = InMemoryKeyStorage::new();
    keystore
      .put(ServiceKey {
        key_id: "test_id".to_owned(),
        service_name: "not_auth".to_owned(),
        public_key: pub_key,
      })
      .unwrap();

    // invalid kid
    assert!(validate_token(
      &keystore,
      &make_jwt(
        "not_formatted_correctly",
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("not_auth".to_owned()),
          exp,
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // expired
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("not_auth".to_owned()),
          exp: now - chrono::Duration::minutes(5),
          nbf: now - chrono::Duration::minutes(20),
          iat: now - chrono::Duration::minutes(20),
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // from the future
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("not_auth".to_owned()),
          exp: now + chrono::Duration::minutes(20),
          nbf: now + chrono::Duration::minutes(5),
          iat: now + chrono::Duration::minutes(5),
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // non-auth service with user jwt
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::User(Uuid::new_v4()),
          exp,
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // non-auth service with system jwt
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::System("some_reason".to_owned()),
          exp,
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // wrong service
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("some_other_service".to_owned()),
          exp,
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // key not in store
    assert!(validate_token(
      &keystore,
      &make_jwt(
        "some_other_app::key",
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("some_other_app".to_owned()),
          exp,
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());

    // requires revocation check
    assert!(validate_token(
      &keystore,
      &make_jwt(
        &kid,
        &private_key,
        AccessToken {
          iss: "http://example.com".to_owned(),
          sub: Subject::Application("not_auth".to_owned()),
          exp: now + chrono::Duration::minutes(20),
          nbf: now,
          iat: now,
          jti: Uuid::new_v4()
        }
      )
    )
    .is_err());
  }
}
