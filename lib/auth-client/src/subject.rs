use std::fmt::Display;

use serde::{Deserialize, Serialize, Serializer};
use uuid::Uuid;

use crate::JwtError;

/// The Subject of the JWT
#[derive(Debug, PartialEq, Eq, Clone)]
pub enum Subject {
  /// a `u:credential_uuid` subject -- for a user
  User(Uuid),
  /// a `a:service_name` subject -- for a service
  Application(String),
  /// a `s:description` subject for the system (or superuser)
  System(String),
}

impl std::convert::TryFrom<&str> for Subject {
  type Error = JwtError;
  fn try_from(other: &str) -> Result<Subject, JwtError> {
    let splits: Vec<&str> = other.split(":").collect();
    if splits.len() != 2 {
      return Err(JwtError::CredentialError);
    }
    let [ty, detail] = [splits[0], splits[1]];
    match ty {
      "u" => Ok(Subject::User(
        Uuid::parse_str(detail).map_err(|_e| JwtError::CredentialError)?,
      )),
      "a" => Ok(Subject::Application(detail.to_owned())),
      "s" => Ok(Subject::System(detail.to_owned())),
      _ => Err(JwtError::CredentialError),
    }
  }
}

impl Subject {
  pub fn format_shorthand(&self) -> String {
    match self {
      Subject::User(user_uuid) => format!("u:{}", user_uuid),
      Subject::Application(service_name) => format!("a:{}", service_name),
      Subject::System(description) => format!("s:{}", description),
    }
  }
}

impl Display for Subject {
  fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
    f.write_str(&self.format_shorthand())
  }
}

impl<'de> Deserialize<'de> for Subject {
  fn deserialize<D>(deserializer: D) -> Result<Self, D::Error>
  where
    D: serde::Deserializer<'de>,
  {
    use serde::de::{self, Visitor};
    use std::{convert::TryFrom, fmt};
    struct SubjectVisitor;
    impl<'de> Visitor<'de> for SubjectVisitor {
      type Value = Subject;
      fn expecting(&self, formatter: &mut fmt::Formatter) -> fmt::Result {
        formatter.write_str("a string of the form u:uuid, a:service_name, or s:description")
      }

      fn visit_str<E: de::Error>(self, value: &str) -> Result<Subject, E> {
        Subject::try_from(value).map_err(E::custom)
      }
    }
    deserializer.deserialize_str(SubjectVisitor)
  }
}

impl Serialize for Subject {
  fn serialize<S: Serializer>(&self, serializer: S) -> Result<S::Ok, S::Error> {
    serializer.serialize_str(&self.format_shorthand())
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use std::convert::TryFrom;

  #[test]
  fn can_convert_to_str_and_back() {
    let subjs = vec![
      Subject::User(Uuid::parse_str("a6ff9c5a-cdff-4b64-ab34-aa311dd1ab3c").unwrap()),
      Subject::Application("test_app".to_owned()),
      Subject::System("some_system_subject".to_owned()),
    ];

    let strs = subjs.iter().map(Subject::format_shorthand).collect::<Vec<String>>();
    pretty_assertions::assert_eq!(
      strs,
      vec![
        "u:a6ff9c5a-cdff-4b64-ab34-aa311dd1ab3c".to_owned(),
        "a:test_app".to_owned(),
        "s:some_system_subject".to_owned()
      ]
    );

    let parsed = strs
      .iter()
      .map(|s| Subject::try_from(&**s).unwrap())
      .collect::<Vec<_>>();
    pretty_assertions::assert_eq!(subjs, parsed);
  }
}
