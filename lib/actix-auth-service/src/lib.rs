use actix::{Actor, Addr};
use actix_web::{
  dev::{Service, ServiceRequest, ServiceResponse, Transform},
  error::ErrorUnauthorized,
  http::header::AUTHORIZATION,
  Error, HttpMessage,
};
use actor::AuthorizeJwt;
use auth_client::AuthClient;
use futures::future::{ok, Future, Ready};
use std::{
  cell::RefCell,
  pin::Pin,
  rc::Rc,
  task::{Context, Poll},
};
use tracing::{debug, event, span, trace, Instrument, Level};

mod actor;

/// get a dyn auth client from environment variables
/// NANO_AUTH_MOCK_SUBJECT = the subject string to mock access as. Will use fake
/// client if provided
/// AUTH_URL = base URL of the auth service
/// Should provide one env var or another
/// Will panic if neither is provided
pub use auth_client::get_auth_client;
/// The credential UUID pulled from the JWT
/// If the JWT is valid, this will be added as a request extension
pub use auth_client::Subject;

pub use actor::AuthClientActor;
/// The raw JWT string, use this if you need to pass the JWT to another service
/// If the JWT is valid, this will be added as a request extension
pub struct Jwt(String);

/// A transform that adds a middleware to check all requests for valid JWTs.
/// It needs to be passed an `AuthClient` that knows where the auth server is, so it can fetch keys.
/// This is suitable for services, UIs need a different middleware since they are valid to access without
/// a valid JWT (for login pages and background refresh)
pub struct JwtVerify<Client: AuthClient + Unpin + 'static> {
  client_addr: Addr<AuthClientActor<Client>>,
}

impl<Client: AuthClient + Unpin> Clone for JwtVerify<Client> {
  fn clone(&self) -> Self {
    Self {
      client_addr: self.client_addr.clone(),
    }
  }
}

impl Default for JwtVerify<Box<dyn AuthClient>> {
  fn default() -> Self {
    JwtVerify {
      client_addr: AuthClientActor::new(get_auth_client()).start(),
    }
  }
}

impl<Client: AuthClient + Unpin> JwtVerify<Client> {
  pub fn new(client_addr: Addr<AuthClientActor<Client>>) -> Self {
    Self {
      client_addr: client_addr,
    }
  }
}

impl<S, T, B> Transform<S> for JwtVerify<T>
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  T: 'static + AuthClient + Unpin,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = JwtVerifyMiddleware<S, T>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(JwtVerifyMiddleware {
      client_addr: self.client_addr.clone(),
      service: Rc::new(RefCell::new(service)),
    })
  }
}

pub struct JwtVerifyMiddleware<S, Client: AuthClient + Unpin + 'static> {
  client_addr: Addr<AuthClientActor<Client>>,
  service: Rc<RefCell<S>>,
}

fn get_jwt(req: &ServiceRequest) -> Result<String, ()> {
  if let Some(header) = req.headers().get(AUTHORIZATION) {
    return Ok(
      header
        .to_str()
        .map_err(|_| ())?
        .split_whitespace()
        .nth(1)
        .ok_or(())?
        .to_owned(),
    );
  } else {
    return Err(());
  }
}

impl<S, T, B> Service for JwtVerifyMiddleware<S, T>
where
  B: 'static,
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  T: 'static + AuthClient + Unpin,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.borrow_mut().poll_ready(ctx)
  }

  fn call(&mut self, req: ServiceRequest) -> Self::Future {
    let span = span!(Level::INFO, "auth middleware");
    let srv = self.service.clone();
    let addr = self.client_addr.clone();

    Box::pin(
      async move {
        trace!("starting JwtVerifyMiddleware");
        let jwt_opt = get_jwt(&req);
        debug!("got jwt {:?}", jwt_opt);
        let jwt = jwt_opt.unwrap_or_default();
        let res = addr.send(AuthorizeJwt(jwt.clone())).await.unwrap();
        match res {
          Ok(access_token) => {
            {
              let mut ext = req.extensions_mut();
              let subject = access_token.sub;
              event!(Level::INFO, %subject, "successfully authenticated");
              ext.insert(Jwt(jwt));
              ext.insert(subject);
            }

            Ok(srv.borrow_mut().call(req).await?)
          }
          Err(e) => {
            event!(Level::ERROR, %e, "error authenticating");
            Err(ErrorUnauthorized(e))
          }
        }
      }
      .instrument(span),
    )
  }
}

/// A middleware that verifies passed-in JWTs are any app (or system JWT). This is the default for "infrastructure"-type services.
/// services with user-data will generally accept user JWTs and return data for that one user, and should use [actix_auth_service::UserAccess]
/// relies on [actix_auth_service::JwtVerify] to have already parsed the JWT.
pub struct AppAccess;
/// A middleware that verifies passed-in JWTs are any user (or system JWT). This is the default for user-data services.
/// infrastructure services generally accept app JWTs, and should use [actix_auth_service::AppAccess]
/// relies on [actix_auth_service::JwtVerify] to have already parsed the JWT.
pub struct UserAccess;

impl<S, B> Transform<S> for AppAccess
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = AccessMiddleware<S>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(AccessMiddleware {
      service: Rc::new(RefCell::new(service)),
      access_type: AccessType::App,
    })
  }
}

impl<S, B> Transform<S> for UserAccess
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type InitError = ();
  type Transform = AccessMiddleware<S>;
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(AccessMiddleware {
      service: Rc::new(RefCell::new(service)),
      access_type: AccessType::User,
    })
  }
}

#[derive(Debug, Clone, Copy, PartialEq)]
enum AccessType {
  App,
  User,
}

pub struct AccessMiddleware<S: Service> {
  service: Rc<RefCell<S>>,
  access_type: AccessType,
}

impl<S, B> Service for AccessMiddleware<S>
where
  B: 'static,
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static + std::future::Future,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&mut self, ctx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.borrow_mut().poll_ready(ctx)
  }

  fn call(&mut self, req: ServiceRequest) -> Self::Future {
    let span = span!(Level::INFO, "access middleware");
    let srv = self.service.clone();
    let access = self.access_type.clone();

    Box::pin(
      async move {
        trace!("starting AccessMiddleware");
        let s = {
          let ext = req.extensions();
          ext
            .get::<Subject>()
            .expect("JwtVerify middleware must run before AppAccess or UserAccess")
            .clone()
        };
        let valid = match s {
          Subject::User(_) => access == AccessType::User,
          Subject::Application(_) => access == AccessType::App,
          Subject::System(_) => true,
        };
        if valid {
          event!(Level::INFO, "successfully authorized for access");
          Ok(srv.borrow_mut().call(req).await?)
        } else {
          event!(Level::ERROR, "Unable to authorize for access");
          Err(ErrorUnauthorized("Invalid Access type"))
        }
      }
      .instrument(span),
    )
  }
}
