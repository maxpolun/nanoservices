use actix::{Actor, Context, Handler, Message, ResponseFuture};
use auth_client::{AccessToken, AuthClient, AuthError};
use std::{cell::RefCell, marker::Unpin, rc::Rc};
use tracing::info;

pub struct AuthClientActor<A: AuthClient> {
  pub client: Rc<RefCell<A>>,
}

impl<A: Unpin + AuthClient + 'static> Actor for AuthClientActor<A> {
  type Context = Context<Self>;
}

impl<A: AuthClient> AuthClientActor<A> {
  pub fn new(client: A) -> Self {
    Self {
      client: Rc::new(RefCell::new(client)),
    }
  }
}

impl<A: Unpin + AuthClient + 'static> actix::Supervised for AuthClientActor<A> {
  fn restarting(&mut self, _ctx: &mut Context<AuthClientActor<A>>) {
    info!("restarting auth client actor");
  }
}

pub struct AuthorizeJwt(pub String);

impl Message for AuthorizeJwt {
  type Result = Result<AccessToken, AuthError>;
}

impl<A: Unpin + AuthClient + 'static> Handler<AuthorizeJwt> for AuthClientActor<A> {
  type Result = ResponseFuture<Result<AccessToken, AuthError>>;
  fn handle(&mut self, msg: AuthorizeJwt, _ctx: &mut Self::Context) -> Self::Result {
    let client = self.client.clone();
    Box::pin(async move { client.borrow_mut().validate_jwt(&msg.0).await })
  }
}
