use actix_web::{
  dev::{Service, ServiceRequest, ServiceResponse, Transform},
  Error, HttpResponse,
};
use futures::{
  future::{ok, Ready},
  task::{Context, Poll},
};
use std::{
  future::{ready, Future},
  pin::Pin,
};

pub struct Healthcheck;

impl<S, B> Transform<S> for Healthcheck
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Transform = HealthcheckMiddleware<S>;
  type InitError = ();
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(HealthcheckMiddleware { service })
  }
}

pub struct HealthcheckMiddleware<S: Service> {
  service: S,
}

impl<S, B> Service for HealthcheckMiddleware<S>
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.poll_ready(cx)
  }

  fn call(&mut self, req: ServiceRequest) -> Self::Future {
    if req.path() == "/health_check" {
      return Box::pin(ready(Ok(req.into_response(HttpResponse::Ok().finish().into_body()))));
    }
    Box::pin(self.service.call(req))
  }
}
