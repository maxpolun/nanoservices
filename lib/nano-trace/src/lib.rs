#[cfg(not(feature = "jaeger"))]
use opentelemetry::sdk::export::trace::stdout;
use opentelemetry::{global, sdk::propagation::TraceContextPropagator};
use tracing_subscriber::{fmt, prelude::*, EnvFilter, Registry};

mod healthcheck_middleware;
mod log_middleware;

pub use actix_web_opentelemetry::RequestTracing;
pub use healthcheck_middleware::Healthcheck;
pub use log_middleware::TracingLogger;

#[cfg(not(feature = "jaeger"))]
pub type Uninstall = stdout::Uninstall;
#[cfg(feature = "jaeger")]
pub type Uninstall = opentelemetry_jaeger::Uninstall;

pub fn init() -> Uninstall {
  global::set_text_map_propagator(TraceContextPropagator::new());

  #[cfg(not(feature = "jaeger"))]
  let (tracer, uninstall) = stdout::new_pipeline().with_pretty_print(true).install();

  #[cfg(feature = "jaeger")]
  let (tracer, uninstall) = opentelemetry_jaeger::new_pipeline()
    .from_env()
    .with_service_name(std::env::var("SERVICE_NAME").unwrap_or_default())
    .install()
    .unwrap();

  println!("starting opentelemetry tracing with {:?}", tracer);
  let telemetry = tracing_opentelemetry::layer().with_tracer(tracer);
  let env_filter = EnvFilter::from_default_env();
  let fmt_layer = fmt::layer().pretty();
  #[cfg(not(debug_assertions))]
  let fmt_layer = fmt_layer.json();
  let _subscriber = Registry::default()
    .with(env_filter)
    .with(fmt_layer)
    .with(telemetry)
    .init();
  uninstall
}
