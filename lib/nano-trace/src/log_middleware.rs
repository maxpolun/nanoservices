use actix_web::{
  dev::{Service, ServiceRequest, ServiceResponse, Transform},
  Error,
};
use futures::{
  future::{ok, Ready},
  task::{Context, Poll},
};
use opentelemetry::trace::get_active_span;
use std::{cell::RefCell, future::Future, pin::Pin, rc::Rc};
use tracing::Span;
use tracing_futures::Instrument;

pub struct TracingLogger;

impl<S, B> Transform<S> for TracingLogger
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Transform = TracingLoggerMiddleware<S>;
  type InitError = ();
  type Future = Ready<Result<Self::Transform, Self::InitError>>;

  fn new_transform(&self, service: S) -> Self::Future {
    ok(TracingLoggerMiddleware {
      service: Rc::new(RefCell::new(service)),
    })
  }
}

pub struct TracingLoggerMiddleware<S> {
  service: Rc<RefCell<S>>,
}

impl<S, B> Service for TracingLoggerMiddleware<S>
where
  S: Service<Request = ServiceRequest, Response = ServiceResponse<B>, Error = Error> + 'static,
  S::Future: 'static,
  B: 'static,
{
  type Request = ServiceRequest;
  type Response = ServiceResponse<B>;
  type Error = Error;
  type Future = Pin<Box<dyn Future<Output = Result<Self::Response, Self::Error>>>>;

  fn poll_ready(&mut self, cx: &mut Context<'_>) -> Poll<Result<(), Self::Error>> {
    self.service.poll_ready(cx)
  }

  fn call(&mut self, req: ServiceRequest) -> Self::Future {
    let user_agent = req
      .headers()
      .get("User-Agent")
      .map(|h| h.to_str().unwrap_or(""))
      .unwrap_or("");
    let otel_span = get_active_span(|span| span.span_context().clone());
    let span = tracing::info_span!(
        "Request",
        request_path = %req.path(),
        user_agent = %user_agent,
        client_ip_address = %req.connection_info().realip_remote_addr().unwrap_or(""),
        status_code = tracing::field::Empty,
        trace_id = %otel_span.trace_id().to_hex(),
        span_id = %otel_span.span_id().to_hex(),
        trace_sampled = %otel_span.is_sampled()
    );

    let s = self.service.clone();
    Box::pin(
      async move {
        let outcome = s.borrow_mut().call(req).await;
        let status_code = match &outcome {
          Ok(response) => response.response().status(),
          Err(error) => error.as_response_error().status_code(),
        };
        Span::current().record("status_code", &status_code.as_u16());
        outcome
      }
      .instrument(span),
    )
  }
}
