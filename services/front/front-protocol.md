# The front protocol

For microfrontends.

## Interface for a microfrontend (MFE)

The MFE has an entry point which is a `.js` file. The .js file should be an esmodule with a default export. The default export should be a function, let's call it `configure`, and returns an object with the methods `mount(elem)` and `unmount`. There's also an optional `reconfigure` method. Here's a trivial example:

```js
export default function configure(config, publicPath) {
  return {
    config,
    elem: null,
    mount(elem) {
      this.elem = elem
      console.log('mounting elem')
    },
    unmount(elem) {
      console.log('unmounting elem')
      this.elem = null
    }
    reconfigure(config) {
      this.config = config
    }
  }
}
```

The meanings of the methods is:

`configure(config, publicPath)`: takes an initial configuration and the public path to the entry point (all other assets in the module should be accessible relative to the entry point, the public path allows the same code to be loaded by different apps). Returns an instance of the MFE.

Dependencies should be lazy-loaded using an `import()` expression relative to the publicPath. This can be done manually, e.g. in webpack setting the public path like so:

```js
__webpack_public_path__ = publicPath
```

`mount(elem)`: mount the MFE on a particular element. For e.g. React, this is where you call `ReactDOM.render`

`unmount()`: unmount the currently mounted MFE. For e.g. React, this is where you would call `ReactDOM.unmountComponentAtNode`

`reconfigure()`: Update the configuration of the MFE

## Runtimes

Runtimes are sets of packages that a MFE may rely on. They are loaded once for all MFEs that need them by the loader, then they can be referenced by the MFE. The MFE registration lists all runtimes required

For example, the `react_16` runtime containes the react and react-dom packages available int he global variables `window.__front_runtimes__.react_16.React` and `window.__front_runtimes__.react_16.ReactDOM` and an MFE can access these variables with the following webpack configuration:

```js
{
  externals: {
    react: 'window.__front_runtimes__.react_16.React',
    'react-dom': 'window.__front_runtimes__.react_16.ReactDOM'
  }
}
```
