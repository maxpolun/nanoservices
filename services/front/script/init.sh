#!/usr/bin/env bash

set -euo pipefail

SERVICE_NAME="front"

function create_database {
  createdb -O nanoservices "$1" 2> /dev/null && echo "$1 created" || echo "$1 exists"
}

create_database "${SERVICE_NAME}_dev"
create_database "${SERVICE_NAME}_test_template"

scriptdir="$( cd "$(dirname "$0")" ; pwd -P )"
servicedir="$(dirname scriptdir)"

cp "$servicedir/.env.template" "$servicedir/.env"
