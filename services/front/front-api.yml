openapi: '3.0.2'
info:
  title: Front
  version: '1.0'
servers:
  - url: https://front.maxpolun.com

components:
  securitySchemes:
    jwt:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    NewModule:
      type: object
      properties:
        identifier:
          type: string
        type:
          type: string
          enum: ['app', 'runtime']
    Module:
      type: object
      properties:
        type:
          type: string
          enum: ['app', 'runtime']
        identifier:
          type: string
        current_version:
          type: string
          format: uuid
        current_entry_point:
          type: string
          format: url
        created_at:
          type: string
          format: date-time
        updated_at:
          type: string
          format: date-time
    NewVersion:
      type: object
      properties:
        gitref:
          type: string
          format: gitref
        runtimes:
          type: array
          items:
            type: string
    Version:
      type: object
      properties:
        uuid:
          type: string
          format: uuid
        gitref:
          type: string
          format: gitref
        created_at:
          type: string
          format: date-time
        updated_at:
          type: string
          format: date-time
        entry_point:
          type: string
          format: url
        entry_point_asset_uuid:
          type: string
          format: uuid
    NewAsset:
      type: object
      properties:
        name:
          type: string
        mime_type:
          type: string
          format: mime-type
        contents:
          type: string
          format: base64
    Asset:
      type: object
      properties:
        uuid:
          type: string
          format: uuid
        name:
          type: string
        mime_type:
          type: string
          format: mime-type
        hash:
          type: object
          properties:
            alg:
              type: string
              enum:
                - sha256
            value:
              type: string
              format: base64
        public_path:
          type: string
          format: url

paths:
  /modules:
    post:
      operationId: modules.create
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/Module'
      responses:
        200:
          description: successfully created/updated module
    get:
      operationId: modules.index
      parameters:
        - name: type
          in: query
          schema:
            type: string
            enum: ['app', 'runtime']
      responses:
        200:
          description: successfully fetched modules
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Module'
  /modules/{module_id}:
    get:
      operationId: modules.show
      parameters:
        - name: module_id
          in: path
          required: true
          schema:
            type: string
      responses:
        200:
          description: successfully fetched the module
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Module'

  /modules/{module_id}/versions:
    post:
      operationId: module_versions.create
      description: Create a version for a given module
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewVersion'
      responses:
        200:
          description: Successfully created a new version for this module.
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Version'
    get:
      operationId: module_versions.index
      description: get a list of versions for the module
      responses:
        200:
          description: successfullly fetched the versions for this module
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Version'

  /modules/{module_id}/versions/{version_id}:
    get:
      operationId: module_versions.show
      description: get a single version
      responses:
        200:
          description: successfully fetched the version
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Version'
    patch:
      operationId: module_versions.update
      description: update an existing version
      requestBody:
        content:
          application/json:
            schema:
              type: object
              properties:
                entry_point_asset_uuid:
                  type: string
                  format: uuid
      responses:
        200:
          description: successfully updated the version
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Version'
  /modules/{module_id}/versions/{version_id}/assets:
    post:
      operationId: module_version_assets.create
      description: upload an asset for a particular version
      requestBody:
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewAsset'
      responses:
        200:
          description: successfully uploaded asset
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asset'
    get:
      operationId: module_version_assets.index
      description: get all assets for a version
      responses:
        200:
          description: successfully fetched assets
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Asset'
  /modules/{module_id}/versions/{version_id}/assets/{asset_id}:
    get:
      operationId: module_version_assets.show
      description: get asset
      responses:
        200:
          description: successfully fetched asset
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Asset'
