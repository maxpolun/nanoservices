-- Your SQL goes here
CREATE EXTENSION pgcrypto;

CREATE TABLE IF NOT EXISTS module_versions (
  uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  module_identifier TEXT NOT NULL REFERENCES modules(module_identifier),
  gitref TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('module_versions');
