-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS module_versions;
DROP EXTENSION IF EXISTS pgcrypto;
