-- Your SQL goes here

CREATE TABLE assets (
  hash_sha256 TEXT PRIMARY KEY,
  contents BYTEA NOT NULL
);

CREATE TABLE version_assets (
  uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  asset_hash TEXT NOT NULL REFERENCES assets(hash_sha256),
  version_uuid UUID NOT NULL REFERENCES module_versions(uuid),
  name TEXT NOT NULL,
  mime_type TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('version_assets');
