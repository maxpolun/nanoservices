-- Your SQL goes here

CREATE TYPE module_type_enum AS ENUM ('app', 'runtime');

CREATE TABLE IF NOT EXISTS modules (
  module_identifier TEXT PRIMARY KEY,
  module_type module_type_enum NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);

SELECT diesel_manage_updated_at('modules');
