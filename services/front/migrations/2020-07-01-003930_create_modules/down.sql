-- This file should undo anything in `up.sql`
DROP TABLE IF EXISTS modules;
DROP TYPE IF EXISTS module_type_enum;
