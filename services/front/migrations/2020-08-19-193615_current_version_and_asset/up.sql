-- Your SQL goes here

ALTER TABLE modules ADD COLUMN current_version_uuid UUID REFERENCES module_versions(uuid);
ALTER TABLE module_versions ADD COLUMN entry_point_asset_uuid UUID REFERENCES version_assets(uuid);
