-- This file should undo anything in `up.sql`

ALTER TABLE module_versions DROP COLUMN entry_point_asset_uuid;
ALTER TABLE modules DROP COLUMN current_version_uuid;
