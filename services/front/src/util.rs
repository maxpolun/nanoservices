use diesel::{r2d2::ConnectionManager, PgConnection};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

pub type Timestamp = chrono::DateTime<chrono::Utc>;
pub type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Serialize, Deserialize)]
pub struct ErrorDescription {
  pub error_id: String,
  pub message: String,
}

#[derive(Debug, Deserialize)]
pub struct PathWithMod {
  pub module_identifier: String,
}

#[derive(Debug, Deserialize)]
pub struct PathWithModVersion {
  pub module_identifier: String,
  pub version_uuid: Uuid,
}

#[derive(Debug, Deserialize)]
pub struct PathWithModVersionAsset {
  pub module_identifier: String,
  pub version_uuid: Uuid,
  pub asset_uuid: Uuid,
}
