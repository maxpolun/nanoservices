use super::model::{NewVersionAsset, VersionAsset, VersionAssetCreateError};
use crate::actors::RealDbActor;
use actix::{Handler, Message};
use uuid::Uuid;

#[derive(Debug)]
pub struct CreateAsset {
  pub asset: NewVersionAsset,
  pub version_uuid: Uuid,
}

impl Message for CreateAsset {
  type Result = Result<VersionAsset, VersionAssetCreateError>;
}

impl Handler<CreateAsset> for RealDbActor {
  type Result = Result<VersionAsset, VersionAssetCreateError>;
  fn handle(&mut self, msg: CreateAsset, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    VersionAsset::create(&conn, msg.version_uuid, msg.asset)
  }
}

pub struct GetByUuid(pub Uuid);

impl Message for GetByUuid {
  type Result = Result<VersionAsset, diesel::result::Error>;
}

impl Handler<GetByUuid> for RealDbActor {
  type Result = Result<VersionAsset, diesel::result::Error>;
  fn handle(&mut self, msg: GetByUuid, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    VersionAsset::get_by_uuid(&conn, msg.0)
  }
}

pub struct GetAllByVersion(pub Uuid);

impl Message for GetAllByVersion {
  type Result = Result<Vec<VersionAsset>, diesel::result::Error>;
}

impl Handler<GetAllByVersion> for RealDbActor {
  type Result = Result<Vec<VersionAsset>, diesel::result::Error>;
  fn handle(&mut self, msg: GetAllByVersion, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    VersionAsset::get_all(&conn, msg.0)
  }
}
