use crate::{
  schema::*,
  util::{ErrorDescription, Timestamp},
};
use actix_web::{dev::HttpResponseBuilder, http::StatusCode, ResponseError};
use diesel::{connection::Connection, prelude::*, PgConnection};
use mime::{FromStrError, Mime};
use serde::{Deserialize, Serialize};
use sha2::{Digest, Sha256};
use thiserror::Error;
use uuid::Uuid;

#[derive(Debug, Queryable, Clone, PartialEq, Eq)]
pub struct VersionAsset {
  pub uuid: Uuid,
  pub asset_hash: String,
  pub version_uuid: Uuid,
  pub name: String,
  pub mime_type: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}
#[derive(Debug, Error)]
pub enum VersionAssetCreateError {
  #[error("name is not formatted correctly")]
  InvalidName,
  #[error("Embedded Hash in name does not match the hash from the contents")]
  MismatchedHash,
  #[error("Unable to parse Mime data: {}", .0)]
  InvalidMime(#[from] FromStrError),
  #[error("contents is expected to be base64 encoded: {}", .0)]
  InvalidContentsEncoding(#[from] base64::DecodeError),
  #[error("database error: {}", .0)]
  DbError(#[from] diesel::result::Error),
}

impl VersionAssetCreateError {
  pub fn get_error_description(&self) -> ErrorDescription {
    let message = format!("{}", self);
    match self {
      VersionAssetCreateError::InvalidName => ErrorDescription {
        error_id: "front::asset::invalid_name".to_owned(),
        message,
      },
      VersionAssetCreateError::MismatchedHash => ErrorDescription {
        error_id: "front::asset::mismatched_hash".to_owned(),
        message,
      },
      VersionAssetCreateError::InvalidMime(_) => ErrorDescription {
        error_id: "front::asset::invalid_mime_type".to_owned(),
        message,
      },
      VersionAssetCreateError::InvalidContentsEncoding(_) => ErrorDescription {
        error_id: "front::asset::invalid_content_encoding".to_owned(),
        message,
      },
      VersionAssetCreateError::DbError(_) => ErrorDescription {
        error_id: "common::db_error::unknown".to_owned(),
        message,
      },
    }
  }
}

impl ResponseError for VersionAssetCreateError {
  fn status_code(&self) -> actix_web::http::StatusCode {
    match self {
      VersionAssetCreateError::DbError(_) => StatusCode::INTERNAL_SERVER_ERROR,
      _ => StatusCode::UNPROCESSABLE_ENTITY,
    }
  }
  fn error_response(&self) -> actix_web::HttpResponse {
    let mut resp = HttpResponseBuilder::new(self.status_code());
    resp.json(self.get_error_description())
  }
}

impl VersionAsset {
  pub fn create(
    conn: &PgConnection,
    version_uuid: Uuid,
    new_asset: NewVersionAsset,
  ) -> Result<VersionAsset, VersionAssetCreateError> {
    use VersionAssetCreateError::*;
    let name = new_asset.name.clone();
    let mime_type = new_asset.mime_type.clone();
    let [_fname, hash_and_ext] = {
      let mut iter = new_asset.name.split("-");
      let items = [iter.next().ok_or(InvalidName)?, iter.next().ok_or(InvalidName)?];
      if let Some(_item) = iter.next() {
        return Err(InvalidName);
      }
      items
    };
    let [hash, _ext] = {
      let mut iter = hash_and_ext.split(".");
      [iter.next().ok_or(InvalidName)?, iter.next().ok_or(InvalidName)?]
    };

    let decoded_contents = base64::decode(new_asset.contents)?;
    let computed_hash = Sha256::digest(&decoded_contents);
    let provided_hash = hex::decode(hash).map_err(|_e| InvalidName)?;
    if computed_hash[..] != provided_hash[..] {
      return Err(MismatchedHash);
    }
    new_asset.mime_type.parse::<Mime>()?;

    Ok(conn.transaction(|| {
      let asset = Asset {
        hash_sha256: hash.to_owned(),
        contents: decoded_contents,
      };
      diesel::insert_into(assets::table).values(asset).execute(conn)?;
      let new_record = NewVersionAssetRecord {
        asset_hash: hash.to_owned(),
        version_uuid,
        name,
        mime_type,
      };
      diesel::insert_into(version_assets::table)
        .values(new_record)
        .get_result(conn)
    })?)
  }

  pub fn get_by_uuid(conn: &PgConnection, asset_uuid: Uuid) -> Result<VersionAsset, diesel::result::Error> {
    version_assets::table.find(asset_uuid).get_result(conn)
  }

  pub fn get_all(conn: &PgConnection, version_uuid: Uuid) -> Result<Vec<VersionAsset>, diesel::result::Error> {
    version_assets::table
      .filter(version_assets::version_uuid.eq(version_uuid))
      .get_results(conn)
  }
}

#[derive(Debug, Queryable, Clone, PartialEq, Eq, Insertable)]
pub struct Asset {
  pub hash_sha256: String,
  pub contents: Vec<u8>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct NewVersionAsset {
  pub name: String,
  pub mime_type: String,
  pub contents: String,
}

#[derive(Debug, Clone, PartialEq, Eq, Insertable)]
#[table_name = "version_assets"]
struct NewVersionAssetRecord {
  pub asset_hash: String,
  pub version_uuid: Uuid,
  pub name: String,
  pub mime_type: String,
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    modules::model::{Module, ModuleType, NewModule},
    test_util::test_connection,
    versions::model::{ModuleVersion, NewModuleVersion},
  };
  use diesel::dsl::count_star;

  fn make_mod(conn: &PgConnection) -> Module {
    Module::create(
      conn,
      NewModule {
        module_identifier: "test".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap()
  }

  fn make_mod_and_version(conn: &PgConnection) -> (Module, ModuleVersion) {
    let m = make_mod(conn);
    let v = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: m.module_identifier.clone(),
        gitref: "git@gitlab.com/maxpolun/nanoservices.git#branch_name".to_owned(),
      },
    )
    .unwrap();
    (m, v)
  }

  #[test]
  fn it_validates_inputs() {
    let conn = test_connection();
    let uuid = Uuid::new_v4();

    // file content: console.log("hello world")
    // file hash: e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::InvalidName)
    ));

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-nohash.js".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::InvalidName)
    ));

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js-bad.js".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::InvalidName)
    ));

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
          mime_type: "missing mime".to_owned(),
          contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::InvalidMime(_err))
    ));

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "odf pojid [iwjf]=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::InvalidContentsEncoding(_err))
    ));

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "Y29uc29sZS5sb2coJ3NvbWV0aGluZyBkaWZmZXJlbnQnKQ==".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::MismatchedHash)
    ));
  }

  #[test]
  fn can_create() {
    let conn = test_connection();
    let (_m, v) = make_mod_and_version(&conn);

    let version_asset = VersionAsset::create(
      &conn,
      v.uuid,
      NewVersionAsset {
        name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
      },
    )
    .unwrap();

    assets::table
      .find(version_asset.asset_hash)
      .get_result::<Asset>(&conn)
      .unwrap();
  }

  #[test]
  fn it_is_atomic() {
    let conn = test_connection();
    let uuid = Uuid::new_v4();

    assert!(matches!(
      VersionAsset::create(
        &conn,
        uuid,
        NewVersionAsset {
          name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
          mime_type: "application/javascript".to_owned(),
          contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
        }
      ),
      Err(VersionAssetCreateError::DbError(_err))
    ));

    pretty_assertions::assert_eq!(assets::table.select(count_star()).first::<i64>(&conn), Ok(0))
  }

  #[test]
  fn get_all_works() {
    let conn = test_connection();
    let (_m, v) = make_mod_and_version(&conn);

    for i in 0..100 {
      let str = format!("console.log('hello, {}')", i);
      let contents = base64::encode(&str);
      let hash = Sha256::digest(str.as_bytes());
      let name = format!("app-{:x}.js", hash);
      VersionAsset::create(
        &conn,
        v.uuid,
        NewVersionAsset {
          name,
          mime_type: "application/javascript".to_owned(),
          contents,
        },
      )
      .unwrap();
    }

    pretty_assertions::assert_eq!(VersionAsset::get_all(&conn, v.uuid).unwrap().len(), 100)
  }

  #[test]
  fn get_by_uuid_works() {
    let conn = test_connection();
    let (_m, v) = make_mod_and_version(&conn);

    let version_asset = VersionAsset::create(
      &conn,
      v.uuid,
      NewVersionAsset {
        name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
      },
    )
    .unwrap();

    pretty_assertions::assert_eq!(
      VersionAsset::get_by_uuid(&conn, version_asset.uuid).unwrap(),
      version_asset
    );
  }
}
