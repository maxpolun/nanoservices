use actix_web::{web, Result, HttpResponse};
use actix::Addr;
use crate::{actors::DbActor, assets::{model::Asset, messages::{GetAssetContentsByHash}}};

pub async fn get_by_hash(db: web::Data<Addr<DbActor>>, asset_name: web::Path<String>) -> Result<HttpResponse> {
  if let Some(hash) = Asset::parse_name(&asset_name.into_inner()) {
    match db.send(GetAssetContentsByHash(hash.to_owned())).await? {
      Ok(asset) => Ok(HttpResponse::Ok().body(asset.contents)),
      Err(diesel::result::Error::NotFound) => Ok(HttpResponse::NotFound().finish()),
      Err(_) => Ok(HttpResponse::InternalServerError().finish())
    }
  } else {
    Ok(HttpResponse::NotFound().finish())
  }

}
