use crate::{
  actors::DbActor,
  assets::{
    messages::CreateAsset,
    model::{NewVersionAsset, VersionAsset},
  },
  util::PathWithModVersion,
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct AssetResponse {
  pub uuid: Uuid,
  pub name: String,
  pub mime_type: String,
  pub hash: HashDescription,
}

impl From<VersionAsset> for AssetResponse {
  fn from(va: VersionAsset) -> Self {
    Self {
      uuid: va.uuid,
      name: va.name,
      mime_type: va.mime_type,
      hash: HashDescription {
        alg: HashAlg::Sha256,
        value: va.asset_hash,
      },
    }
  }
}

#[derive(Debug, Serialize, Deserialize)]
pub struct HashDescription {
  pub alg: HashAlg,
  pub value: String,
}

#[derive(Debug, Serialize, Deserialize)]
#[serde(rename_all = "snake_case")]
pub enum HashAlg {
  Sha256,
}

pub async fn create(
  db: web::Data<Addr<DbActor>>,
  new_asset: web::Json<NewVersionAsset>,
  path: web::Path<PathWithModVersion>,
) -> Result<HttpResponse> {
  let va = db
    .send(CreateAsset {
      asset: new_asset.into_inner(),
      version_uuid: path.version_uuid,
    })
    .await??;
  Ok(HttpResponse::Ok().json(AssetResponse::from(va)))
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    assets::{messages::CreateAsset, model::VersionAsset},
    test_util::parse_http_response,
  };
  use actix_mock_helper::simple_mock_actor;
  use actix_web::http;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn can_create() {
    let mock_actor = simple_mock_actor::<RealDbActor, CreateAsset, _>(|msg| {
      Ok(VersionAsset {
        uuid: Uuid::new_v4(),
        asset_hash: "abc123".to_owned(),
        version_uuid: msg.version_uuid,
        name: "fake_asset-abc123.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      })
    });

    let response = create(
      web::Data::new(mock_actor),
      web::Json(NewVersionAsset {
        name: "fake_asset-abc123.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        contents: "abc123".to_owned(),
      }),
      web::Path::from(PathWithModVersion {
        module_identifier: "test".to_owned(),
        version_uuid: Uuid::new_v4(),
      }),
    )
    .await
    .unwrap();

    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<AssetResponse>(&response).hash.value, "abc123")
  }
}
