use super::create::AssetResponse;
use crate::{actors::DbActor, assets::messages::GetByUuid, util::PathWithModVersionAsset};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn get(db: web::Data<Addr<DbActor>>, path: web::Path<PathWithModVersionAsset>) -> Result<HttpResponse> {
  match db.send(GetByUuid(path.asset_uuid)).await? {
    Ok(va) => Ok(HttpResponse::Ok().json(AssetResponse::from(va))),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    assets::{messages::GetByUuid, model::VersionAsset, routes::create::AssetResponse},
    test_util::parse_http_response,
  };
  use actix_mock_helper::simple_mock_actor;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn testname() {
    let asset_uuid = Uuid::new_v4();
    let mock_actor = simple_mock_actor::<RealDbActor, GetByUuid, _>(|msg| {
      Ok(VersionAsset {
        uuid: msg.0,
        asset_hash: "abc123".to_owned(),
        version_uuid: Uuid::new_v4(),
        name: "fake_asset-abc123.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      })
    });

    let response = get(
      web::Data::new(mock_actor),
      web::Path::from(PathWithModVersionAsset {
        module_identifier: "test".to_owned(),
        version_uuid: Uuid::new_v4(),
        asset_uuid,
      }),
    )
    .await
    .unwrap();

    pretty_assertions::assert_eq!(parse_http_response::<AssetResponse>(&response).uuid, asset_uuid)
  }
}
