use super::create::AssetResponse;
use crate::{actors::DbActor, assets::messages::GetAllByVersion, util::PathWithModVersion};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn index(db: web::Data<Addr<DbActor>>, path: web::Path<PathWithModVersion>) -> Result<HttpResponse> {
  match db.send(GetAllByVersion(path.version_uuid)).await? {
    Ok(vas) => Ok(
      HttpResponse::Ok().json(
        vas
          .into_iter()
          .map(|va| AssetResponse::from(va))
          .collect::<Vec<AssetResponse>>(),
      ),
    ),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    assets::{messages::GetAllByVersion, model::VersionAsset, routes::create::AssetResponse},
    test_util::parse_http_response,
  };
  use actix_mock_helper::simple_mock_actor;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn index_works() {
    let mock_actor = simple_mock_actor::<RealDbActor, GetAllByVersion, _>(|msg| {
      Ok(vec![VersionAsset {
        uuid: Uuid::new_v4(),
        asset_hash: "abc123".to_owned(),
        version_uuid: msg.0,
        name: "fake_asset-abc123.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      }])
    });

    let response = index(
      web::Data::new(mock_actor),
      web::Path::from(PathWithModVersion {
        module_identifier: "test".to_owned(),
        version_uuid: Uuid::new_v4(),
      }),
    )
    .await
    .unwrap();

    pretty_assertions::assert_eq!(parse_http_response::<Vec<AssetResponse>>(&response).len(), 1)
  }
}
