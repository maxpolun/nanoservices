use crate::{schema::*, util::Timestamp, versions::model::ModuleVersion};
use diesel::{prelude::*, PgConnection};
use diesel_derive_enum::DbEnum;
use serde::{Deserialize, Serialize};
use thiserror::Error;
use uuid::Uuid;

#[derive(Debug, DbEnum, Clone, Serialize, Deserialize, PartialEq, Eq)]
#[DieselType = "Module_type_enum"]
#[serde(rename_all = "snake_case")]
pub enum ModuleType {
  App,
  Runtime,
}

#[derive(Debug, Queryable, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct Module {
  pub module_identifier: String,
  pub module_type: ModuleType,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
  pub current_version_uuid: Option<Uuid>,
}

#[derive(Debug, Insertable, Clone, Serialize, Deserialize)]
#[table_name = "modules"]
pub struct NewModule {
  #[serde(rename = "identifier")]
  pub module_identifier: String,
  #[serde(rename = "type")]
  pub module_type: ModuleType,
}

#[derive(Debug, Error)]
pub enum ModuleCreateError {
  #[error("module_identifier is invalid")]
  InvalidId,
  #[error("error saving to database")]
  DbError(#[from] diesel::result::Error),
  #[error("a module with identifier {}", .0.module_identifier)]
  AlreadyExists(Module),
}

#[derive(Debug, Error)]
pub enum SetVersionError {
  #[error("version is for a different module")]
  InvalidVersion,
  #[error("version is not in database")]
  MissingVersion,
  #[error("error saving to database: {}", .0)]
  DbError(#[from] diesel::result::Error),
}

impl Module {
  pub fn create(conn: &PgConnection, new_mod: NewModule) -> Result<Self, ModuleCreateError> {
    if !new_mod
      .module_identifier
      .chars()
      .all(|c| c.is_ascii_alphanumeric() || c == '_')
    {
      return Err(ModuleCreateError::InvalidId);
    }

    match modules::table
      .find(&new_mod.module_identifier)
      .get_result::<Module>(conn)
    {
      Ok(module) => return Err(ModuleCreateError::AlreadyExists(module)),
      Err(diesel::result::Error::NotFound) => {}
      Err(e) => return Err(e)?,
    }
    Ok(diesel::insert_into(modules::table).values(&new_mod).get_result(conn)?)
  }

  pub fn index(conn: &PgConnection) -> Result<Vec<Self>, diesel::result::Error> {
    modules::table.load::<Module>(conn)
  }

  pub fn get_by_identifier(conn: &PgConnection, identifier: &str) -> Result<Self, diesel::result::Error> {
    modules::table.find(identifier).get_result(conn)
  }

  pub fn set_current_version(&self, conn: &PgConnection, version: &ModuleVersion) -> Result<Self, SetVersionError> {
    use diesel::result::{DatabaseErrorKind, Error};
    if self.module_identifier != version.module_identifier {
      return Err(SetVersionError::InvalidVersion);
    }
    diesel::update(modules::table.find(&self.module_identifier))
      .set(modules::current_version_uuid.eq(version.uuid))
      .get_result(conn)
      .map_err(|e| match e {
        Error::DatabaseError(DatabaseErrorKind::ForeignKeyViolation, _) => SetVersionError::MissingVersion,
        e @ _ => SetVersionError::DbError(e),
      })
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{diesel::Connection, test_util::test_connection, versions::model::NewModuleVersion};
  use chrono::Utc;

  #[test]
  fn can_create() {
    let conn = test_connection();
    Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    pretty_assertions::assert_eq!(modules::table.load::<Module>(&conn).unwrap().len(), 1);
  }

  #[test]
  fn cannot_create_with_invalid_name() {
    let conn = test_connection();
    let names = vec!["test module", "😊", "test-module"];
    for name in names {
      assert!(matches!(
        Module::create(
          &conn,
          NewModule {
            module_identifier: name.to_owned(),
            module_type: ModuleType::App,
          }
        )
        .unwrap_err(),
        ModuleCreateError::InvalidId
      ))
    }

    pretty_assertions::assert_eq!(conn.execute("SELECT * FROM modules;").unwrap(), 0);
  }

  #[test]
  fn cannot_make_dupe() {
    let conn = test_connection();
    let mod1 = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    let err = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap_err();

    let mod2 = match err {
      ModuleCreateError::AlreadyExists(m) => m,
      other_variant => panic!("different error {:?}", other_variant),
    };

    pretty_assertions::assert_eq!(mod1, mod2);
  }

  #[test]
  fn index_gets_all_modules() {
    let conn = test_connection();
    for i in 0..100 {
      Module::create(
        &conn,
        NewModule {
          module_identifier: format!("test_module{}", i),
          module_type: ModuleType::App,
        },
      )
      .unwrap();
    }
    pretty_assertions::assert_eq!(Module::index(&conn).unwrap().len(), 100)
  }

  #[test]
  fn get_by_identifier_finds_the_module() {
    let conn = test_connection();
    let mod1 = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    let mod2 = Module::get_by_identifier(&conn, "test_module1").unwrap();

    pretty_assertions::assert_eq!(mod1, mod2);
  }

  #[test]
  fn get_by_identifier_returns_not_found_if_mod_doesnt_exist() {
    let conn = test_connection();

    let err = Module::get_by_identifier(&conn, "test_module1").unwrap_err();

    pretty_assertions::assert_eq!(err, diesel::result::Error::NotFound);
  }

  #[test]
  fn set_current_version_works() {
    let conn = test_connection();

    let m = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    let v = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: m.module_identifier.clone(),
        gitref: "git@gitlab.com:maxpolun/nanoservices#branchname".to_owned(),
      },
    )
    .unwrap();

    let m2 = m.set_current_version(&conn, &v).unwrap();
    pretty_assertions::assert_eq!(m2.current_version_uuid, Some(v.uuid))
  }

  #[test]
  fn set_current_version_validates_version() {
    let conn = test_connection();

    let m1 = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module1".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    let m2 = Module::create(
      &conn,
      NewModule {
        module_identifier: "test_module2".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();

    let v1 = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: m2.module_identifier.clone(),
        gitref: "git@gitlab.com:maxpolun/nanoservices#branchname".to_owned(),
      },
    )
    .unwrap();

    let v2 = ModuleVersion {
      uuid: Uuid::new_v4(),
      module_identifier: "test_module1".to_owned(),
      gitref: "fake".to_owned(),
      created_at: Utc::now(),
      updated_at: Utc::now(),
      entry_point_asset_uuid: None,
    };

    assert!(matches!(
      m1.set_current_version(&conn, &v1),
      Err(SetVersionError::InvalidVersion)
    ));

    assert!(matches!(
      m1.set_current_version(&conn, &v2),
      Err(SetVersionError::MissingVersion)
    ));
  }
}
