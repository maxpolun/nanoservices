use super::model::{Module, ModuleCreateError, NewModule, SetVersionError};
use crate::{actors::RealDbActor, versions::model::ModuleVersion};
use actix::{Handler, Message};
use uuid::Uuid;

#[derive(Debug)]
pub struct CreateModule(pub NewModule);

impl Message for CreateModule {
  type Result = Result<Module, ModuleCreateError>;
}

impl Handler<CreateModule> for RealDbActor {
  type Result = Result<Module, ModuleCreateError>;
  fn handle(&mut self, msg: CreateModule, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Module::create(&conn, msg.0)
  }
}

#[derive(Debug)]
pub struct IndexModules;

impl Message for IndexModules {
  type Result = Result<Vec<Module>, diesel::result::Error>;
}

impl Handler<IndexModules> for RealDbActor {
  type Result = Result<Vec<Module>, diesel::result::Error>;
  fn handle(&mut self, _msg: IndexModules, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Module::index(&conn)
  }
}

#[derive(Debug)]
pub struct GetModuleById(pub String);

impl Message for GetModuleById {
  type Result = Result<Module, diesel::result::Error>;
}

impl Handler<GetModuleById> for RealDbActor {
  type Result = Result<Module, diesel::result::Error>;
  fn handle(&mut self, msg: GetModuleById, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Module::get_by_identifier(&conn, &msg.0)
  }
}

#[derive(Debug)]
pub struct PatchModule {
  pub module_identifier: String,
  pub current_version_uuid: Uuid,
}

impl Message for PatchModule {
  type Result = Result<Module, SetVersionError>;
}

impl Handler<PatchModule> for RealDbActor {
  type Result = Result<Module, SetVersionError>;
  fn handle(&mut self, msg: PatchModule, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let version =
      ModuleVersion::get_by_uuid(&conn, msg.current_version_uuid).map_err(|_| SetVersionError::MissingVersion)?;
    Module::get_by_identifier(&conn, &msg.module_identifier)?.set_current_version(&conn, &version)
  }
}
