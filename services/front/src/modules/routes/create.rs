use crate::{
  actors::DbActor,
  modules::{
    messages::CreateModule,
    model::{ModuleCreateError, NewModule},
  },
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn create(db: web::Data<Addr<DbActor>>, new_mod: web::Json<NewModule>) -> Result<HttpResponse> {
  let new_mod_clone = new_mod.clone();
  match db.send(CreateModule(new_mod.into_inner())).await? {
    Ok(module) => Ok(HttpResponse::Ok().json(module)),
    Err(ModuleCreateError::InvalidId) => Ok(HttpResponse::UnprocessableEntity().finish()),
    Err(ModuleCreateError::AlreadyExists(m)) => {
      if m.module_type == new_mod_clone.module_type {
        Ok(HttpResponse::Ok().json(m))
      } else {
        Ok(HttpResponse::Conflict().finish())
      }
    }
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    modules::model::{Module, ModuleType},
    test_util::parse_http_response,
    actors::RealDbActor
  };
  use actix_web::http;
  use chrono::Utc;

  #[actix_rt::test]
  async fn can_create() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, CreateModule, _>(|msg| {
      Ok(Module {
        module_identifier: msg.0.module_identifier.clone(),
        module_type: msg.0.module_type.clone(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: None,
      })
    });
    let new_module = NewModule {
      module_identifier: "test".to_owned(),
      module_type: ModuleType::App,
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module)).await.unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Module>(&response).module_identifier, "test")
  }

  #[actix_rt::test]
  async fn create_422_with_invalid_name() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, CreateModule, _>(|msg| {
      Err(ModuleCreateError::InvalidId)
    });

    let new_module = NewModule {
      module_identifier: "test module".to_owned(),
      module_type: ModuleType::App,
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module)).await.unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::UNPROCESSABLE_ENTITY);
  }

  #[actix_rt::test]
  async fn will_409_if_the_create_would_change_the_type() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, CreateModule, _>(|msg| {
      Err(ModuleCreateError::AlreadyExists(Module {
        module_identifier: "test".to_owned(),
        module_type: ModuleType::Runtime,
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: None,
      }))
    });

    let new_module = NewModule {
      module_identifier: "test".to_owned(),
      module_type: ModuleType::App,
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module)).await.unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::CONFLICT);
  }
  #[actix_rt::test]
  async fn can_create_existing_if_same_type() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, CreateModule, _>(|_msg| {
      Err(ModuleCreateError::AlreadyExists(Module {
        module_identifier: "test".to_owned(),
        module_type: ModuleType::App,
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: None,
      }))
    });

    let new_module = NewModule {
      module_identifier: "test".to_owned(),
      module_type: ModuleType::App,
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module)).await.unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Module>(&response).module_identifier, "test");
  }
}
