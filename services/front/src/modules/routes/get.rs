use crate::{actors::DbActor, modules::messages::GetModuleById, util::PathWithMod};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn get(db: web::Data<Addr<DbActor>>, path: web::Path<PathWithMod>) -> Result<HttpResponse> {
  match db.send(GetModuleById(path.module_identifier.clone())).await? {
    Ok(module) => Ok(HttpResponse::Ok().json(module)),
    Err(diesel::result::Error::NotFound) => Ok(HttpResponse::NotFound().finish()),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    modules::model::{Module, ModuleType},
    test_util::parse_http_response,
    actors::RealDbActor
  };
  use actix_web::http;
  use chrono::Utc;

  #[actix_rt::test]
  async fn can_get() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, GetModuleById, _>(|_msg| {
      Ok(Module {
        module_identifier: "test_mod".to_owned(),
        module_type: ModuleType::App,
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: None,
      })
    });

    let response = get(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test_mod".to_owned(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Module>(&response).module_identifier, "test_mod")
  }

  #[actix_rt::test]
  async fn will_404_if_missing() {
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, GetModuleById, _>(|_msg| {
      Err(diesel::result::Error::NotFound)
    });

    let response = get(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test_mod".to_owned(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::NOT_FOUND);
  }
}
