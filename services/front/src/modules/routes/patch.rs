use crate::{
  actors::DbActor,
  modules::{messages::PatchModule, model::SetVersionError},
  util::PathWithMod,
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct PatchModuleRequest {
  pub current_version_uuid: Uuid,
}

pub async fn patch(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<PathWithMod>,
  patch_body: web::Json<PatchModuleRequest>,
) -> Result<HttpResponse> {
  match db
    .send(PatchModule {
      module_identifier: path.module_identifier.clone(),
      current_version_uuid: patch_body.current_version_uuid,
    })
    .await?
  {
    Ok(module) => Ok(HttpResponse::Ok().json(module)),
    Err(SetVersionError::MissingVersion) => Ok(HttpResponse::NotFound().finish()),
    Err(SetVersionError::InvalidVersion) => Ok(HttpResponse::UnprocessableEntity().finish()),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    modules::model::{Module, ModuleType, SetVersionError},
    test_util::parse_http_response,
  };
  use actix_mock_helper::simple_mock_actor;
  use actix_web::http;
  use chrono::Utc;

  #[actix_rt::test]
  async fn can_patch() {
    let mock_actor = simple_mock_actor::<RealDbActor, PatchModule, _>(|msg| {
      Ok(Module {
        module_identifier: msg.module_identifier.clone(),
        module_type: ModuleType::App,
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: Some(msg.current_version_uuid),
      })
    });
    let version_uuid = Uuid::new_v4();

    let response = patch(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test_mod".to_owned(),
      }),
      web::Json(PatchModuleRequest {
        current_version_uuid: version_uuid.clone(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(
      parse_http_response::<Module>(&response).current_version_uuid,
      Some(version_uuid)
    )
  }

  #[actix_rt::test]
  async fn patch_404s_on_missing_version() {
    let mock_actor = simple_mock_actor::<RealDbActor, PatchModule, _>(|_msg| Err(SetVersionError::MissingVersion));
    let version_uuid = Uuid::new_v4();

    let response = patch(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test_mod".to_owned(),
      }),
      web::Json(PatchModuleRequest {
        current_version_uuid: version_uuid.clone(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::NOT_FOUND);
  }

  #[actix_rt::test]
  async fn patch_422s_on_invalid_version() {
    let mock_actor = simple_mock_actor::<RealDbActor, PatchModule, _>(|_msg| Err(SetVersionError::InvalidVersion));
    let version_uuid = Uuid::new_v4();

    let response = patch(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test_mod".to_owned(),
      }),
      web::Json(PatchModuleRequest {
        current_version_uuid: version_uuid.clone(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::UNPROCESSABLE_ENTITY);
  }
}
