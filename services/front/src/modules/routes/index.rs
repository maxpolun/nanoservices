use crate::{actors::DbActor, modules::messages::IndexModules};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn index(db: web::Data<Addr<DbActor>>) -> Result<HttpResponse> {
  match db.send(IndexModules).await? {
    Ok(mods) => Ok(HttpResponse::Ok().json(mods)),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    modules::model::{Module, ModuleType},
    test_util::parse_http_response,
    actors::RealDbActor
  };
  use actix_web::http;
  use chrono::Utc;

  #[actix_rt::test]
  async fn can_index() {
    let mods = (0..100)
      .map(|i| Module {
        module_identifier: format!("test_mod{}", i),
        module_type: ModuleType::App,
        created_at: Utc::now(),
        updated_at: Utc::now(),
        current_version_uuid: None,
      })
      .collect::<Vec<_>>();

    let mods_clone = mods.clone();
    let mock_actor = actix_mock_helper::simple_mock_actor::<RealDbActor, IndexModules, _>(move |_msg| {
      Ok(mods_clone.clone())
    });

    let response = index(web::Data::new(mock_actor)).await.unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Vec<Module>>(&response), mods)
  }
}
