table! {
    use diesel::sql_types::*;
    use crate::modules::model::Module_type_enum;

    assets (hash_sha256) {
        hash_sha256 -> Text,
        contents -> Bytea,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::modules::model::Module_type_enum;

    module_versions (uuid) {
        uuid -> Uuid,
        module_identifier -> Text,
        gitref -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        entry_point_asset_uuid -> Nullable<Uuid>,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::modules::model::Module_type_enum;

    modules (module_identifier) {
        module_identifier -> Text,
        module_type -> Module_type_enum,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        current_version_uuid -> Nullable<Uuid>,
    }
}

table! {
    use diesel::sql_types::*;
    use crate::modules::model::Module_type_enum;

    version_assets (uuid) {
        uuid -> Uuid,
        asset_hash -> Text,
        version_uuid -> Uuid,
        name -> Text,
        mime_type -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(version_assets -> assets (asset_hash));

allow_tables_to_appear_in_same_query!(assets, module_versions, modules, version_assets,);
