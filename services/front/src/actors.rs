use crate::util::Pool;
#[cfg(test)]
use actix::actors::mocker::Mocker;
use actix::prelude::*;

#[cfg(not(test))]
pub type DbActor = RealDbActor;
#[cfg(test)]
pub type DbActor = Mocker<RealDbActor>;

pub struct RealDbActor {
  pub pool: Pool,
}

impl RealDbActor {
  pub fn new(pool: Pool) -> Self {
    Self { pool }
  }
}

impl Actor for RealDbActor {
  type Context = SyncContext<Self>;
}
