use crate::{
  assets::model::VersionAsset,
  schema::*,
  util::{ErrorDescription, Timestamp},
};
use actix_web::{dev::HttpResponseBuilder, http::StatusCode, ResponseError};
use diesel::{prelude::*, PgConnection};
use serde::{Deserialize, Serialize};
use std::str::FromStr;
use thiserror::Error;
use uuid::Uuid;

#[derive(Debug, Queryable, Clone, Serialize, Deserialize, PartialEq, Eq)]
pub struct ModuleVersion {
  pub uuid: Uuid,
  pub module_identifier: String,
  pub gitref: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
  pub entry_point_asset_uuid: Option<Uuid>,
}

#[derive(Debug, Insertable, Clone, Serialize, Deserialize)]
#[table_name = "module_versions"]
pub struct NewModuleVersion {
  pub module_identifier: String,
  pub gitref: String,
}

#[derive(Debug, Error)]
pub enum ModuleVersionCreateError {
  #[error("invalid gitref: {0}")]
  BadGitref(&'static str),
  #[error("database error: {0}")]
  DbError(#[from] diesel::result::Error),
}

#[derive(Debug, Error)]
pub enum SetAssetError {
  #[error("Asset is not for this module version")]
  InvalidAsset,
  #[error("Asset does not exist")]
  MissingAsset,
  #[error("Asset must be of type 'application/javascript'")]
  WrongAssetType,
  #[error("database error: {0}")]
  DbError(#[from] diesel::result::Error),
}

impl From<&SetAssetError> for ErrorDescription {
  fn from(e: &SetAssetError) -> Self {
    let message = format!("{}", e);
    match e {
      SetAssetError::InvalidAsset => ErrorDescription {
        error_id: "front::version::invalid_asset".to_owned(),
        message,
      },
      SetAssetError::MissingAsset => ErrorDescription {
        error_id: "front::version::missing_asset".to_owned(),
        message,
      },
      SetAssetError::WrongAssetType => ErrorDescription {
        error_id: "front::version::wrong_asset_type".to_owned(),
        message,
      },
      SetAssetError::DbError(_) => ErrorDescription {
        error_id: "common::db_error::unknown".to_owned(),
        message,
      },
    }
  }
}

impl ResponseError for SetAssetError {
  fn status_code(&self) -> actix_web::http::StatusCode {
    match self {
      SetAssetError::DbError(_) => StatusCode::INTERNAL_SERVER_ERROR,
      _ => StatusCode::UNPROCESSABLE_ENTITY,
    }
  }
  fn error_response(&self) -> actix_web::HttpResponse {
    let mut resp = HttpResponseBuilder::new(self.status_code());
    resp.json(ErrorDescription::from(self))
  }
}

impl ModuleVersion {
  pub fn create(conn: &PgConnection, new_version: NewModuleVersion) -> Result<ModuleVersion, ModuleVersionCreateError> {
    if new_version.gitref.chars().any(|c| c.is_whitespace()) {
      return Err(ModuleVersionCreateError::BadGitref(
        "gitref must not contain any whitespace",
      ));
    }
    let split_ref = new_version.gitref.split('#').collect::<Vec<_>>();
    if split_ref.len() != 2 {
      return Err(ModuleVersionCreateError::BadGitref(
        "gitref must contain exactly one # to seperate the repo from the branch/commit",
      ));
    }

    Ok(
      diesel::insert_into(module_versions::table)
        .values(new_version)
        .get_result(conn)?,
    )
  }

  pub fn get_by_uuid(conn: &PgConnection, uuid: Uuid) -> Result<ModuleVersion, diesel::result::Error> {
    module_versions::table.find(uuid).get_result(conn)
  }

  pub fn get_all_by_module(
    conn: &PgConnection,
    module_identifier: &str,
  ) -> Result<Vec<ModuleVersion>, diesel::result::Error> {
    module_versions::table
      .filter(module_versions::module_identifier.eq(module_identifier))
      .get_results(conn)
  }

  pub fn set_entry_point_asset(
    &self,
    conn: &PgConnection,
    asset: &VersionAsset,
  ) -> Result<ModuleVersion, SetAssetError> {
    if asset.version_uuid != self.uuid {
      return Err(SetAssetError::InvalidAsset);
    }
    if mime::Mime::from_str(&asset.mime_type).unwrap_or(mime::APPLICATION_OCTET_STREAM) != mime::APPLICATION_JAVASCRIPT
    {
      return Err(SetAssetError::WrongAssetType);
    }
    Ok(
      diesel::update(module_versions::table.find(self.uuid))
        .set(module_versions::entry_point_asset_uuid.eq(asset.uuid))
        .get_result(conn)?,
    )
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    assets::model::{NewVersionAsset, VersionAsset},
    modules::model::{Module, ModuleType, NewModule},
    test_util::test_connection,
  };

  fn make_mod(conn: &PgConnection) -> Module {
    Module::create(
      conn,
      NewModule {
        module_identifier: "test".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap()
  }

  fn make_mod_and_version(conn: &PgConnection) -> (Module, ModuleVersion) {
    let m = make_mod(conn);
    let v = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: m.module_identifier.clone(),
        gitref: "git@gitlab.com/maxpolun/nanoservices.git#branch_name".to_owned(),
      },
    )
    .unwrap();
    (m, v)
  }

  fn make_asset(conn: &PgConnection, version: &ModuleVersion) -> VersionAsset {
    VersionAsset::create(
      conn,
      version.uuid,
      NewVersionAsset {
        name: "js-e7fb2f4978d27e4f9e23fe22cea20bb3da1632fabb50362e2963c68700a6f1a5.js".to_owned(),
        mime_type: "application/javascript".to_owned(),
        contents: "Y29uc29sZS5sb2coImhlbGxvIHdvcmxkIik=".to_owned(),
      },
    )
    .unwrap()
  }

  #[test]
  fn can_create() {
    let conn = test_connection();
    let m = make_mod(&conn);
    let v = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: m.module_identifier.clone(),
        gitref: "git@gitlab.com/maxpolun/nanoservices.git#branch_name".to_owned(),
      },
    )
    .unwrap();

    pretty_assertions::assert_eq!(v.module_identifier, m.module_identifier)
  }

  #[test]
  fn created_must_point_to_valid_module() {
    let conn = test_connection();
    make_mod(&conn);

    let v = ModuleVersion::create(
      &conn,
      NewModuleVersion {
        module_identifier: "different".to_owned(),
        gitref: "git@gitlab.com/maxpolun/nanoservices.git#branch_name".to_owned(),
      },
    );

    assert!(v.is_err())
  }

  #[test]
  fn create_will_error_with_invalid_gitref() {
    let conn = test_connection();
    let m = make_mod(&conn);

    let bad_gitrefs = vec![
      "",
      "test",
      "git@gitlab.com:maxpolun/nanoservices",
      "git@gitlab.com:maxpolun/nanoservices # branchname",
    ];
    for gitref in bad_gitrefs {
      let v = ModuleVersion::create(
        &conn,
        NewModuleVersion {
          module_identifier: m.module_identifier.clone(),
          gitref: gitref.to_owned(),
        },
      );
      assert!(matches!(v, Err(ModuleVersionCreateError::BadGitref(_))))
    }
  }

  #[test]
  fn get_from_uuid_works() {
    let conn = test_connection();
    let (_m, v) = make_mod_and_version(&conn);

    let v2 = ModuleVersion::get_by_uuid(&conn, v.uuid).unwrap();
    pretty_assertions::assert_eq!(v, v2)
  }

  #[test]
  fn returns_not_found_with_wrong_uuid() {
    let conn = test_connection();
    make_mod_and_version(&conn);

    let res = ModuleVersion::get_by_uuid(&conn, Uuid::new_v4());
    assert!(matches!(res, Err(diesel::result::Error::NotFound)))
  }

  #[test]
  fn get_all_returns_all_versions_for_a_mod() {
    let conn = test_connection();
    let m = make_mod(&conn);
    for i in 0..100 {
      ModuleVersion::create(
        &conn,
        NewModuleVersion {
          module_identifier: m.module_identifier.clone(),
          gitref: format!("git@gitlab.com:nanoservices#{}", i),
        },
      )
      .unwrap();
    }

    let m2 = Module::create(
      &conn,
      NewModule {
        module_identifier: "test2".to_owned(),
        module_type: ModuleType::App,
      },
    )
    .unwrap();
    for i in 0..100 {
      ModuleVersion::create(
        &conn,
        NewModuleVersion {
          module_identifier: m2.module_identifier.clone(),
          gitref: format!("git@gitlab.com:nanoservices#{}", i),
        },
      )
      .unwrap();
    }

    pretty_assertions::assert_eq!(
      ModuleVersion::get_all_by_module(&conn, &m.module_identifier)
        .unwrap()
        .len(),
      100
    )
  }

  #[test]
  fn get_all_returns_empty_array_if_module_uuid_does_not_exist() {
    let conn = test_connection();
    let m = make_mod(&conn);
    for i in 0..100 {
      ModuleVersion::create(
        &conn,
        NewModuleVersion {
          module_identifier: m.module_identifier.clone(),
          gitref: format!("git@gitlab.com:nanoservices#{}", i),
        },
      )
      .unwrap();
    }

    pretty_assertions::assert_eq!(
      ModuleVersion::get_all_by_module(&conn, "different_ident")
        .unwrap()
        .len(),
      0
    )
  }

  #[test]
  fn can_set_entry_point() {
    let conn = test_connection();
    let (_m, v) = make_mod_and_version(&conn);
    let a = make_asset(&conn, &v);

    pretty_assertions::assert_eq!(
      v.set_entry_point_asset(&conn, &a).unwrap().entry_point_asset_uuid,
      Some(a.uuid)
    )
  }
}
