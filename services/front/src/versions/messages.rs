use super::model::{ModuleVersion, ModuleVersionCreateError, NewModuleVersion, SetAssetError};
use crate::{actors::RealDbActor, assets::model::VersionAsset};
use actix::{Handler, Message};
use uuid::Uuid;

#[derive(Debug)]
pub struct CreateModuleVersion(pub NewModuleVersion);

impl Message for CreateModuleVersion {
  type Result = Result<ModuleVersion, ModuleVersionCreateError>;
}

impl Handler<CreateModuleVersion> for RealDbActor {
  type Result = Result<ModuleVersion, ModuleVersionCreateError>;
  fn handle(&mut self, msg: CreateModuleVersion, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    ModuleVersion::create(&conn, msg.0)
  }
}

#[derive(Debug)]
pub struct GetVersionById(pub Uuid);

impl Message for GetVersionById {
  type Result = Result<ModuleVersion, diesel::result::Error>;
}

impl Handler<GetVersionById> for RealDbActor {
  type Result = Result<ModuleVersion, diesel::result::Error>;
  fn handle(&mut self, msg: GetVersionById, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    ModuleVersion::get_by_uuid(&conn, msg.0)
  }
}

#[derive(Debug)]
pub struct GetAllVersionsForModule {
  pub module_identifier: String,
}

impl Message for GetAllVersionsForModule {
  type Result = Result<Vec<ModuleVersion>, diesel::result::Error>;
}

impl Handler<GetAllVersionsForModule> for RealDbActor {
  type Result = Result<Vec<ModuleVersion>, diesel::result::Error>;
  fn handle(&mut self, msg: GetAllVersionsForModule, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    ModuleVersion::get_all_by_module(&conn, &msg.module_identifier)
  }
}

#[derive(Debug)]
pub struct SetVersionEntryPoint {
  pub version_uuid: Uuid,
  pub asset_uuid: Uuid,
}

impl Message for SetVersionEntryPoint {
  type Result = Result<ModuleVersion, SetAssetError>;
}

impl Handler<SetVersionEntryPoint> for RealDbActor {
  type Result = Result<ModuleVersion, SetAssetError>;
  fn handle(&mut self, msg: SetVersionEntryPoint, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let v = ModuleVersion::get_by_uuid(&conn, msg.version_uuid)?;
    let a = VersionAsset::get_by_uuid(&conn, msg.asset_uuid).map_err(|e| match e {
      diesel::result::Error::NotFound => SetAssetError::MissingAsset,
      _ => SetAssetError::DbError(e),
    })?;
    v.set_entry_point_asset(&conn, &a)
  }
}
