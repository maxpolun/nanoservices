use crate::{actors::DbActor, util::PathWithModVersion, versions::messages::SetVersionEntryPoint};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct PatchVersionRequestBody {
  pub entry_point_asset_uuid: Uuid,
}

pub async fn patch(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<PathWithModVersion>,
  body: web::Json<PatchVersionRequestBody>,
) -> Result<HttpResponse> {
  let version = db
    .send(SetVersionEntryPoint {
      version_uuid: path.version_uuid,
      asset_uuid: body.entry_point_asset_uuid,
    })
    .await??;

  Ok(HttpResponse::Ok().json(version))
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{actors::RealDbActor, test_util::parse_http_response, versions::model::ModuleVersion};
  use actix_mock_helper::simple_mock_actor;
  use actix_web::http;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn can_patch() {
    let mock_actor = simple_mock_actor::<RealDbActor, SetVersionEntryPoint, _>(|msg| {
      Ok(ModuleVersion {
        uuid: msg.version_uuid,
        module_identifier: "test_module".to_owned(),
        gitref: "git@gitlab.com/mpolun:nanoservices#commit".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
        entry_point_asset_uuid: Some(msg.asset_uuid),
      })
    });
    let version_uuid = Uuid::new_v4();
    let asset_uuid = Uuid::new_v4();
    let response = patch(
      web::Data::new(mock_actor),
      web::Path::from(PathWithModVersion {
        module_identifier: "test".to_owned(),
        version_uuid,
      }),
      web::Json(PatchVersionRequestBody {
        entry_point_asset_uuid: asset_uuid,
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(
      parse_http_response::<ModuleVersion>(&response).entry_point_asset_uuid,
      Some(asset_uuid)
    );
  }
}
