use crate::{actors::DbActor, util::PathWithModVersion, versions::messages::GetVersionById};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn get(db: web::Data<Addr<DbActor>>, path: web::Path<PathWithModVersion>) -> Result<HttpResponse> {
  match db.send(GetVersionById(path.version_uuid)).await? {
    Ok(version) => Ok(HttpResponse::Ok().json(version)),
    Err(diesel::result::Error::NotFound) => Ok(HttpResponse::NotFound().finish()),
    Err(_) => todo!(),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    test_util::parse_http_response,
    versions::{messages::GetVersionById, model::ModuleVersion},
  };
  use actix_mock_helper::simple_mock_actor;
  use actix_web::http;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn can_get() {
    let mock_actor = simple_mock_actor::<RealDbActor, GetVersionById, _>(|msg: &GetVersionById| {
      Ok(ModuleVersion {
        uuid: msg.0,
        module_identifier: "test_module".to_owned(),
        gitref: "git@gitlab.com/mpolun:nanoservices#commit".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
        entry_point_asset_uuid: None,
      })
    });
    let uuid = Uuid::new_v4();
    let response = get(
      web::Data::new(mock_actor),
      web::Path::from(PathWithModVersion {
        module_identifier: "test".to_owned(),
        version_uuid: uuid,
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<ModuleVersion>(&response).uuid, uuid);
  }

  #[actix_rt::test]
  async fn error_404_when_not_found() {
    let mock_actor = simple_mock_actor::<RealDbActor, GetVersionById, _>(|_msg| Err(diesel::result::Error::NotFound));
    let uuid = Uuid::new_v4();
    let response = get(
      web::Data::new(mock_actor),
      web::Path::from(PathWithModVersion {
        module_identifier: "test".to_owned(),
        version_uuid: uuid,
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::NOT_FOUND);
  }
}
