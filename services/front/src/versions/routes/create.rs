use crate::{
  actors::DbActor,
  versions::{
    messages::CreateModuleVersion,
    model::{ModuleVersionCreateError, NewModuleVersion},
  },
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn create(db: web::Data<Addr<DbActor>>, new_mod: web::Json<NewModuleVersion>) -> Result<HttpResponse> {
  match db.send(CreateModuleVersion(new_mod.into_inner())).await? {
    Ok(module_version) => Ok(HttpResponse::Ok().json(module_version)),
    Err(ModuleVersionCreateError::BadGitref(_s)) => Ok(HttpResponse::UnprocessableEntity().finish()),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{actors::RealDbActor, test_util::parse_http_response, versions::model::ModuleVersion};
  use actix_mock_helper::simple_mock_actor;
  use actix_web::http;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn can_create() {
    let mock_actor = simple_mock_actor::<RealDbActor, CreateModuleVersion, _>(|msg: &CreateModuleVersion| {
      Ok(ModuleVersion {
        uuid: Uuid::parse_str("79651c23-382a-4c02-8fa4-4ab138b1f83a").unwrap(),
        module_identifier: msg.0.module_identifier.clone(),
        gitref: msg.0.gitref.clone(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
        entry_point_asset_uuid: None,
      })
    });
    let new_module_version = NewModuleVersion {
      module_identifier: "test".to_owned(),
      gitref: "git@gitlab.com:maxpolun/nanoservices#branch".to_owned(),
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module_version))
      .await
      .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(
      parse_http_response::<ModuleVersion>(&response).module_identifier,
      "test"
    )
  }

  #[actix_rt::test]
  async fn cannot_create_with_bad_gitref() {
    let mock_actor = simple_mock_actor::<RealDbActor, CreateModuleVersion, _>(|_msg: &CreateModuleVersion| {
      Err(ModuleVersionCreateError::BadGitref("gitref is bad"))
    });
    let new_module_version = NewModuleVersion {
      module_identifier: "test".to_owned(),
      gitref: "git@gitlab.com:maxpolun/nanoservicesbranch".to_owned(),
    };
    let response = create(web::Data::new(mock_actor), web::Json(new_module_version))
      .await
      .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::UNPROCESSABLE_ENTITY);
  }
}
