use crate::{
  actors::DbActor, modules::messages::GetModuleById, util::PathWithMod, versions::messages::GetAllVersionsForModule,
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};

pub async fn index(db: web::Data<Addr<DbActor>>, path: web::Path<PathWithMod>) -> Result<HttpResponse> {
  let module_identifier = path.into_inner().module_identifier;
  if let Err(diesel::result::Error::NotFound) = db.send(GetModuleById(module_identifier.clone())).await? {
    return Ok(HttpResponse::NotFound().finish());
  }
  match db
    .send(GetAllVersionsForModule {
      module_identifier: module_identifier,
    })
    .await?
  {
    Ok(mods) => Ok(HttpResponse::Ok().json(mods)),
    Err(diesel::result::Error::NotFound) => Ok(HttpResponse::NotFound().finish()),
    Err(_) => todo!(),
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::RealDbActor,
    modules::model::{Module, ModuleType},
    versions::{messages::GetAllVersionsForModule, model::ModuleVersion},
  };
  use actix_mock_helper::{simple_mock_actor, MockActorSequence};
  use actix_web::http;
  use chrono::Utc;
  use uuid::Uuid;

  #[actix_rt::test]
  async fn index_200_when_module_exists() {
    let mock_actor = MockActorSequence::new()
      .msg::<GetModuleById, _>(|m| {
        Ok(Module {
          module_identifier: m.0.clone(),
          module_type: ModuleType::App,
          created_at: Utc::now(),
          updated_at: Utc::now(),
          current_version_uuid: None,
        })
      })
      .msg::<GetAllVersionsForModule, _>(|_msg| {
        let mut mods = Vec::new();
        for _i in 0..100 {
          mods.push(ModuleVersion {
            uuid: Uuid::new_v4(),
            module_identifier: "test".to_owned(),
            gitref: "git@gitlab.com:maxpolun/nanoservices".to_owned(),
            created_at: Utc::now(),
            updated_at: Utc::now(),
            entry_point_asset_uuid: None,
          });
        }
        Ok(mods)
      })
      .build::<RealDbActor>();

    let result = index(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test".to_owned(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(result.status(), http::StatusCode::OK)
  }

  #[actix_rt::test]
  async fn index_404_when_module_does_not_exist() {
    let mock_actor = simple_mock_actor::<RealDbActor, GetModuleById, _>(|_msg| Err(diesel::result::Error::NotFound));

    let result = index(
      web::Data::new(mock_actor),
      web::Path::from(PathWithMod {
        module_identifier: "test".to_owned(),
      }),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(result.status(), http::StatusCode::NOT_FOUND)
  }
}
