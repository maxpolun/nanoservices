openapi: '3.0.2'
info:
  title: Disco
  version: '1.0'
servers:
  - url: https://disco.maxpolun.com

components:
  securitySchemes:
    jwt:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    ResourceRegistration:
      type: object
      required:
        - resource_name
      properties:
        resource_name:
          type: string
    Resource:
      type: object
      required:
        - resource_name
        - service_id
        - created_at
        - updated_at
      properties:
        resource_name:
          type: string
        service_id:
          type: string
        created_at:
          type: string
          format: timestamp
        updated_at:
          type: string
          format: timestamp
    DiscoService:
      type: object
      properties:
        id:
          type: string
        base_url:
          type: string
          format: url
        created_at:
          type: string
          format: timestamp
        updated_at:
          type: string
          format: timestamp
    NewDiscoService:
      type: object
      properties:
        id:
          type: string
        base_url:
          type: string
          format: url
    Endpoint:
      type: object
      properties:
        id:
          type: string
        service_id:
          type: string
        method:
          type: string
          enum:
            - GET
            - PUT
            - POST
            - PATCH
            - DELETE
        url_template:
          type: string
        resource_name:
          type: string
        created_at:
          type: string
          format: timestamp
        updated_at:
          type: string
          format: timestamp
    NewEndpoint:
      type: object
      properties:
        id:
          type: string
        service_id:
          type: string
        method:
          type: string
          enum:
            - GET
            - PUT
            - POST
            - PATCH
            - DELETE
        url_template:
          type: string
        resource_name:
          type: string

paths:
  /services:
    post:
      operationId: services.create
      description: create or update a disco service
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewDiscoService'
      responses:
        200:
          description: successfully created service
    get:
      operationId: services.index
      description: get all services
      responses:
        200:
          description: successfully got all services
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/DiscoService'
  /services/{service_id}:
    get:
      operationId: services.get
      responses:
        200:
          description: successfully got the service
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/DiscoService'
  /services/{service_id}/resources:
    post:
      operationId: resource.create
      description: create or update a resource registration
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/ResourceRegistration'
      responses:
        200:
          description: successfully created or updated a resource registration
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResourceRegistration'
        400:
          description: one or more required fields is missing
    get:
      operationId: resource.index
      description: get all resources
      responses:
        200:
          description: successfully fetched resources
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/ResourceRegistration'
  /services/{service_id}/resources/{resource_name}:
    get:
      operationId: resource.get
      description: get a resource by resource_name
      parameters:
        - name: resource_name
          in: path
          required: true
          schema:
            type: string
      responses:
        200:
          description: successfully got the resource
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ResourceRegistration'
        404:
          description: no resource with that name found
  /services/{service_id}/endpoints:
    post:
      operationId: endpoints.create
      description: create a disco endpoint
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewEndpoint'
      responses:
        200:
          description: successfully created endpoint
    get:
      operationId: endpoints.index
      description: get all endpoints
      responses:
        200:
          description: successfully got all endpoints for this service
          content:
            application/json:
              schema:
                type: array
                items:
                  $ref: '#/components/schemas/Endpoint'
  /services/{service_id}/endpoints/{endpoints_id}:
    get:
      operationId: endpoints.get
      responses:
        200:
          description: successfully got the endpoint
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/Endpoint'

security:
  - jwt: []
