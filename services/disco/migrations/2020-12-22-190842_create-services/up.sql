CREATE TABLE IF NOT EXISTS services (
  id TEXT PRIMARY KEY,
  base_url TEXT NOT NULL,
  created_at TIMESTAMPTZ DEFAULT NOW() NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT NOW() NOT NULL
);

SELECT diesel_manage_updated_at('services')
