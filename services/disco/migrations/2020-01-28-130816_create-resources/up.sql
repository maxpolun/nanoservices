-- Your SQL goes here
CREATE TABLE resources (
  name TEXT PRIMARY KEY,
  base_url TEXT UNIQUE NOT NULL,
  created_at TIMESTAMPTZ DEFAULT NOW() NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT NOW() NOT NULL
);

SELECT diesel_manage_updated_at('resources')
