CREATE TABLE IF NOT EXISTS endpoints (
  service_id TEXT NOT NULL REFERENCES services(id),
  id TEXT NOT NULL,
  method TEXT NOT NULL CHECK (method in ('GET', 'POST', 'PUT', 'PATCH', 'DELETE')),
  url_template TEXT NOT NULL,
  resource_name TEXT NOT NULL REFERENCES resources(name),
  created_at TIMESTAMPTZ DEFAULT NOW() NOT NULL,
  updated_at TIMESTAMPTZ DEFAULT NOW() NOT NULL,

  PRIMARY KEY (service_id, id),
  UNIQUE (service_id, id, method, url_template)
);

SELECT diesel_manage_updated_at('endpoints')
