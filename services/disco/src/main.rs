use actix_auth_service::{AppAccess, JwtVerify};
use actix_web::{App, HttpServer};
use diesel::{r2d2::ConnectionManager, PgConnection};
use discolib::{config_api, StaticConfig};
use dotenv::dotenv;
use nano_trace::{RequestTracing, TracingLogger};

#[actix_rt::main]
async fn main() {
  dotenv().ok();

  let _tracer = nano_trace::init();
  let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");
  let manager = ConnectionManager::<PgConnection>::new(&database_url);
  let pool = r2d2::Pool::builder().build(manager).expect("Failed to create pool.");
  let verify = JwtVerify::default();
  HttpServer::new(move || {
    App::new()
      .wrap(AppAccess)
      .wrap(verify.clone())
      .wrap(TracingLogger)
      .wrap(RequestTracing::new())
      .configure(|cfg| {
        let db_connection_count = std::env::var("DB_CONNECTIONS").unwrap_or_default().parse().unwrap_or(3);
        config_api(
          cfg,
          StaticConfig {
            pool: pool.clone(),
            db_connection_count,
          },
        )
      })
  })
  .bind((
    "0.0.0.0",
    std::env::var("PORT")
      .and_then(|port_str| port_str.parse::<u16>().map_err(|_| std::env::VarError::NotPresent))
      .unwrap_or(8502),
  ))
  .unwrap()
  .run()
  .await
  .unwrap();
}
