table! {
    endpoints (service_id, id) {
        service_id -> Text,
        id -> Text,
        method -> Text,
        url_template -> Text,
        resource_name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    resources (name) {
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        service_id -> Text,
    }
}

table! {
    services (id) {
        id -> Text,
        base_url -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(endpoints -> resources (resource_name));
joinable!(endpoints -> services (service_id));
joinable!(resources -> services (service_id));

allow_tables_to_appear_in_same_query!(endpoints, resources, services,);
