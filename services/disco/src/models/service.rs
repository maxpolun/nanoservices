use diesel::{pg::upsert::excluded, prelude::*, PgConnection};
use serde::{Deserialize, Serialize};
use url::Url;

use crate::{schema::*, util::Timestamp};

#[derive(Debug, Queryable, Clone, Serialize, Deserialize)]
pub struct Service {
  pub id: String,
  pub base_url: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

impl Service {
  pub fn create(conn: &PgConnection, ns: NewService) -> Result<Self, diesel::result::Error> {
    Ok(
      diesel::insert_into(services::table)
        .values(ns)
        .on_conflict(services::id)
        .do_update()
        .set(services::base_url.eq(excluded(services::base_url)))
        .get_result(conn)?,
    )
  }
}

#[derive(Debug, Insertable, Deserialize)]
#[table_name = "services"]
pub struct NewService {
  pub id: String,
  pub base_url: String,
}

impl NewService {
  pub fn new(id: String, base_url: Url) -> Self {
    Self {
      id,
      base_url: base_url.to_string(),
    }
  }
}
