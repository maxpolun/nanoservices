use crate::{
  schema::{endpoints, services},
  util::{valid_name, Timestamp},
};
use actix_http::Result;
use diesel::{prelude::*, PgConnection};
use serde::Serialize;
use thiserror::Error;

use super::service::Service;

#[derive(Debug, Queryable, Clone, Serialize)]
pub struct Endpoint {
  pub service_id: String,
  pub id: String,
  pub method: String,
  pub url_template: String,
  pub resource_name: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

#[derive(Debug, Insertable)]
#[table_name = "endpoints"]
pub struct NewEndpoint {
  pub service_id: String,
  pub id: String,
  pub method: String,
  pub url_template: String,
  pub resource_name: String,
}

#[derive(Debug, Error)]
pub enum CreateEndpointError {
  #[error("invalid endpoint id {0}")]
  InvalidId(String),
  #[error("Unable to find service with service_id {0}")]
  MissingService(String),
  #[error("invalid endpoint http method, only GET, POST, PUT, PATCH, or DELETE supported. Got {0}")]
  InvalidMethod(String),
  #[error("invalid endpoint url template {0}")]
  InvalidUrlTemplate(#[from] url::ParseError),
  #[error("database error while saving endpoint {0}")]
  DbError(#[from] diesel::result::Error),
}

const ALLOWED_METHODS: [&str; 5] = ["GET", "POST", "PUT", "PATCH", "DELETE"];

impl Endpoint {
  pub fn create(conn: &PgConnection, new_endpoint: NewEndpoint) -> Result<Self, CreateEndpointError> {
    if !valid_name(&new_endpoint.id) {
      return Err(CreateEndpointError::InvalidId(new_endpoint.id));
    }
    if !ALLOWED_METHODS.contains(&new_endpoint.method.to_ascii_uppercase().as_str()) {
      return Err(CreateEndpointError::InvalidMethod(new_endpoint.method));
    }

    let service = services::table
      .find(&new_endpoint.service_id)
      .get_result::<Service>(conn)
      .map_err(|_| CreateEndpointError::MissingService(new_endpoint.service_id.clone()))?;
    url::Url::parse(&service.base_url)?.join(&new_endpoint.url_template)?;
    Ok(
      diesel::insert_into(endpoints::table)
        .values(new_endpoint)
        .get_result(conn)?,
    )
  }

  pub fn get_all(conn: &PgConnection, service_id: &str) -> Result<Vec<Self>, diesel::result::Error> {
    Ok(
      endpoints::table
        .filter(endpoints::service_id.eq(service_id))
        .get_results(conn)?,
    )
  }

  pub fn get_by_id(conn: &PgConnection, service_id: &str, endpoint_id: &str) -> Result<Self, diesel::result::Error> {
    Ok(endpoints::table.find((service_id, endpoint_id)).get_result(conn)?)
  }

  pub fn delete_by_id(conn: &PgConnection, service_id: &str, endpoint_id: &str) -> Result<(), diesel::result::Error> {
    diesel::delete(endpoints::table.find((service_id, endpoint_id))).execute(conn)?;
    Ok(())
  }
}
