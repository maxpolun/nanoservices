use diesel::{prelude::*, PgConnection};
use serde::{Deserialize, Serialize};
use thiserror::Error;

use crate::{
  schema::*,
  util::{valid_name, Timestamp},
};

#[derive(Debug, Error)]
pub enum ResourceError {
  #[error("resource does not have matching service")]
  MissingService,
  #[error("invalid resource name, it must be ascii alphanumeric with underscores allowed")]
  BadName,
  #[error("database error: {0}")]
  DatabaseError(#[from] diesel::result::Error),
}

#[derive(Debug, Queryable, Clone, Serialize, Deserialize)]
pub struct Resource {
  pub name: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
  pub service_id: String,
}

#[derive(Debug, Insertable, Deserialize, Serialize)]
#[table_name = "resources"]
pub struct NewResource {
  pub name: String,
  pub service_id: String,
}

impl NewResource {
  pub fn validate(&self) -> Result<(), ResourceError> {
    if !valid_name(&self.name) {
      return Err(ResourceError::BadName);
    }
    Ok(())
  }
}

pub trait ResourceRepository {
  fn create_or_update(&self, res: NewResource) -> Result<Resource, ResourceError>;
  fn get_by_name(&self, name: &str) -> Result<Resource, ResourceError>;
  fn get_all(&self, service_id: &str) -> Result<Vec<Resource>, ResourceError>;
  fn delete_by_name(&self, name: &str) -> Result<(), ResourceError>;
}

pub struct RealResourceRepository<'a> {
  conn: &'a PgConnection,
}

impl<'a> RealResourceRepository<'a> {
  pub fn new(conn: &'a PgConnection) -> Self {
    Self { conn }
  }
}

impl<'a> ResourceRepository for RealResourceRepository<'a> {
  fn create_or_update(&self, res: NewResource) -> Result<Resource, ResourceError> {
    res.validate()?;
    Ok(
      diesel::insert_into(resources::table)
        .values(&res)
        .get_result::<Resource>(self.conn)?,
    )
  }

  fn get_by_name(&self, name: &str) -> Result<Resource, ResourceError> {
    Ok(resources::table.find(name).get_result::<Resource>(self.conn)?)
  }

  fn get_all(&self, service_id: &str) -> Result<Vec<Resource>, ResourceError> {
    Ok(
      resources::table
        .filter(resources::service_id.eq(service_id))
        .get_results::<Resource>(self.conn)?,
    )
  }

  fn delete_by_name(&self, name: &str) -> Result<(), ResourceError> {
    diesel::delete(resources::table.find(name)).execute(self.conn)?;
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::util::{create_service, test_connection};

  #[test]
  fn validate_requires_valid_name() {
    let res = NewResource {
      name: "invalid name".to_owned(),
      service_id: "test_service".to_owned(),
    };
    assert!(res.validate().is_err());

    let res = NewResource {
      name: "invalid$name".to_owned(),
      service_id: "test_service".to_owned(),
    };
    assert!(res.validate().is_err());

    let res = NewResource {
      name: "this_name_is_ok".to_owned(),
      service_id: "test_service".to_owned(),
    };
    assert!(res.validate().is_ok());
  }

  #[test]
  fn resource_repository_can_create() {
    let conn = test_connection();
    let service = create_service(&conn, "test_service");
    let repo = RealResourceRepository::new(&conn);
    repo
      .create_or_update(NewResource {
        name: "test".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    assert!(conn.execute("SELECT * FROM resources WHERE name = 'test'").is_ok())
  }

  #[test]
  fn resource_repository_get_if_exists() {
    let conn = test_connection();
    let service = create_service(&conn, "test_service");
    let repo = RealResourceRepository::new(&conn);
    repo
      .create_or_update(NewResource {
        name: "test".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    pretty_assertions::assert_eq!(repo.get_by_name("test").unwrap().service_id, "test_service");
  }

  #[test]
  fn resource_repository_get_errors_if_not_exists() {
    let conn = test_connection();
    let repo = RealResourceRepository::new(&conn);
    assert!(repo.get_by_name("test").is_err());
  }

  #[test]
  fn resource_repository_get_all_gets_all_resources() {
    let conn = test_connection();
    let service = create_service(&conn, "test_service");
    let repo = RealResourceRepository::new(&conn);
    repo
      .create_or_update(NewResource {
        name: "test1".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    repo
      .create_or_update(NewResource {
        name: "test2".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    repo
      .create_or_update(NewResource {
        name: "test3".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    pretty_assertions::assert_eq!(repo.get_all(&service.id).unwrap().len(), 3);
  }

  #[test]
  fn resource_repository_delete_works() {
    let conn = test_connection();
    let service = create_service(&conn, "test_service");
    let repo = RealResourceRepository::new(&conn);
    repo
      .create_or_update(NewResource {
        name: "test".to_owned(),
        service_id: service.id.clone(),
      })
      .unwrap();
    repo.delete_by_name("test").unwrap();
    assert!(repo.get_by_name("test").is_err());
  }

  #[test]
  fn resource_repository_delete_does_nothing_if_not_exists() {
    let conn = test_connection();
    let repo = RealResourceRepository::new(&conn);
    assert!(repo.delete_by_name("test").is_ok());
  }
}
