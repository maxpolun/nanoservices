use crate::models::{
  endpoint::{CreateEndpointError, Endpoint, NewEndpoint},
  resource::{NewResource, RealResourceRepository, Resource, ResourceError, ResourceRepository},
  service::{NewService, Service},
};
#[cfg(test)]
use actix::actors::mocker::Mocker;
use actix::prelude::*;
use tracing::info;

#[cfg(not(test))]
pub type DbActor = RealDbActor;
#[cfg(test)]
pub type DbActor = Mocker<RealDbActor>;

pub struct RealDbActor {
  pool: crate::Pool,
}

impl RealDbActor {
  pub fn new(pool: crate::Pool) -> Self {
    Self { pool }
  }
}

impl actix::Supervised for RealDbActor {
  fn restarting(&mut self, _ctx: &mut SyncContext<RealDbActor>) {
    info!("restarting db actor");
  }
}

impl Actor for RealDbActor {
  type Context = SyncContext<Self>;
}

#[derive(Debug)]
pub struct CreateOrUpdateResource(pub NewResource);

impl Message for CreateOrUpdateResource {
  type Result = Result<Resource, ResourceError>;
}

impl Handler<CreateOrUpdateResource> for RealDbActor {
  type Result = Result<Resource, ResourceError>;
  fn handle(&mut self, msg: CreateOrUpdateResource, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let repo = RealResourceRepository::new(&conn);
    repo.create_or_update(msg.0)
  }
}

#[derive(Debug)]
pub struct GetResourceByName(pub String);

impl Message for GetResourceByName {
  type Result = Result<Resource, ResourceError>;
}

impl Handler<GetResourceByName> for RealDbActor {
  type Result = Result<Resource, ResourceError>;
  fn handle(&mut self, msg: GetResourceByName, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let repo = RealResourceRepository::new(&conn);
    repo.get_by_name(&msg.0)
  }
}

#[derive(Debug)]
pub struct GetAllResources(pub String);

impl Message for GetAllResources {
  type Result = Result<Vec<Resource>, ResourceError>;
}

impl Handler<GetAllResources> for RealDbActor {
  type Result = Result<Vec<Resource>, ResourceError>;
  fn handle(&mut self, msg: GetAllResources, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let repo = RealResourceRepository::new(&conn);
    repo.get_all(&msg.0)
  }
}

#[derive(Debug)]
pub struct DeleteResourceByName(pub String);

impl Message for DeleteResourceByName {
  type Result = Result<(), ResourceError>;
}

impl Handler<DeleteResourceByName> for RealDbActor {
  type Result = Result<(), ResourceError>;
  fn handle(&mut self, msg: DeleteResourceByName, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    let repo = RealResourceRepository::new(&conn);
    repo.delete_by_name(&msg.0)
  }
}

pub struct CreateService(pub NewService);

impl Message for CreateService {
  type Result = Result<Service, diesel::result::Error>;
}

impl Handler<CreateService> for RealDbActor {
  type Result = <CreateService as Message>::Result;

  fn handle(&mut self, msg: CreateService, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Service::create(&conn, msg.0)
  }
}

pub struct CreateEndpoint(pub NewEndpoint);
impl Message for CreateEndpoint {
  type Result = Result<Endpoint, CreateEndpointError>;
}

impl Handler<CreateEndpoint> for RealDbActor {
  type Result = <CreateEndpoint as Message>::Result;

  fn handle(&mut self, msg: CreateEndpoint, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Endpoint::create(&conn, msg.0)
  }
}

pub struct GetEndpointById {
  pub service_id: String,
  pub endpoint_id: String,
}
impl Message for GetEndpointById {
  type Result = Result<Endpoint, diesel::result::Error>;
}

impl Handler<GetEndpointById> for RealDbActor {
  type Result = <GetEndpointById as Message>::Result;

  fn handle(&mut self, msg: GetEndpointById, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Endpoint::get_by_id(&conn, &msg.service_id, &msg.endpoint_id)
  }
}

pub struct GetAllEndpoints(pub String);
impl Message for GetAllEndpoints {
  type Result = Result<Vec<Endpoint>, diesel::result::Error>;
}

impl Handler<GetAllEndpoints> for RealDbActor {
  type Result = <GetAllEndpoints as Message>::Result;

  fn handle(&mut self, msg: GetAllEndpoints, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Endpoint::get_all(&conn, &msg.0)
  }
}

pub struct DeleteEndpointById {
  pub service_id: String,
  pub endpoint_id: String,
}
impl Message for DeleteEndpointById {
  type Result = Result<(), diesel::result::Error>;
}

impl Handler<DeleteEndpointById> for RealDbActor {
  type Result = <DeleteEndpointById as Message>::Result;

  fn handle(&mut self, msg: DeleteEndpointById, _ctx: &mut Self::Context) -> Self::Result {
    let conn = self.pool.get().unwrap();
    Endpoint::delete_by_id(&conn, &msg.service_id, &msg.endpoint_id)
  }
}
