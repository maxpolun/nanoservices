use crate::{
  actors::{CreateOrUpdateResource, DbActor, DeleteResourceByName, GetAllResources, GetResourceByName},
  models::resource::{NewResource, ResourceError},
};
use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use diesel::result::DatabaseErrorKind;
use serde::Deserialize;
use tracing::{error, info};

#[derive(Deserialize)]
pub struct NewResourceRequest {
  pub name: String,
}

pub async fn create_or_update_resource(
  db: web::Data<Addr<DbActor>>,
  service_id: web::Path<String>,
  new_res: web::Json<NewResourceRequest>,
) -> Result<HttpResponse> {
  let name = new_res.into_inner().name;
  match db
    .send(CreateOrUpdateResource(NewResource {
      name,
      service_id: service_id.into_inner(),
    }))
    .await?
  {
    Ok(res) => {
      info!("successfully found resource with name {}", res.name);
      Ok(HttpResponse::Ok().json(res))
    }
    Err(ResourceError::MissingService) | Err(ResourceError::BadName) => {
      info!("new resource is invalid");
      Ok(HttpResponse::UnprocessableEntity().finish())
    }
    Err(ResourceError::DatabaseError(diesel::result::Error::DatabaseError(DatabaseErrorKind::UniqueViolation, e))) => {
      info!("unable to create/update resource -- duplicate row: {:?}", e);
      Ok(HttpResponse::Conflict().finish())
    }
    Err(e) => {
      error!("Unexpected error creating/updating resource: {}", e);
      Ok(HttpResponse::InternalServerError().finish())
    }
  }
}

pub async fn get_resources(db: web::Data<Addr<DbActor>>, service_id: web::Path<String>) -> Result<HttpResponse> {
  match db.send(GetAllResources(service_id.into_inner())).await? {
    Ok(res) => {
      info!("loaded {} resources", res.len());
      Ok(HttpResponse::Ok().json(res))
    }
    Err(e) => {
      error!("error loading resources: {:?}", e);
      Ok(HttpResponse::InternalServerError().finish())
    }
  }
}

pub async fn get_resource_by_name(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<(String, String)>,
) -> Result<HttpResponse> {
  let (_service_id, name) = path.into_inner();
  match db.send(GetResourceByName(name.clone())).await? {
    Ok(res) => {
      info!("successfully got resource for name {}", res.name);
      Ok(HttpResponse::Ok().json(res))
    }
    Err(ResourceError::DatabaseError(diesel::result::Error::NotFound)) => {
      info!("resource name not found: {}", name);
      Ok(HttpResponse::NotFound().finish())
    }
    Err(e) => {
      error!("error when trying to load resource {}: {}", name, e);
      Ok(HttpResponse::InternalServerError().finish())
    }
  }
}

pub async fn delete_resource_by_name(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<(String, String)>,
) -> Result<HttpResponse> {
  let (_service_id, name) = path.into_inner();
  match db.send(DeleteResourceByName(name.clone())).await? {
    Ok(_) => {
      info!("successfully deleted resource {}", name);
      Ok(HttpResponse::Ok().finish())
    }
    Err(e) => {
      error!("error deleting resource {}: {}", name, e);
      Ok(HttpResponse::InternalServerError().finish())
    }
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::{
    actors::{DbActor, RealDbActor},
    models::resource::Resource,
    util::parse_http_response,
  };
  use actix_mock_helper::simple_mock_actor;
  use actix_web::{http, web};
  use chrono::Utc;

  #[actix_rt::test]
  async fn create_or_update_resource_creates_and_returns_resource_if_successfull() {
    let mock_actor = simple_mock_actor::<RealDbActor, CreateOrUpdateResource, _>(|msg| {
      Ok(Resource {
        name: msg.0.name.clone(),
        service_id: "test_service".into(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      })
    });
    let new_resource = NewResourceRequest { name: "test".into() };
    let response = create_or_update_resource(
      web::Data::new(mock_actor),
      web::Path("test_service".into()),
      web::Json(new_resource),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Resource>(&response).name, "test")
  }

  #[actix_rt::test]
  async fn create_or_update_resource_422s_if_bad_params() {
    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, CreateOrUpdateResource, _>(|_msg| {
       Err(ResourceError::BadName)
    });
    let new_resource = NewResourceRequest { name: "a b c".into() };
    let response = create_or_update_resource(
      web::Data::new(mock_actor),
      web::Path("test_service".into()),
      web::Json(new_resource),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::UNPROCESSABLE_ENTITY);

    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, CreateOrUpdateResource, _>(|_msg| {
      Err(ResourceError::MissingService)
    });
    let new_resource = NewResourceRequest {
      name: "test".to_owned(),
    };
    let response = create_or_update_resource(
      web::Data::new(mock_actor),
      web::Path("doesnt_exist".into()),
      web::Json(new_resource),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::UNPROCESSABLE_ENTITY);
  }

  #[actix_rt::test]
  async fn index_gets_all_resources() {
    let mut fake = Vec::new();
    for i in 0..100 {
      fake.push(Resource {
        name: format!("test{}", i),
        service_id: "test_service".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      });
    }
    let fake_clone = fake.clone();
    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, GetAllResources, _>(move |_msg| {
      Ok(fake_clone.clone())
    });
    let response = get_resources(web::Data::new(mock_actor), web::Path("test_service".to_owned()))
      .await
      .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Vec<Resource>>(&response).len(), 100)
  }

  #[actix_rt::test]
  async fn get_by_name_can_get_a_single_resource() {
    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, GetResourceByName, _>(move |_msg| {
      Ok(Resource {
        name: "test".to_owned(),
        service_id: "test_service".to_owned(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      })
    });

    let response = get_resource_by_name(
      web::Data::new(mock_actor),
      ("test_service".into(), "test".into()).into(),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Resource>(&response).name, "test")
  }

  #[actix_rt::test]
  async fn get_by_name_404s_if_not_found() {
    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, GetResourceByName, _>(move |_msg| {
      Err(ResourceError::DatabaseError(diesel::result::Error::NotFound))
    });

    let response = get_resource_by_name(
      web::Data::new(mock_actor),
      ("test_service".into(), "test".into()).into(),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::NOT_FOUND);
  }

  #[actix_rt::test]
  async fn delete_deletes_a_resource() {
    let mock_actor: Addr<DbActor> = simple_mock_actor::<RealDbActor, DeleteResourceByName, _>(move |_msg| {
      Ok(())
    });

    let response = delete_resource_by_name(
      web::Data::new(mock_actor),
      ("test_service".into(), "test".into()).into(),
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(response.status(), http::StatusCode::OK);
  }
}
