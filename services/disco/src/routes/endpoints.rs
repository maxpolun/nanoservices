use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use serde::Deserialize;
use tracing::info;

use crate::{
  actors::{CreateEndpoint, DbActor, DeleteEndpointById, GetAllEndpoints, GetEndpointById},
  models::endpoint::NewEndpoint,
};

#[derive(Deserialize)]
pub struct NewEndpointRequest {
  pub id: String,
  pub method: String,
  pub url_template: String,
  pub resource_name: String,
}

pub async fn create_or_update_endpoint(
  db: web::Data<Addr<DbActor>>,
  service_id: web::Path<String>,
  new_endpoint: web::Json<NewEndpointRequest>,
) -> Result<HttpResponse> {
  let NewEndpointRequest {
    id,
    method,
    url_template,
    resource_name,
  } = new_endpoint.into_inner();
  match db
    .send(CreateEndpoint(NewEndpoint {
      service_id: service_id.0,
      id,
      method,
      url_template,
      resource_name,
    }))
    .await?
  {
    Ok(endpoint) => Ok(HttpResponse::Ok().json(endpoint)),
    Err(err) => {
      info!("error creating endpoint: {}", err);
      Ok(HttpResponse::UnprocessableEntity().finish())
    }
  }
}

pub async fn get_endpoints(db: web::Data<Addr<DbActor>>, service_id: web::Path<String>) -> Result<HttpResponse> {
  match db.send(GetAllEndpoints(service_id.into_inner())).await? {
    Ok(endpoints) => Ok(HttpResponse::Ok().json(endpoints)),
    Err(err) => {
      info!("error getting endpoints: {}", err);
      Ok(HttpResponse::UnprocessableEntity().finish())
    }
  }
}

pub async fn get_endpoint_by_id(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<(String, String)>,
) -> Result<HttpResponse> {
  let (service_id, endpoint_id) = path.into_inner();
  match db
    .send(GetEndpointById {
      service_id,
      endpoint_id,
    })
    .await?
  {
    Ok(endpoint) => Ok(HttpResponse::Ok().json(endpoint)),
    Err(err) => {
      info!("error getting endpoint: {}", err);
      Ok(HttpResponse::UnprocessableEntity().finish())
    }
  }
}

pub async fn delete_endpoint_by_id(
  db: web::Data<Addr<DbActor>>,
  path: web::Path<(String, String)>,
) -> Result<HttpResponse> {
  let (service_id, endpoint_id) = path.into_inner();
  match db
    .send(DeleteEndpointById {
      service_id,
      endpoint_id,
    })
    .await?
  {
    Ok(_) => Ok(HttpResponse::Ok().finish()),
    Err(err) => {
      info!("error deleting endpoint: {}", err);
      Ok(HttpResponse::UnprocessableEntity().finish())
    }
  }
}
