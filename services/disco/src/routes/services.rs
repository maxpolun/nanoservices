use actix::Addr;
use actix_web::{web, HttpResponse, Result};
use web::Json;

use crate::{
  actors::{CreateService, DbActor},
  models::service::NewService,
};

pub async fn create_or_update_service(db: web::Data<Addr<DbActor>>, body: Json<NewService>) -> Result<HttpResponse> {
  let s = db.send(CreateService(body.0)).await?.unwrap();
  Ok(HttpResponse::Ok().json(s))
}
pub async fn index_services() -> Result<HttpResponse> {
  todo!()
}
pub async fn get_service_by_id() -> Result<HttpResponse> {
  todo!()
}

#[cfg(test)]
mod tests {
  use actix_http::http::StatusCode;
  use actix_mock_helper::simple_mock_actor;
  use chrono::Utc;

  use crate::{actors::RealDbActor, models::service::Service, util::parse_http_response};

  use super::*;

  #[actix_rt::test]
  async fn create_or_update_service_works() {
    let mock_actor = simple_mock_actor::<RealDbActor, CreateService, _>(|msg| {
      Ok(Service {
        id: msg.0.id.clone(),
        base_url: msg.0.base_url.clone(),
        created_at: Utc::now(),
        updated_at: Utc::now(),
      })
    });

    let response = create_or_update_service(
      web::Data::new(mock_actor),
      Json(NewService {
        id: "test".to_owned(),
        base_url: "http://example.com/test".to_owned(),
      }),
    )
    .await
    .unwrap();

    pretty_assertions::assert_eq!(response.status(), StatusCode::OK);
    pretty_assertions::assert_eq!(parse_http_response::<Service>(&response).id, "test")
  }
}
