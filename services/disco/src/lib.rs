#[macro_use]
extern crate diesel;
use actix::prelude::SyncArbiter;
use actix_web::web;
use actors::RealDbActor;
use diesel::{
  r2d2::{self, ConnectionManager},
  PgConnection,
};
use routes::{
  endpoints::{create_or_update_endpoint, delete_endpoint_by_id, get_endpoint_by_id, get_endpoints},
  resources::{create_or_update_resource, delete_resource_by_name, get_resource_by_name, get_resources},
  services::{create_or_update_service, get_service_by_id, index_services},
};

mod actors;
mod models;
mod routes;
mod schema;
mod util;

type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Clone)]
pub struct StaticConfig {
  pub pool: Pool,
  pub db_connection_count: usize,
}

pub fn config_api(cfg: &mut web::ServiceConfig, static_config: StaticConfig) {
  let addr = SyncArbiter::start(static_config.db_connection_count, move || {
    RealDbActor::new(static_config.pool.clone())
  });
  cfg.service(
    web::scope("/").data(addr).service(
      web::scope("/services")
        .route("", web::post().to(create_or_update_service))
        .route("", web::get().to(index_services))
        .route("/{service_id}", web::get().to(get_service_by_id))
        .service(
          web::scope("/{service_id}/resources")
            .route("", web::post().to(create_or_update_resource))
            .route("", web::get().to(get_resources))
            .route("/{resource_name}", web::get().to(get_resource_by_name))
            .route("/{resource_name}", web::delete().to(delete_resource_by_name)),
        )
        .service(
          web::scope("/{service_id}/endpoints")
            .route("", web::post().to(create_or_update_endpoint))
            .route("", web::get().to(get_endpoints))
            .route("/{endpoint_name}", web::get().to(get_endpoint_by_id))
            .route("/{endpoint_name}", web::delete().to(delete_endpoint_by_id)),
        ),
    ),
  );
}
