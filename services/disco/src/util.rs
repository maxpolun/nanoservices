#[cfg(test)]
use crate::models::service::{NewService, Service};
#[cfg(test)]
use actix_web::{dev::Body, HttpResponse};
#[cfg(test)]
use diesel::pg::PgConnection;
#[cfg(test)]
use diesel::Connection;
#[cfg(test)]
use serde::Deserialize;
#[cfg(test)]
use std::env;
#[cfg(test)]
use url::Url;

pub type Timestamp = chrono::DateTime<chrono::Utc>;

#[cfg(test)]
pub fn test_connection() -> PgConnection {
  dotenv::dotenv().ok();
  let database_url = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  let conn = PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url));
  conn.begin_test_transaction().unwrap();
  conn
}

#[cfg(test)]
pub fn parse_http_response<'a, T: Deserialize<'a>>(response: &'a HttpResponse) -> T {
  let body = response.body();
  let bytes = match body.as_ref().unwrap() {
    Body::Bytes(b) => b,
    _ => panic!("expected bytes"),
  };
  serde_json::from_slice(bytes.as_ref()).unwrap()
}

pub fn valid_name(name: &str) -> bool {
  name.chars().all(|c| c.is_ascii_alphanumeric() || c == '_')
}

#[cfg(test)]
pub fn create_service(conn: &PgConnection, id: &str) -> Service {
  Service::create(
    conn,
    NewService::new(id.to_owned(), Url::parse("http://test.example.com/").unwrap()),
  )
  .unwrap()
}
