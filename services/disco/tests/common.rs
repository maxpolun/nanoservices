#![allow(dead_code)]
use actix::Actor;
use actix_auth_service::{AuthClientActor, JwtVerify, Subject};
use auth_client::{AuthClient, FakeAuthClient};
use diesel::{r2d2::ConnectionManager, Connection, PgConnection};
use std::env;
use tracing::{debug, trace};
use url::Url;
use uuid::Uuid;

fn db_connstring(dbname: &str, template: &str) -> String {
  let mut parsed = Url::parse(template).unwrap();
  parsed.set_path(dbname);
  parsed.into_string()
}

fn template_db(template: &str) -> String {
  let parsed = Url::parse(template).unwrap();
  parsed.path_segments().unwrap().nth(0).unwrap().to_owned()
}

fn db_name() -> String {
  let uuid = uuid::Uuid::new_v4();
  format!("test_{}", uuid.to_simple())
}

pub struct TestDb {
  template_db: String,
  pub connstring: String,
  db_name: String,
}

impl TestDb {
  fn from_template(template: String) -> Self {
    let conn = PgConnection::establish(&template).unwrap();
    let name = db_name();
    let create_command = format!("CREATE DATABASE {} TEMPLATE {}", name, template_db(&template));
    debug!("executing command \"{}\"", create_command);
    conn.execute(&create_command).unwrap();
    let connstring = db_connstring(&name, &template);
    Self {
      template_db: template,
      db_name: name,
      connstring,
    }
  }

  pub fn conn(&self) -> PgConnection {
    PgConnection::establish(&self.connstring).unwrap()
  }
}

fn sql(conn: &PgConnection, command: &str) {
  trace!("executing sql command: \"{}\"", command);
  conn.execute(command).unwrap();
}

impl Drop for TestDb {
  fn drop(&mut self) {
    let conn = PgConnection::establish(&self.template_db).unwrap();
    let kill_other_connections = format!(
      "SELECT pg_terminate_backend(pg_stat_activity.pid)
        FROM pg_stat_activity
        WHERE pg_stat_activity.datname = '{}'
          AND pid <> pg_backend_pid();",
      &self.db_name
    );
    debug!("executing command \"{}\"", kill_other_connections);
    conn.execute(&kill_other_connections).unwrap();
    let drop_cmd = format!("DROP DATABASE {}", self.db_name);
    debug!("executing command \"{}\"", drop_cmd);
    conn.execute(&drop_cmd).unwrap();
  }
}

pub fn test_db() -> TestDb {
  dotenv::dotenv().ok();
  let template_db = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  TestDb::from_template(template_db)
}

pub fn static_config(db: &TestDb) -> discolib::StaticConfig {
  let manager = ConnectionManager::<PgConnection>::new(&db.connstring);
  let pool = r2d2::Pool::builder().build(manager).expect("Failed to create pool.");
  discolib::StaticConfig {
    pool,
    db_connection_count: 1,
  }
}

pub fn auth_client(uuid: Uuid) -> JwtVerify<Box<dyn auth_client::AuthClient>> {
  let client: Box<dyn AuthClient + 'static> = Box::new(FakeAuthClient {
    subject: Subject::Application(uuid.to_string()),
  });
  let actor = AuthClientActor::new(client);
  let addr = actor.start();
  JwtVerify::new(addr.clone())
}
