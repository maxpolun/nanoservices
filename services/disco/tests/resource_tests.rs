mod common;
use actix_web::{test, App};
use common::{auth_client, test_db};
use discolib::config_api;
use serde_json::{json, Value};
use uuid::Uuid;

#[actix_rt::test]
async fn test_can_create_resource() {
  let db = test_db();
  let mut app = test::init_service(
    App::new()
      .wrap(auth_client(
        Uuid::parse_str("665b3bf5-463a-4716-bf49-fe480f2afe13").unwrap(),
      ))
      .configure(|cfg| config_api(cfg, common::static_config(&db))),
  )
  .await;
  let _s: Value = test::read_response_json(
    &mut app,
    test::TestRequest::post()
      .uri("/services")
      .set_json(&json!({
        "id": "test_service",
        "base_url": "http://example.com/test"
      }))
      .to_request(),
  )
  .await;
  let req = test::TestRequest::post()
    .uri("/services/test_service/resources")
    .set_json(&json!({
      "name": "test_resource"
    }))
    .to_request();
  let post_result: Value = test::read_response_json(&mut app, req).await;
  let req2 = test::TestRequest::get()
    .uri("/services/test_service/resources/test_resource")
    .to_request();
  let get_result: Value = test::read_response_json(&mut app, req2).await;
  pretty_assertions::assert_eq!(post_result, get_result)
}
