#!/usr/bin/env bash

set -euo pipefail

echo "RUNNING MIGRATIONS ON DEV DB"
diesel migration run
echo "RUNNING MIGRATIONS ON TEST DB"
DATABASE_URL=postgres://nanoservices:nanoservices@localhost/auth_test_template diesel migration run
