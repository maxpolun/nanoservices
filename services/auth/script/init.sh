#!/usr/bin/env bash

set -euo pipefail

createdb -O nanoservices auth_dev
createdb -O nanoservices auth_test_template
scriptdir="$( cd "$(dirname "$0")" ; pwd -P )"
servicedir="${dirname scriptdir}"

cp "$servicedir/.env.template" "$servicedir/.env"
