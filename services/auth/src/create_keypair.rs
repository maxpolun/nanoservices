use authlib::{
  models::keypairs::{Keypair, KeypairRepository, RealKeypairRepository},
  util::create_connection,
};

fn main() {
  let kp = Keypair::generate().expect("could not generate keypair");
  let conn = create_connection();

  println!("saving keypair {} with pubkey \n\n{}", kp.uuid, kp.public_key);

  RealKeypairRepository::new(&conn)
    .save(kp)
    .expect("unable to save keypair");
  println!("successfully saved keypair");
}
