use actix_web::{App, HttpServer};
use authlib::{config_api, StaticConfig};
use diesel::{r2d2::ConnectionManager, PgConnection};
use dotenv::dotenv;

#[actix_rt::main]
async fn main() {
  dotenv().ok();
  let _uninstall_tracer = nano_trace::init();
  let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");
  let manager = ConnectionManager::<PgConnection>::new(&database_url);
  let pool = r2d2::Pool::builder().build(manager).expect("Failed to create pool.");
  HttpServer::new(move || {
    App::new()
      .wrap(nano_trace::TracingLogger)
      .wrap(nano_trace::RequestTracing::new())
      .wrap(nano_trace::Healthcheck)
      .configure(|cfg| config_api(cfg, StaticConfig { pool: pool.clone() }))
  })
  .bind((
    "0.0.0.0",
    std::env::var("PORT")
      .and_then(|port_str| port_str.parse::<u16>().map_err(|_| std::env::VarError::NotPresent))
      .unwrap_or(8500),
  ))
  .unwrap()
  .run()
  .await
  .unwrap();
}
