table! {
    credentials (uuid) {
        uuid -> Uuid,
        password_hash -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    keypairs (uuid) {
        uuid -> Uuid,
        private_key -> Text,
        public_key -> Text,
        created_at -> Timestamptz,
    }
}

table! {
    refresh_tokens (uuid) {
        uuid -> Uuid,
        credential_uuid -> Uuid,
        created_at -> Timestamptz,
    }
}

table! {
    revoked_tokens (token_id) {
        token_id -> Text,
        expiration -> Timestamptz,
    }
}

table! {
    service_keys (service_name, key_id) {
        key_id -> Text,
        service_name -> Text,
        public_key -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

table! {
    services (name) {
        name -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

joinable!(refresh_tokens -> credentials (credential_uuid));
joinable!(service_keys -> services (service_name));

allow_tables_to_appear_in_same_query!(
  credentials,
  keypairs,
  refresh_tokens,
  revoked_tokens,
  service_keys,
  services,
);
