#[cfg(test)]
use actix_web::{dev::Body, web::HttpResponse};
use diesel::{pg::PgConnection, Connection};
#[cfg(test)]
use serde::Deserialize;
use std::env;

#[cfg(test)]
pub fn test_connection() -> PgConnection {
  dotenv::dotenv().ok();
  let database_url = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  let conn = PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url));
  conn.begin_test_transaction().unwrap();
  conn
}

pub fn create_connection() -> PgConnection {
  dotenv::dotenv().ok();
  let database_url = env::var("DATABASE_URL").expect("DATABASE_URL must be set");
  PgConnection::establish(&database_url).unwrap_or_else(|_| panic!("Error connecting to {}", database_url))
}

pub type BoxError = Box<dyn std::error::Error>;
pub type BoxResult<T> = Result<T, BoxError>;
pub type Timestamp = chrono::DateTime<chrono::Utc>;

#[cfg(test)]
pub fn parse_http_response<'a, T: Deserialize<'a>>(response: &'a HttpResponse) -> T {
  let body = response.body();
  let bytes = match body.as_ref().unwrap() {
    Body::Bytes(b) => b,
    _ => panic!("expected bytes"),
  };
  serde_json::from_slice(bytes.as_ref()).unwrap()
}
