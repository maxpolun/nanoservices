use crate::{
  models::credentials::{CredentialRepository, NewCredential, RealCredentialRepository},
  Pool,
};
use actix_web::{web, HttpResponse, Result};
use serde::Deserialize;
use tracing::info;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct CredentialCreateRequestBody {
  password: String,
}

pub async fn create_credential(
  pool: web::Data<Pool>,
  req_body: web::Json<CredentialCreateRequestBody>,
) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let cred = RealCredentialRepository::new(&conn)
    .create(NewCredential::from_password(&req_body.0.password))
    .unwrap();
  info!("Successfully created credential {}", cred.uuid);
  Ok(HttpResponse::Ok().json(cred))
}

pub async fn update_credential(
  pool: web::Data<Pool>,
  req_body: web::Json<CredentialCreateRequestBody>,
  cred_uuid: web::Path<Uuid>,
) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let cred_repo = RealCredentialRepository::new(&conn);
  update_credential_impl(cred_repo, req_body.into_inner(), cred_uuid.into_inner())
}

pub fn update_credential_impl(
  cred_repo: impl CredentialRepository,
  req_body: CredentialCreateRequestBody,
  cred_uuid: Uuid,
) -> Result<HttpResponse> {
  let cred = cred_repo
    .update(cred_uuid, NewCredential::from_password(&req_body.password))
    .unwrap();
  info!("updated credential {}", cred.uuid);
  Ok(HttpResponse::Ok().json(cred))
}
