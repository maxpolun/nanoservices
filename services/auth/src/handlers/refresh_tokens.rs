use crate::{
  models::{
    credentials::{CredentialRepository, RealCredentialRepository},
    refresh_tokens::{NewRefreshToken, RealRefreshTokenRepository, RefreshTokenRepository},
  },
  Pool,
};
use actix_web::{web, HttpResponse, Result};
use serde::Deserialize;
use tracing::info;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct RefreshTokenCreateRequestBody {
  password: String,
}

pub async fn create_refresh_token(
  pool: web::Data<Pool>,
  req_body: web::Json<RefreshTokenCreateRequestBody>,
  cred_uuid: web::Path<Uuid>,
) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let cred_repo = RealCredentialRepository::new(&conn);
  let token_repo = RealRefreshTokenRepository::new(&conn);
  create_refresh_token_impl(&cred_repo, &token_repo, req_body.into_inner(), cred_uuid.into_inner())
}

pub fn create_refresh_token_impl(
  cred_repo: &impl CredentialRepository,
  token_repo: &impl RefreshTokenRepository,
  req_body: RefreshTokenCreateRequestBody,
  cred_uuid: Uuid,
) -> Result<HttpResponse> {
  let cred = cred_repo.get_by_uuid(cred_uuid).unwrap();
  if cred.verify(&req_body.password) {
    info!("successfully logged in to credential {}", cred_uuid);
    let refresh_token = token_repo
      .create(NewRefreshToken {
        credential_uuid: cred_uuid,
      })
      .unwrap();
    return Ok(HttpResponse::Ok().json(refresh_token));
  }
  info!("failed to log into to credential {}", cred_uuid);
  Ok(HttpResponse::Unauthorized().into())
}

pub async fn delete_all_refresh_tokens(pool: web::Data<Pool>, cred_uuid: web::Path<Uuid>) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let token_repo = RealRefreshTokenRepository::new(&conn);
  delete_all_refresh_tokens_impl(&token_repo, cred_uuid.into_inner())
}

pub(crate) fn delete_all_refresh_tokens_impl(
  token_repo: &impl RefreshTokenRepository,
  cred_uuid: Uuid,
) -> Result<HttpResponse> {
  token_repo.delete_all(cred_uuid).unwrap();
  Ok(HttpResponse::NoContent().into())
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::{
    models::{
      credentials::{FakeCredentialRepository, NewCredential},
      refresh_tokens::{FakeRefreshTokenRepository, RefreshToken},
    },
    util::parse_http_response,
  };
  use actix_web::http::StatusCode;

  #[test]
  fn create_refresh_token_creates_a_token() {
    let cred_repo = FakeCredentialRepository::new();
    let token_repo = FakeRefreshTokenRepository::new();
    let cred = cred_repo.create(NewCredential::from_password("test")).unwrap();
    let req_body = RefreshTokenCreateRequestBody {
      password: "test".to_owned(),
    };
    let response: RefreshToken =
      parse_http_response(&create_refresh_token_impl(&cred_repo, &token_repo, req_body, cred.uuid).unwrap());
    pretty_assertions::assert_eq!(response.credential_uuid, cred.uuid);
    assert!(token_repo.get_by_uuid(response.uuid).is_ok())
  }

  #[test]
  fn create_refresh_token_returns_401_on_bad_password() {
    let cred_repo = FakeCredentialRepository::new();
    let token_repo = FakeRefreshTokenRepository::new();
    let cred = cred_repo
      .create(NewCredential::from_password("correct_password"))
      .unwrap();
    let req_body = RefreshTokenCreateRequestBody {
      password: "incorrect_password".to_owned(),
    };

    let response = create_refresh_token_impl(&cred_repo, &token_repo, req_body, cred.uuid).unwrap();
    pretty_assertions::assert_eq!(response.head().status, StatusCode::UNAUTHORIZED)
  }

  #[test]
  fn delete_all_refresh_tokens_deletes_tokens() {
    let token_repo = FakeRefreshTokenRepository::new();
    let cred_uuid = Uuid::new_v4();
    for _ in 0..10 {
      token_repo
        .create(NewRefreshToken {
          credential_uuid: cred_uuid,
        })
        .unwrap();
    }
    pretty_assertions::assert_eq!(token_repo.refresh_tokens.borrow().len(), 10);
    delete_all_refresh_tokens_impl(&token_repo, cred_uuid).unwrap();
    pretty_assertions::assert_eq!(token_repo.refresh_tokens.borrow().len(), 0);
  }
}
