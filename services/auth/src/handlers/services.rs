use crate::{models::services::Service, Pool};
use actix_web::{web, HttpResponse, Result};

pub async fn index_services(pool: web::Data<Pool>) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let services = Service::get_all(&conn).unwrap();
  Ok(HttpResponse::Ok().json(services))
}

pub async fn index_keys(pool: web::Data<Pool>, service_name: web::Path<String>) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let service = Service::get_by_name(&conn, &service_name).unwrap();
  Ok(HttpResponse::Ok().json(service.get_all_keys(&conn).unwrap()))
}

pub async fn get_key_by_id(pool: web::Data<Pool>, path: web::Path<(String, String)>) -> Result<HttpResponse> {
  let (service_name, key_id) = path.into_inner();
  let conn = pool.get().unwrap();
  let service = Service::get_by_name(&conn, &service_name).unwrap();
  Ok(HttpResponse::Ok().json(service.get_key(&conn, &key_id).unwrap()))
}
