use crate::{
  models::{
    access_tokens::AccessToken,
    keypairs::{KeypairRepository, RealKeypairRepository},
    refresh_tokens::{RealRefreshTokenRepository, RefreshTokenRepository},
    revoked_tokens::RevokedToken,
    services::Service,
  },
  Pool,
};
use actix_web::{web, Error, HttpResponse, Result};
use jsonwebtoken::{Algorithm, DecodingKey, Validation};
use serde::{Deserialize, Serialize};
use tracing::{debug, info};
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct AccessTokenCreateRequestBody {
  refresh_token_uuid: Uuid,
}

#[derive(Debug, Serialize)]
pub struct AccessTokenJwtResponse {
  access_token: String,
}

pub async fn create_access_token(
  pool: web::Data<Pool>,
  req_body: web::Json<AccessTokenCreateRequestBody>,
) -> Result<HttpResponse, Error> {
  let conn = pool.get().unwrap();
  let token = RealRefreshTokenRepository::new(&conn)
    .get_by_uuid(req_body.refresh_token_uuid)
    .unwrap();
  debug!("loaded refresh token {}", token.uuid);
  let access_token = AccessToken::from_refresh_token(token).unwrap();
  let keypair = &RealKeypairRepository::new(&conn).get_all().unwrap()[0];
  debug!("using keypair {}", keypair.uuid);
  let jwt = access_token.to_jwt(keypair).unwrap();
  info!("generated jwt {}", jwt);
  Ok(HttpResponse::Ok().json(AccessTokenJwtResponse { access_token: jwt }))
}

pub async fn validate(pool: web::Data<Pool>, raw_token: web::Bytes) -> Result<HttpResponse, Error> {
  let conn = pool.get().unwrap();
  let token = std::str::from_utf8(raw_token.as_ref())?;
  let header = jsonwebtoken::decode_header(token).unwrap();
  let full_key_id = header.kid.unwrap();
  let items = full_key_id.split("::").collect::<Vec<&str>>();
  if items.len() != 2 {
    return Ok(HttpResponse::UnprocessableEntity().finish());
  }
  let [service, key_id] = [items[0], items[1]];
  let val = Validation {
    leeway: 60,
    validate_nbf: true,
    validate_exp: true,
    ..Validation::new(Algorithm::ES256)
  };
  let pubkey = if service == "auth" {
    let keypair = RealKeypairRepository::new(&conn)
      .get_by_uuid(Uuid::parse_str(key_id).unwrap())
      .unwrap();
    keypair.public_key
  } else {
    Service::get_by_name(&conn, &service)
      .unwrap()
      .get_key(&conn, &key_id)
      .unwrap()
      .public_key
  };
  let parsed = jsonwebtoken::decode::<AccessToken>(token, &DecodingKey::from_ec_der(pubkey.as_bytes()), &val).unwrap();
  if RevokedToken::is_revoked(&conn, &parsed.claims.jti.to_string()).unwrap_or(true) {
    return Ok(HttpResponse::UnprocessableEntity().finish());
  }

  Ok(HttpResponse::Ok().finish())
}
