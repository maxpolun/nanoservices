use crate::{
  models::keypairs::{Keypair, KeypairRepository, RealKeypairRepository},
  Pool,
};
use actix_web::{web, HttpResponse, Result};
use serde::{Deserialize, Serialize};
use tracing::info;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct KeyResponse {
  key_id: Uuid,
  public_key: String,
}

impl KeyResponse {
  fn from_keypair(keypair: &Keypair) -> Self {
    Self {
      key_id: keypair.uuid,
      public_key: format!("BEGIN PUBLIC KEY\n{}\nEND PUBLIC KEY", keypair.public_key),
    }
  }
}

pub async fn get_keys(pool: web::Data<Pool>) -> Result<HttpResponse> {
  let conn = pool.get().unwrap();
  let key_repo = RealKeypairRepository::new(&conn);
  get_keys_impl(key_repo)
}

pub fn get_keys_impl(key_repo: impl KeypairRepository) -> Result<HttpResponse> {
  let keys = key_repo.get_all().unwrap();
  info!("loaded {} keys", keys.len());
  Ok(HttpResponse::Ok().json(keys.iter().map(KeyResponse::from_keypair).collect::<Vec<_>>()))
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::{
    models::keypairs::{FakeKeypairRepository, Keypair, KeypairRepository},
    util::parse_http_response,
  };
  use actix_web::dev::Body;

  #[test]
  fn returns_an_empty_array_if_no_keys_exist() {
    let repo = FakeKeypairRepository::new();
    let result = get_keys_impl(repo).unwrap();
    let body = result.body();
    pretty_assertions::assert_eq!(body.as_ref().unwrap(), &Body::from_slice(b"[]"));
  }

  #[test]
  fn returns_pubkeys_for_all_keypairs() {
    let repo = FakeKeypairRepository::new();
    let keypairs = vec![
      Keypair::generate().unwrap(),
      Keypair::generate().unwrap(),
      Keypair::generate().unwrap(),
      Keypair::generate().unwrap(),
    ];
    for keypair in keypairs.iter() {
      repo.save(keypair.clone()).unwrap();
    }
    let response: Vec<KeyResponse> = parse_http_response(&get_keys_impl(repo).unwrap());
    for keypair in keypairs.iter() {
      assert!(response.iter().any(|k| k.key_id == keypair.uuid))
    }
  }
}
