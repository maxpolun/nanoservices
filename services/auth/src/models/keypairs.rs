use crate::{
  schema::keypairs,
  util::{BoxResult, Timestamp},
};
use base64::{decode, encode};
use diesel::pg::PgConnection;
use ring::{
  rand,
  signature::{self, KeyPair},
};
use std::fmt;
use uuid::Uuid;

use super::services::{auth_service_name, ServiceKey};

#[derive(Clone, PartialEq, Eq, Queryable, Insertable)]
pub struct Keypair {
  pub uuid: Uuid,
  pub private_key: String,
  pub public_key: String,
  pub created_at: Timestamp,
}

impl fmt::Debug for Keypair {
  fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
    write!(
      f,
      "Keypair {{ uuid: {:?}, public_key: {:?}, private_key: <<FILTERED>>, created_at: {:?} }}",
      self.uuid, self.public_key, self.created_at
    )
  }
}

impl Keypair {
  pub fn generate() -> BoxResult<Self> {
    let rng = rand::SystemRandom::new();
    let key_bytes = signature::EcdsaKeyPair::generate_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, &rng)?;
    let pair = signature::EcdsaKeyPair::from_pkcs8(&signature::ECDSA_P256_SHA256_FIXED_SIGNING, key_bytes.as_ref())?;
    Ok(Self {
      uuid: Uuid::new_v4(),
      public_key: encode(pair.public_key().as_ref()),
      private_key: encode(key_bytes.as_ref()),
      created_at: chrono::Utc::now(),
    })
  }

  pub fn private_key_der(&self) -> BoxResult<Vec<u8>> {
    Ok(decode(&self.private_key)?)
  }

  pub fn public_key_der(&self) -> BoxResult<Vec<u8>> {
    Ok(decode(&self.public_key)?)
  }

  pub fn as_service_key(&self) -> ServiceKey {
    ServiceKey {
      service_name: auth_service_name(),
      key_id: self.uuid.to_hyphenated().to_string(),
      public_key: format!("BEGIN PUBLIC KEY\n{}\nEND PUBLIC KEY", self.public_key),
      created_at: self.created_at,
      updated_at: self.created_at,
    }
  }
}

pub trait KeypairRepository {
  fn get_all(&self) -> BoxResult<Vec<Keypair>>;
  fn get_by_uuid(&self, uuid: Uuid) -> BoxResult<Keypair>;
  fn save(&self, key: Keypair) -> BoxResult<()>;
}

pub struct RealKeypairRepository<'a> {
  conn: &'a PgConnection,
}

impl<'a> RealKeypairRepository<'a> {
  pub fn new(conn: &'a PgConnection) -> Self {
    Self { conn }
  }
}

impl<'a> KeypairRepository for RealKeypairRepository<'a> {
  fn get_all(&self) -> BoxResult<Vec<Keypair>> {
    use crate::schema::keypairs::dsl::*;
    use diesel::prelude::*;

    Ok(keypairs.order_by(created_at.desc()).get_results::<Keypair>(self.conn)?)
  }
  fn get_by_uuid(&self, key_uuid: Uuid) -> BoxResult<Keypair> {
    use crate::schema::keypairs::dsl::*;
    use diesel::prelude::*;

    Ok(keypairs.find(key_uuid).get_result::<Keypair>(self.conn)?)
  }
  fn save(&self, key: Keypair) -> BoxResult<()> {
    use diesel::prelude::*;
    diesel::insert_into(keypairs::table).values(&key).execute(self.conn)?;
    Ok(())
  }
}

#[cfg(test)]
pub struct FakeKeypairRepository {
  keypairs: std::rc::Rc<std::cell::RefCell<Vec<Keypair>>>,
}

#[cfg(test)]
impl FakeKeypairRepository {
  pub fn new() -> Self {
    Self {
      keypairs: std::rc::Rc::new(std::cell::RefCell::new(Vec::new())),
    }
  }
}

#[cfg(test)]
impl KeypairRepository for FakeKeypairRepository {
  fn get_all(&self) -> BoxResult<Vec<Keypair>> {
    Ok((*self.keypairs).borrow().clone())
  }
  fn get_by_uuid(&self, key_uuid: Uuid) -> BoxResult<Keypair> {
    Ok(
      (*self.keypairs)
        .borrow()
        .iter()
        .find(|keypair| keypair.uuid == key_uuid)
        .unwrap()
        .clone(),
    )
  }
  fn save(&self, key: Keypair) -> BoxResult<()> {
    (*self.keypairs).borrow_mut().push(key);
    Ok(())
  }
}
