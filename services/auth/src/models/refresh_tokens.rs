use crate::{
  models::credentials::Credential,
  schema::refresh_tokens,
  util::{BoxResult, Timestamp},
};
#[cfg(test)]
use chrono::Utc;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Queryable, PartialEq, Eq, Serialize, Deserialize, Associations, Clone)]
#[belongs_to(Credential, foreign_key = "credential_uuid")]
#[table_name = "refresh_tokens"]
pub struct RefreshToken {
  pub uuid: Uuid,
  pub credential_uuid: Uuid,
  pub created_at: Timestamp,
}

#[derive(Debug, Insertable)]
#[table_name = "refresh_tokens"]
pub struct NewRefreshToken {
  pub credential_uuid: Uuid,
}

pub trait RefreshTokenRepository {
  fn create(&self, new_token: NewRefreshToken) -> BoxResult<RefreshToken>;
  fn get_by_uuid(&self, uuid: Uuid) -> BoxResult<RefreshToken>;
  fn delete(&self, uuid: Uuid) -> BoxResult<()>;
  fn delete_all(&self, credential_uuid: Uuid) -> BoxResult<()>;
}

pub struct RealRefreshTokenRepository<'a> {
  conn: &'a PgConnection,
}

impl<'a> RealRefreshTokenRepository<'a> {
  pub fn new(conn: &'a PgConnection) -> Self {
    Self { conn }
  }
}

impl<'a> RefreshTokenRepository for RealRefreshTokenRepository<'a> {
  fn create(&self, new_token: NewRefreshToken) -> BoxResult<RefreshToken> {
    use diesel::prelude::*;
    Ok(
      diesel::insert_into(refresh_tokens::table)
        .values(new_token)
        .get_result(self.conn)?,
    )
  }

  fn get_by_uuid(&self, token_uuid: Uuid) -> BoxResult<RefreshToken> {
    use crate::schema::refresh_tokens::dsl::*;
    use diesel::prelude::*;
    Ok(refresh_tokens.find(token_uuid).get_result(self.conn)?)
  }

  fn delete(&self, token_uuid: Uuid) -> BoxResult<()> {
    use crate::schema::refresh_tokens::dsl::*;
    use diesel::prelude::*;
    diesel::delete(refresh_tokens.find(token_uuid)).execute(self.conn)?;
    Ok(())
  }

  fn delete_all(&self, cred_uuid: Uuid) -> BoxResult<()> {
    use crate::schema::refresh_tokens::dsl::*;
    use diesel::prelude::*;

    diesel::delete(refresh_tokens.filter(credential_uuid.eq(cred_uuid))).execute(self.conn)?;
    Ok(())
  }
}

#[cfg(test)]
pub struct FakeRefreshTokenRepository {
  pub refresh_tokens: std::rc::Rc<std::cell::RefCell<Vec<RefreshToken>>>,
}

#[cfg(test)]
impl FakeRefreshTokenRepository {
  pub fn new() -> Self {
    Self {
      refresh_tokens: std::rc::Rc::new(std::cell::RefCell::new(Vec::new())),
    }
  }
}

#[cfg(test)]
impl RefreshTokenRepository for FakeRefreshTokenRepository {
  fn create(&self, new_token: NewRefreshToken) -> BoxResult<RefreshToken> {
    let token = RefreshToken {
      credential_uuid: new_token.credential_uuid,
      uuid: Uuid::new_v4(),
      created_at: Utc::now(),
    };
    self.refresh_tokens.borrow_mut().push(token.clone());
    Ok(token)
  }
  fn get_by_uuid(&self, uuid: Uuid) -> BoxResult<RefreshToken> {
    Ok(
      self
        .refresh_tokens
        .borrow()
        .iter()
        .find(|t| t.uuid == uuid)
        .unwrap()
        .clone(),
    )
  }
  fn delete(&self, uuid: Uuid) -> BoxResult<()> {
    let (idx, _) = self
      .refresh_tokens
      .borrow()
      .iter()
      .enumerate()
      .find(|(_, t)| t.uuid == uuid)
      .unwrap();
    self.refresh_tokens.borrow_mut().remove(idx);
    Ok(())
  }

  fn delete_all(&self, cred_uuid: Uuid) -> BoxResult<()> {
    self
      .refresh_tokens
      .borrow_mut()
      .retain(|t| t.credential_uuid != cred_uuid);
    Ok(())
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::{diesel::Connection, util::test_connection};
  use chrono::offset::TimeZone;

  fn create_cred(uuid: Uuid, conn: &PgConnection) {
    conn
      .execute(
        // danger -- yucky test code. Can cause an SQL injection if used in prod. Only use in tests
        &format!(
          "INSERT INTO credentials (
        uuid,
        password_hash,
        created_at,
        updated_at
      ) VALUES (
        '{}',
        'fake_password',
        '2019-9-12 14:23:54',
        '2019-9-12 14:23:54'
        )",
          uuid
        ),
      )
      .unwrap();
  }

  #[test]
  fn test_can_create_refresh_token() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let uuid = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    create_cred(uuid, &conn);
    let token = repo.create(NewRefreshToken { credential_uuid: uuid }).unwrap();
    pretty_assertions::assert_eq!(token.credential_uuid, uuid);
  }

  #[test]
  fn create_fails_if_cred_uuid_doesnt_exist() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let uuid = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    let token = repo.create(NewRefreshToken { credential_uuid: uuid });

    assert!(token.is_err());
  }

  #[test]
  fn get_by_uuid_gets_the_token() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let cred_uuid = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    create_cred(cred_uuid, &conn);
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a3', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();

    let uuid = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a3").unwrap();
    let token = repo.get_by_uuid(uuid).unwrap();
    pretty_assertions::assert_eq!(
      token,
      RefreshToken {
        uuid,
        credential_uuid: cred_uuid,
        created_at: chrono::Utc.timestamp(1568298234, 0)
      }
    );
  }

  #[test]
  fn delete_removes_the_token_from_the_database() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let cred_uuid = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    create_cred(cred_uuid, &conn);
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a3', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();

    let uuid = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a3").unwrap();
    repo.delete(uuid).unwrap();
    assert!(repo.get_by_uuid(uuid).is_err());
  }

  #[test]
  fn delete_all_removes_the_tokens_from_the_database() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let cred_uuid = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    create_cred(cred_uuid, &conn);
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a3', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a4', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a5', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();

    let uuid1 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a3").unwrap();
    let uuid2 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a4").unwrap();
    let uuid3 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a5").unwrap();
    repo.delete_all(cred_uuid).unwrap();
    assert!(repo.get_by_uuid(uuid1).is_err());
    assert!(repo.get_by_uuid(uuid2).is_err());
    assert!(repo.get_by_uuid(uuid3).is_err());
  }

  #[test]
  fn delete_all_keeps_tokens_from_other_creds() {
    let conn = test_connection();
    let repo = RealRefreshTokenRepository::new(&conn);
    let cred_uuid1 = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485a").unwrap();
    let cred_uuid2 = Uuid::parse_str("00da6bb8-6752-4c2b-a332-64467765485b").unwrap();
    create_cred(cred_uuid1, &conn);
    create_cred(cred_uuid2, &conn);
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a3', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a4', '00da6bb8-6752-4c2b-a332-64467765485a', '2019-9-12 14:23:54')",
      )
      .unwrap();
    conn
      .execute(
        "INSERT INTO refresh_tokens
      (uuid, credential_uuid, created_at)
      VALUES
      ('db6b19b1-92e7-4844-a9c5-914de9d272a5', '00da6bb8-6752-4c2b-a332-64467765485b', '2019-9-12 14:23:54')",
      )
      .unwrap();

    let uuid1 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a3").unwrap();
    let uuid2 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a4").unwrap();
    let uuid3 = Uuid::parse_str("db6b19b1-92e7-4844-a9c5-914de9d272a5").unwrap();
    repo.delete_all(cred_uuid1).unwrap();
    assert!(repo.get_by_uuid(uuid1).is_err());
    assert!(repo.get_by_uuid(uuid2).is_err());
    assert!(repo.get_by_uuid(uuid3).is_ok());
  }
}
