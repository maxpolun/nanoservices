use crate::{
  models::{keypairs::Keypair, refresh_tokens::RefreshToken},
  util::{BoxResult, Timestamp},
};
use chrono::{prelude::*, serde::ts_seconds};
use jsonwebtoken::{encode, Algorithm, EncodingKey, Header};
use serde::{Deserialize, Serialize};
use time::Duration;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessToken {
  pub iss: String,
  pub sub: String,
  #[serde(with = "ts_seconds")]
  pub exp: Timestamp,
  #[serde(with = "ts_seconds")]
  pub nbf: Timestamp,
  #[serde(with = "ts_seconds")]
  pub iat: Timestamp,
  pub jti: Uuid,
}

impl AccessToken {
  pub fn from_refresh_token(refresh_token: RefreshToken) -> BoxResult<Self> {
    let now = Utc::now();
    let exp = now + Duration::minutes(15);
    Ok(AccessToken {
      iss: std::env::var("HOSTNAME").expect("HOSTNAME"),
      sub: format!("u:{}", refresh_token.credential_uuid),
      exp,
      nbf: now,
      iat: now,
      jti: Uuid::new_v4(),
    })
  }

  pub fn to_jwt(&self, keypair: &Keypair) -> BoxResult<String> {
    let mut header = Header::new(Algorithm::ES256);
    header.kid = Some(format!("{}::{}", "auth", keypair.uuid.to_hyphenated()));
    Ok(encode(
      &header,
      self,
      &EncodingKey::from_ec_der(&keypair.private_key_der()?),
    )?)
  }
}
