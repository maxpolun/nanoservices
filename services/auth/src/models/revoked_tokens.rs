use crate::{schema::*, util::Timestamp};
use chrono::Utc;
use diesel::{prelude::*, PgConnection};
use serde::{Deserialize, Serialize};

#[derive(Debug, Queryable, PartialEq, Eq, Serialize, Insertable, Deserialize, Clone)]
pub struct RevokedToken {
  token_id: String,
  expiration: Timestamp,
}

impl RevokedToken {
  pub fn is_revoked(conn: &PgConnection, token_id: &str) -> Result<bool, diesel::result::Error> {
    revoked_tokens::table
      .find(token_id)
      .get_result::<RevokedToken>(conn)
      .optional()
      .map(|opt| opt.is_some())
  }

  pub fn cleanup_expired(conn: &PgConnection) -> Result<usize, diesel::result::Error> {
    diesel::delete(revoked_tokens::table.filter(revoked_tokens::expiration.lt(Utc::now()))).execute(conn)
  }

  pub fn revoke(conn: &PgConnection, token_id: &str, expiration: Timestamp) -> Result<(), diesel::result::Error> {
    diesel::insert_into(revoked_tokens::table)
      .values(RevokedToken {
        token_id: token_id.to_owned(),
        expiration,
      })
      .execute(conn)
      .map(|_k| ())
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::util::test_connection;

  #[test]
  fn can_revoke() {
    let conn = test_connection();
    let token_id = "test_tok_id";

    RevokedToken::revoke(&conn, token_id, Utc::now() + chrono::Duration::hours(1)).unwrap();
    assert!(RevokedToken::is_revoked(&conn, token_id).unwrap());
  }

  #[test]
  fn can_cleanup() {
    let conn = test_connection();
    let token_id = "test_tok_id";

    RevokedToken::revoke(&conn, token_id, Utc::now() - chrono::Duration::hours(1)).unwrap();
    pretty_assertions::assert_eq!(RevokedToken::cleanup_expired(&conn).unwrap(), 1);
  }
}
