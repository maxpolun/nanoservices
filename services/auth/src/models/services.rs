use crate::{schema::*, util::Timestamp};
use base64::decode;
use chrono::{TimeZone, Utc};
use diesel::{prelude::*, PgConnection};
use serde::{Deserialize, Serialize};
use thiserror::Error;
use uuid::Uuid;

use super::keypairs::{KeypairRepository, RealKeypairRepository};

#[derive(Debug, Queryable, PartialEq, Eq, Serialize, Deserialize, Associations, Clone)]
pub struct Service {
  pub name: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

#[derive(Debug, Insertable)]
#[table_name = "services"]
struct NewService<'a> {
  name: &'a str,
}

#[derive(Debug, Error)]
pub enum CreateServiceError {
  #[error("Invalid name")]
  BadName,
  #[error("Error saving to db {}", .0)]
  DbError(#[from] diesel::result::Error),
}

pub fn auth_service_name() -> String {
  std::env::var("AUTH_SERVICE_NAME").unwrap_or("auth".to_owned())
}

impl Service {
  pub fn create(conn: &PgConnection, name: &str) -> Result<Service, CreateServiceError> {
    if name == auth_service_name() {
      return Err(CreateServiceError::BadName);
    }
    if name.len() < 3 || !name.chars().all(|c| c.is_ascii_alphanumeric() || c == '_') {
      return Err(CreateServiceError::BadName);
    }
    Ok(
      diesel::insert_into(services::table)
        .values(NewService { name })
        .get_result(conn)?,
    )
  }

  pub fn this_service() -> Service {
    Service {
      name: auth_service_name(),
      created_at: Utc.ymd(2020, 11, 6).and_hms(8, 50, 15), // arbitrary good time as created/updated at
      updated_at: Utc.ymd(2020, 11, 6).and_hms(8, 50, 15),
    }
  }

  pub fn get_all(conn: &PgConnection) -> Result<Vec<Service>, diesel::result::Error> {
    services::table.get_results(conn).map(|services| {
      let mut v = vec![Service::this_service()];
      v.extend(services);
      v
    })
  }

  pub fn get_by_name(conn: &PgConnection, name: &str) -> Result<Service, diesel::result::Error> {
    if name == auth_service_name() {
      return Ok(Service::this_service());
    }
    services::table.find(name).get_result(conn)
  }

  pub fn add_key(&self, conn: &PgConnection, key_id: &str, public_key: &str) -> Result<ServiceKey, CreateKeyError> {
    if self.name == auth_service_name() {
      return Err(CreateKeyError::BadService);
    }
    if key_id.len() < 3
      || !key_id
        .chars()
        .all(|c| c.is_ascii_alphanumeric() || c == '_' || c == '-')
    {
      return Err(CreateKeyError::BadId);
    }

    let mut has_begin = false;
    let mut has_end = false;
    let mut body = String::new();
    for line in public_key.lines() {
      let trimmed = line.trim();
      if trimmed.contains("BEGIN PUBLIC KEY") {
        has_begin = true;
        continue;
      } else if trimmed.contains("END PUBLIC KEY") {
        has_end = true;
        break;
      } else {
        body.push_str(trimmed);
      }
    }
    if !has_begin || !has_end {
      return Err(CreateKeyError::BadKey);
    }

    decode(&body).map_err(|_e| CreateKeyError::BadKey)?;

    Ok(
      diesel::insert_into(service_keys::table)
        .values(NewServiceKey {
          key_id,
          service_name: &self.name,
          public_key: &body,
        })
        .get_result(conn)?,
    )
  }

  pub fn delete_key(&self, conn: &PgConnection, key_id: &str) -> Result<(), diesel::result::Error> {
    diesel::delete(service_keys::table.find((&self.name, key_id)))
      .execute(conn)
      .map(|_| ())
  }

  pub fn get_key(&self, conn: &PgConnection, key_id: &str) -> Result<ServiceKey, diesel::result::Error> {
    if self.name == auth_service_name() {
      return RealKeypairRepository::new(conn)
        .get_by_uuid(Uuid::parse_str(key_id).unwrap())
        .map(|keypair| keypair.as_service_key())
        .map_err(|_e| diesel::result::Error::NotFound);
    }
    service_keys::table.find((&self.name, key_id)).get_result(conn)
  }

  pub fn get_all_keys(&self, conn: &PgConnection) -> Result<Vec<ServiceKey>, diesel::result::Error> {
    if self.name == auth_service_name() {
      return RealKeypairRepository::new(conn)
        .get_all()
        .map(|keys| keys.iter().map(|k| k.as_service_key()).collect())
        .map_err(|_e| diesel::result::Error::NotFound);
    }
    service_keys::table
      .filter(service_keys::service_name.eq(&self.name))
      .get_results(conn)
  }
}

#[derive(Debug, Queryable, PartialEq, Eq, Serialize, Deserialize, Associations, Clone)]
pub struct ServiceKey {
  pub key_id: String,
  pub service_name: String,
  pub public_key: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

#[derive(Debug, Insertable)]
#[table_name = "service_keys"]
struct NewServiceKey<'a> {
  pub key_id: &'a str,
  pub service_name: &'a str,
  pub public_key: &'a str,
}

#[derive(Debug, Error)]
pub enum CreateKeyError {
  #[error("Invalid key id")]
  BadId,
  #[error("bad pubkey (only basic format is checked, not validity of the key)")]
  BadKey,
  #[error("bad service (cannot add keys to the core auth service this way)")]
  BadService,
  #[error("Error saving to db {}", .0)]
  DbError(#[from] diesel::result::Error),
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::util::test_connection;

  fn create_service(conn: &PgConnection) -> Service {
    Service::create(&conn, "test").unwrap()
  }

  #[test]
  fn can_create_service() {
    let conn = test_connection();

    Service::create(&conn, "test").unwrap();
    pretty_assertions::assert_eq!(
      services::table.find("test").get_result::<Service>(&conn).unwrap().name,
      "test"
    )
  }

  #[test]
  fn name_must_be_valid() {
    let conn = test_connection();

    assert!(matches!(Service::create(&conn, ""), Err(CreateServiceError::BadName)));

    assert!(matches!(
      Service::create(&conn, "test name"),
      Err(CreateServiceError::BadName)
    ));

    assert!(matches!(
      Service::create(&conn, "🦀🦀🦀"),
      Err(CreateServiceError::BadName)
    ));

    assert!(matches!(
      Service::create(&conn, "auth"),
      Err(CreateServiceError::BadName)
    ));
  }

  #[test]
  fn can_add_key() {
    let conn = test_connection();

    let service = create_service(&conn);
    let key = service.add_key(&conn, "test_key", "BEGIN PUBLIC KEY\nBJ8ZJpuycTBkfmM1zAcC9OwE5CpcqEpgcdL9vSDUbBQ8FglR1J4v3djAK1rKShiDHKzgFOVW6vzbIwuZHcfQ3+w=\nEND PUBLIC KEY").unwrap();
    pretty_assertions::assert_eq!(key.key_id, "test_key");
    pretty_assertions::assert_eq!(
      key.public_key,
      "BJ8ZJpuycTBkfmM1zAcC9OwE5CpcqEpgcdL9vSDUbBQ8FglR1J4v3djAK1rKShiDHKzgFOVW6vzbIwuZHcfQ3+w="
    );
  }

  #[test]
  fn key_id_must_be_valid() {
    let conn = test_connection();

    let service = create_service(&conn);
    let valid_pubkey = "BEGIN PUBLIC KEY\nBJ8ZJpuycTBkfmM1zAcC9OwE5CpcqEpgcdL9vSDUbBQ8FglR1J4v3djAK1rKShiDHKzgFOVW6vzbIwuZHcfQ3+w=\nEND PUBLIC KEY";

    assert!(matches!(
      service.add_key(&conn, "", valid_pubkey),
      Err(CreateKeyError::BadId)
    ));
    assert!(matches!(
      service.add_key(&conn, "test key", valid_pubkey),
      Err(CreateKeyError::BadId)
    ));
    assert!(matches!(
      service.add_key(&conn, "test🦀key", valid_pubkey),
      Err(CreateKeyError::BadId)
    ));

    let auth_service = Service {
      name: "auth".to_owned(),
      created_at: Utc::now(),
      updated_at: Utc::now(),
    };

    assert!(matches!(
      auth_service.add_key(&conn, "test_key", valid_pubkey),
      Err(CreateKeyError::BadService)
    ));
  }

  #[test]
  fn pubkey_must_be_valid() {
    let conn = test_connection();

    let service = create_service(&conn);

    assert!(matches!(
      service.add_key(&conn, "test_key", ""),
      Err(CreateKeyError::BadKey)
    ));
    assert!(matches!(
      service.add_key(&conn, "test_key", "BEGIN PUBLIC KEY\nnot base 64\nEND PUBLIC KEY"),
      Err(CreateKeyError::BadKey)
    ));
    assert!(matches!(
      service.add_key(
        &conn,
        "test_key",
        "BJ8ZJpuycTBkfmM1zAcC9OwE5CpcqEpgcdL9vSDUbBQ8FglR1J4v3djAK1rKShiDHKzgFOVW6vzbIwuZHcfQ3+w="
      ),
      Err(CreateKeyError::BadKey)
    ));
  }
}
