use crate::{
  schema::credentials,
  util::{BoxResult, Timestamp},
};
#[cfg(test)]
use chrono::Utc;
use diesel::pg::PgConnection;
use scrypt::{scrypt_check, scrypt_simple, ScryptParams};
use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Queryable, PartialEq, Eq, Serialize, Deserialize, Clone)]
pub struct Credential {
  pub uuid: Uuid,
  #[serde(skip_serializing)]
  pub password_hash: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

fn scrypt_params() -> ScryptParams {
  #[cfg(not(debug_assertions))]
  let params = ScryptParams::new(15, 8, 1).unwrap();
  #[cfg(debug_assertions)]
  let params = ScryptParams::new(10, 1, 1).unwrap();
  params
}

impl Credential {
  pub fn verify(&self, password: &str) -> bool {
    scrypt_check(password, &self.password_hash).is_ok()
  }
}

#[derive(Debug, Insertable)]
#[table_name = "credentials"]
pub struct NewCredential {
  pub password_hash: String,
}

impl NewCredential {
  pub fn from_password(password: &str) -> Self {
    let params = scrypt_params();
    let password_hash = scrypt_simple(password, &params).unwrap();
    NewCredential { password_hash }
  }
}

pub trait CredentialRepository {
  fn get_by_uuid(&self, uuid: Uuid) -> BoxResult<Credential>;
  fn create(&self, cred: NewCredential) -> BoxResult<Credential>;
  fn update(&self, uuid: Uuid, cred: NewCredential) -> BoxResult<Credential>;
  fn delete(&self, uuid: Uuid) -> BoxResult<()>;
}

pub struct RealCredentialRepository<'a> {
  conn: &'a PgConnection,
}

impl<'a> RealCredentialRepository<'a> {
  pub fn new(conn: &'a PgConnection) -> Self {
    Self { conn }
  }
}

impl<'a> CredentialRepository for RealCredentialRepository<'a> {
  fn get_by_uuid(&self, cred_uuid: Uuid) -> BoxResult<Credential> {
    use crate::schema::credentials::dsl::*;
    use diesel::prelude::*;
    Ok(credentials.find(cred_uuid).get_result::<Credential>(self.conn)?)
  }

  fn create(&self, cred: NewCredential) -> BoxResult<Credential> {
    use diesel::prelude::*;
    Ok(
      diesel::insert_into(credentials::table)
        .values(&cred)
        .get_result(self.conn)?,
    )
  }

  fn update(&self, cred_uuid: Uuid, cred: NewCredential) -> BoxResult<Credential> {
    use crate::schema::credentials::dsl::*;
    use diesel::prelude::*;
    Ok(
      diesel::update(credentials.find(cred_uuid))
        .set(password_hash.eq(cred.password_hash))
        .get_result(self.conn)?,
    )
  }

  fn delete(&self, cred_uuid: Uuid) -> BoxResult<()> {
    use crate::schema::credentials::dsl::*;
    use diesel::prelude::*;
    diesel::delete(credentials.find(cred_uuid)).execute(self.conn)?;
    Ok(())
  }
}

#[cfg(test)]
pub struct FakeCredentialRepository {
  pub credentials: std::rc::Rc<std::cell::RefCell<Vec<Credential>>>,
}

#[cfg(test)]
impl FakeCredentialRepository {
  pub fn new() -> Self {
    Self {
      credentials: std::rc::Rc::new(std::cell::RefCell::new(Vec::new())),
    }
  }
}

#[cfg(test)]
impl CredentialRepository for FakeCredentialRepository {
  fn get_by_uuid(&self, cred_uuid: Uuid) -> BoxResult<Credential> {
    Ok(
      self
        .credentials
        .borrow()
        .iter()
        .find(|c| c.uuid == cred_uuid)
        .unwrap()
        .clone(),
    )
  }
  fn create(&self, cred: NewCredential) -> BoxResult<Credential> {
    let c = Credential {
      uuid: Uuid::new_v4(),
      password_hash: cred.password_hash,
      created_at: Utc::now(),
      updated_at: Utc::now(),
    };
    self.credentials.borrow_mut().push(c.clone());
    Ok(c)
  }
  fn update(&self, cred_uuid: Uuid, cred: NewCredential) -> BoxResult<Credential> {
    let mut c = self
      .credentials
      .borrow_mut()
      .iter_mut()
      .find(|c| c.uuid == cred_uuid)
      .unwrap()
      .clone();
    c.password_hash = cred.password_hash;
    c.updated_at = Utc::now();
    Ok(c.clone())
  }
  fn delete(&self, cred_uuid: Uuid) -> BoxResult<()> {
    let (idx, _) = self
      .credentials
      .borrow()
      .iter()
      .enumerate()
      .find(|(_, c)| c.uuid == cred_uuid)
      .unwrap();
    self.credentials.borrow_mut().remove(idx);
    Ok(())
  }
}
#[cfg(test)]
#[derive(Debug, Insertable)]
#[table_name = "credentials"]
pub struct FakeNewCredential {
  pub uuid: Uuid,
  pub password_hash: String,
  pub created_at: chrono::DateTime<chrono::Utc>,
  pub updated_at: chrono::DateTime<chrono::Utc>,
}

#[cfg(test)]
impl FakeNewCredential {
  pub fn from_params(
    uuid: Uuid,
    password: &str,
    created_at: chrono::DateTime<chrono::Utc>,
    updated_at: chrono::DateTime<chrono::Utc>,
  ) -> Self {
    let pw_hash = NewCredential::from_password(password);
    Self {
      uuid,
      password_hash: pw_hash.password_hash,
      created_at,
      updated_at,
    }
  }
}

#[cfg(test)]
pub fn fake_create_credential<'a>(new_cred: FakeNewCredential, conn: &PgConnection) -> BoxResult<Credential> {
  use diesel::prelude::*;

  Ok(
    diesel::insert_into(credentials::table)
      .values(&new_cred)
      .get_result(conn)?,
  )
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::util::test_connection;
  use chrono::offset::TimeZone;
  use scrypt::scrypt_simple;
  use uuid::Uuid;

  fn make_hash(pw: &str) -> String {
    let params = scrypt_params();
    scrypt_simple(pw, &params).unwrap()
  }

  #[test]
  fn test_verify_works_for_matching_password() {
    let cred = Credential {
      uuid: Uuid::parse_str("eb247792-152c-4321-ae39-38b9bf802453").unwrap(),
      password_hash: make_hash("12345"),
      created_at: chrono::Utc::now(),
      updated_at: chrono::Utc::now(),
    };

    assert!(cred.verify("12345"))
  }

  #[test]
  fn test_verify_fails_for_different_password() {
    let cred = Credential {
      uuid: Uuid::parse_str("eb247792-152c-4321-ae39-38b9bf802453").unwrap(),
      password_hash: make_hash("12345"),
      created_at: chrono::Utc::now(),
      updated_at: chrono::Utc::now(),
    };

    assert!(!cred.verify("12346"))
  }

  #[test]
  fn from_password_makes_a_new_credential() {
    let cred = NewCredential::from_password("test");
    // hash is randomly generated, so don't check that, just that it's got the correct header
    pretty_assertions::assert_eq!(&cred.password_hash[0..16], "$rscrypt$0$CgEB$");
  }

  #[test]
  fn test_get_by_uuid_works() {
    let conn = test_connection();
    let uuid = Uuid::parse_str("0d93cc35-9d6e-4714-a2e5-e7c9ffeaa6e8").unwrap();
    let timestamp = chrono::Utc.timestamp(1568298234, 0);
    let original = fake_create_credential(
      FakeNewCredential::from_params(uuid, "password", timestamp, timestamp),
      &conn,
    )
    .unwrap();
    let repo = RealCredentialRepository::new(&conn);
    let cred = repo.get_by_uuid(uuid).unwrap();
    pretty_assertions::assert_eq!(cred, original)
  }

  #[test]
  fn test_get_by_uuid_fails_when_cred_doesnt_exist() {
    let conn = test_connection();
    let repo = RealCredentialRepository::new(&conn);
    let cred_result = repo.get_by_uuid(Uuid::parse_str("0d93cc35-9d6e-4714-a2e5-e7c9ffeaa6e8").unwrap());
    assert!(cred_result.is_err())
  }

  #[test]
  fn test_create_inserts_a_record() {
    let conn = test_connection();
    let repo = RealCredentialRepository::new(&conn);
    let cred = repo
      .create(NewCredential {
        password_hash: make_hash("123456789"),
      })
      .unwrap();
    let inserted_cred = repo.get_by_uuid(cred.uuid).unwrap();
    pretty_assertions::assert_eq!(cred, inserted_cred);
  }

  #[test]
  fn test_update_changes_the_password_hash() {
    let conn = test_connection();
    let repo = RealCredentialRepository::new(&conn);
    let cred = repo
      .create(NewCredential {
        password_hash: make_hash("123456789"),
      })
      .unwrap();
    let new_cred = repo
      .update(
        cred.uuid,
        NewCredential {
          password_hash: make_hash("987654321"),
        },
      )
      .unwrap();

    pretty_assertions::assert_eq!(cred.uuid, new_cred.uuid);
    pretty_assertions::assert_eq!(cred.created_at, new_cred.created_at);

    assert_ne!(cred.password_hash, new_cred.password_hash);
  }

  #[test]
  fn test_delete_removes_a_credential() {
    let conn = test_connection();
    let repo = RealCredentialRepository::new(&conn);
    let cred = repo
      .create(NewCredential {
        password_hash: make_hash("123456789"),
      })
      .unwrap();

    repo.delete(cred.uuid).unwrap();

    assert!(repo.get_by_uuid(cred.uuid).is_err());
  }
}
