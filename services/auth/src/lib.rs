#[macro_use]
extern crate diesel;
use actix_web::web;
use diesel::{
  r2d2::{self, ConnectionManager},
  PgConnection,
};

mod handlers;
pub mod models;
mod schema;
pub mod util;

type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Clone)]
pub struct StaticConfig {
  pub pool: Pool,
}

pub fn config_api(cfg: &mut web::ServiceConfig, static_config: StaticConfig) {
  cfg.service(
    web::scope("/")
      .data(static_config.pool)
      .service(
        web::scope("/credentials")
          .route("", web::post().to(crate::handlers::credentials::create_credential))
          .route(
            "/{credential_uuid}",
            web::put().to(crate::handlers::credentials::update_credential),
          )
          .route(
            "/{credential_uuid}/refresh_tokens",
            web::post().to(crate::handlers::refresh_tokens::create_refresh_token),
          )
          .route(
            "/{credential_uuid}/refresh_tokens",
            web::delete().to(crate::handlers::refresh_tokens::delete_all_refresh_tokens),
          ),
      )
      .service(
        web::resource("/access_tokens").route(web::post().to(crate::handlers::access_tokens::create_access_token)),
      )
      .service(web::resource("/keys").route(web::get().to(crate::handlers::keys::get_keys)))
      .service(
        web::scope("/services")
          .route("", web::get().to(crate::handlers::services::index_services))
          .route(
            "/{service_name}/keys",
            web::get().to(crate::handlers::services::index_keys),
          )
          .route(
            "/{service_name}/keys/{key_id}",
            web::get().to(crate::handlers::services::get_key_by_id),
          ),
      )
      .route(
        "/access_tokens/validate",
        web::post().to(crate::handlers::access_tokens::validate),
      ),
  );
}
