use authlib::{
  models::{keypairs::Keypair, services::Service},
  util::create_connection,
};

fn main() {
  dotenv::dotenv().unwrap();
  let argv: Vec<String> = std::env::args().skip(1).collect();
  let strs: Vec<&str> = argv.iter().map(|s| s.as_ref()).collect();

  match strs[..] {
    [] | ["help"] => usage(),
    ["new", service_name] => create_service(service_name),
    ["list"] => list_services(),
    ["key", "add", service_name, key_id] => add_key(service_name, key_id),
    ["key", "generate", service_name, key_id] => gen_key(service_name, key_id),
    ["key", "delete", service_name, key_id] => delete_key(service_name, key_id),
    ["key", "list", service_name] => list_keys(service_name),
    _ => panic!("unrecognized args"),
  }
}

fn usage() {
  println!(
    "
service: admin cli for managing auth integrated services

service; service help: print this help
service list: list all services
service new [service_name]: create a new service with name [service_name]
service key list [service_name]: list all active keys for this service
service key add [service_name] [key_id]: add a key with id [key_id] for [service_name]. Reads the pubkey from stdin.
service key generate [service_name] [key_id]: create a key with id [key_id] for [service_name]. outputs the private key.
service key delete [service_name] [key_id]: delete key [key_id] for service [service_name]
"
  );
}

fn list_services() {
  let conn = create_connection();
  let services = Service::get_all(&conn).unwrap();
  println!("found {} services", services.len());
  for s in services {
    println!("{}", s.name)
  }
}

fn create_service(name: &str) {
  let conn = create_connection();
  Service::create(&conn, name).unwrap();
  println!("successfully created service {}", name);
}

fn add_key(service_name: &str, key_id: &str) {
  let conn = create_connection();
  let s = Service::get_by_name(&conn, service_name).unwrap();
  let pubkey = read_pubkey();
  s.add_key(&conn, key_id, &pubkey).unwrap();
  println!("successfully added key {} for service {}", key_id, service_name);
}

fn gen_key(service_name: &str, key_id: &str) {
  let conn = create_connection();
  let s = Service::get_by_name(&conn, service_name).unwrap();
  let k = Keypair::generate().unwrap();
  s.add_key(&conn, key_id, &k.as_service_key().public_key).unwrap();
  println!(
        "Successfully generated key {} for service {}, see below for your public key:\n\nBEGIN PRIVATE KEY\n{}\nEND PRIVATE KEY",
        key_id, service_name, k.private_key
    );
}

fn read_pubkey() -> String {
  use std::io::{self, BufRead};
  println!("paste the pubkey for this service:");
  let mut buf = String::new();
  let stdin = io::stdin();
  for line in stdin.lock().lines() {
    let text = line.unwrap();
    buf.push_str(&text);
    buf.push_str("\n");
    if text.contains("END PUBLIC KEY") {
      break;
    }
  }
  buf
}

fn list_keys(service_name: &str) {
  let conn = create_connection();
  let s = Service::get_by_name(&conn, service_name).unwrap();
  let keys = s.get_all_keys(&conn).unwrap();
  println!("got {} keys for service {}", keys.len(), s.name);
  for k in keys {
    println!(
      "{}/{} -- \nBEGIN PUBLIC KEY\n{}\nEND PUBLIC KEY",
      s.name, k.key_id, k.public_key
    )
  }
}

fn delete_key(service_name: &str, key_id: &str) {
  let conn = create_connection();
  let s = Service::get_by_name(&conn, service_name).unwrap();
  s.delete_key(&conn, key_id).unwrap();
  println!("successfully deleted key {} for service {}", key_id, service_name);
}
