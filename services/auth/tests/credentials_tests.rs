mod common;
use actix_web::{test, App};
use authlib::config_api;
use serde_json::{json, Value};

#[actix_rt::test]
async fn test_can_create_credentials() {
  let db = common::test_db();
  let mut app = test::init_service(App::new().configure(|cfg| config_api(cfg, common::static_config(&db)))).await;
  let req = test::TestRequest::post()
    .uri("/credentials")
    .set_json(&json!({
      "password": "xyz 123"
    }))
    .to_request();
  let res: Value = test::read_response_json(&mut app, req).await;
  let cred = res.as_object().unwrap();
  // make sure we get a uuid
  uuid::Uuid::parse_str(cred["uuid"].as_str().unwrap()).unwrap();
  chrono::DateTime::parse_from_rfc3339(cred["created_at"].as_str().unwrap()).unwrap();
  chrono::DateTime::parse_from_rfc3339(cred["updated_at"].as_str().unwrap()).unwrap();
}
