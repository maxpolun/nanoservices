mod common;
use actix_web::{test, App};
use authlib::{config_api, util};
use serde::{Deserialize, Serialize};
use serde_json::{json, Value};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct Credential {
  pub uuid: Uuid,
  pub created_at: util::Timestamp,
  pub updated_at: util::Timestamp,
}

#[actix_rt::test]
async fn test_can_create_refresh_tokens() {
  let db = common::test_db();
  let mut app = test::init_service(
    App::new()
      .wrap(actix_web::middleware::Logger::default())
      .configure(|cfg| config_api(cfg, common::static_config(&db))),
  )
  .await;
  let conn = db.conn();
  let cred = common::create_credential(&conn, "xyz 123");
  let req2 = test::TestRequest::post()
    .uri(&format!("/credentials/{}/refresh_tokens", cred.uuid))
    .set_json(&json!({
      "credential_uuid": cred.uuid,
      "password": "xyz 123"
    }))
    .to_request();

  let res: Value = test::read_response_json(&mut app, req2).await;
  pretty_assertions::assert_eq!(
    res["credential_uuid"].as_str().unwrap(),
    cred.uuid.to_hyphenated_ref().to_string()
  );
}
