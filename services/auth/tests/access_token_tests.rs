mod common;
use actix_web::{test, App};
use authlib::{config_api, models::access_tokens::AccessToken};
use common::{create_credential, create_keypair, create_refresh_token};
use jsonwebtoken::{decode, Algorithm, DecodingKey, Validation};
use serde_json::{json, Value};

#[actix_rt::test]
async fn test_can_create_access_tokens() {
  let db = common::test_db();
  let mut app = test::init_service(App::new().configure(|cfg| config_api(cfg, common::static_config(&db)))).await;
  let conn = db.conn();
  let keypair = create_keypair(&conn);
  let cred = create_credential(&conn, "xyz 123");
  let refresh_token = create_refresh_token(&conn, &cred);

  let req3 = test::TestRequest::post()
    .uri("/access_tokens")
    .set_json(&json!({
      "refresh_token_uuid": refresh_token.uuid
    }))
    .to_request();
  let access_token: Value = test::read_response_json(&mut app, req3).await;
  let jwt = access_token["access_token"].as_str().unwrap();
  let decoded_token = decode::<AccessToken>(
    &jwt,
    &DecodingKey::from_ec_der(&keypair.public_key_der().unwrap()),
    &Validation::new(Algorithm::ES256),
  );
  decoded_token.unwrap();
}
