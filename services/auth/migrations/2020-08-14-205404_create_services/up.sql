-- Your SQL goes here

CREATE TABLE IF NOT EXISTS services (
  name TEXT PRIMARY KEY,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
SELECT diesel_manage_updated_at('services');

CREATE TABLE IF NOT EXISTS service_keys (
  key_id TEXT NOT NULL,
  service_name TEXT NOT NULL REFERENCES services(name),
  public_key TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  PRIMARY KEY (service_name, key_id)
);
SELECT diesel_manage_updated_at('service_keys');
