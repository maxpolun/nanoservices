-- This file should undo anything in `up.sql`
DROP TABLE keypairs;

CREATE TABLE api_keys (
  uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  secret TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW(),
  updated_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
SELECT diesel_manage_updated_at('api_keys');
