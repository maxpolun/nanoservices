-- drop api keys, we don't need it.

DROP TABLE IF EXISTS api_keys;

CREATE TABLE keypairs (
  uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  private_key TEXT NOT NULL,
  public_key TEXT NOT NULL,
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
