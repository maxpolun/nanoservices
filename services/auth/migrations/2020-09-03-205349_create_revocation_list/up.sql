-- Your SQL goes here
CREATE TABLE IF NOT EXISTS revoked_tokens (
  token_id TEXT PRIMARY KEY,
  expiration TIMESTAMPTZ NOT NULL
);
