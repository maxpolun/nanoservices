CREATE TABLE refresh_tokens (
  uuid UUID PRIMARY KEY DEFAULT gen_random_uuid(),
  credential_uuid UUID NOT NULL references credentials(uuid),
  created_at TIMESTAMPTZ NOT NULL DEFAULT NOW()
);
