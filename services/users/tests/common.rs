#![allow(dead_code)]
use diesel::{Connection, PgConnection};
use std::env;
use tracing::debug;
use tracing_subscriber::{fmt, EnvFilter};
use url::Url;

fn db_connstring(dbname: &str, template: &str) -> String {
  let mut parsed = Url::parse(template).unwrap();
  parsed.set_path(dbname);
  parsed.into_string()
}

fn template_db(template: &str) -> String {
  let parsed = Url::parse(template).unwrap();
  parsed.path_segments().unwrap().nth(0).unwrap().to_owned()
}

fn db_name() -> String {
  let uuid = uuid::Uuid::new_v4();
  format!("test_{}", uuid.to_simple())
}

pub struct TestDb {
  template_db: String,
  pub connstring: String,
  db_name: String,
}

impl TestDb {
  fn from_template(template: String) -> Self {
    let conn = PgConnection::establish(&template).unwrap();
    let name = db_name();
    let create_command = format!("CREATE DATABASE {} TEMPLATE {}", name, template_db(&template));
    debug!("executing command \"{}\"", create_command);
    conn.execute(&create_command).unwrap();
    let connstring = db_connstring(&name, &template);
    Self {
      template_db: template,
      db_name: name,
      connstring,
    }
  }

  pub fn conn(&self) -> PgConnection {
    PgConnection::establish(&self.connstring).unwrap()
  }
}

impl Drop for TestDb {
  fn drop(&mut self) {
    let conn = PgConnection::establish(&self.template_db).unwrap();
    let drop_cmd = format!("DROP DATABASE {}", self.db_name);
    debug!("executing command \"{}\"", drop_cmd);
    conn.execute(&drop_cmd).unwrap();
  }
}

pub fn test_db() -> TestDb {
  dotenv::dotenv().ok();
  let subscriber = fmt::Subscriber::builder()
    .with_env_filter(EnvFilter::from_default_env())
    .finish();
  tracing::subscriber::set_global_default(subscriber).expect("setting tracing default failed");
  let template_db = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  TestDb::from_template(template_db)
}

pub fn static_config(db: &TestDb) -> userslib::StaticConfig {
  userslib::StaticConfig {
    database_url: db.connstring.clone(),
  }
}
