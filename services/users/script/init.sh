#!/usr/bin/env bash

set -euo pipefail

createdb -O nanoservices users_dev
createdb -O nanoservices users_test_template

scriptdir="$( cd "$(dirname "$0")" ; pwd -P )"
servicedir="$(dirname scriptdir)"

cp "$servicedir/.env.template" "$servicedir/.env"
