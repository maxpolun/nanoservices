use super::{FakeUserRepository, RealUserRepository};
use crate::schema::*;
use diesel::{prelude::*, result::Error};
use uuid::Uuid;

pub trait DeleteUser {
  fn delete(&self, user_uuid: Uuid) -> Result<(), Error>;
}

impl<'a> DeleteUser for RealUserRepository<'a> {
  fn delete(&self, user_uuid: Uuid) -> Result<(), Error> {
    let res = diesel::delete(users::table)
      .filter(users::uuid.eq(user_uuid))
      .execute(self.conn)?;
    if res == 0 {
      Err(Error::NotFound)
    } else {
      Ok(())
    }
  }
}

impl DeleteUser for FakeUserRepository {
  fn delete(&self, user_uuid: Uuid) -> Result<(), Error> {
    if let Some(pos) = self.users.borrow().iter().position(|u| u.uuid == user_uuid) {
      self.users.borrow_mut().remove(pos);
      Ok(())
    } else {
      return Err(Error::NotFound);
    }
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::util::{prepopulate_users, test_connection};

  #[test]
  fn can_delete_user() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let uuid = Uuid::parse_str("b907cf6e-7fb0-494d-b06b-bcf14cb68000").unwrap();

    repo.delete(uuid).unwrap();
    pretty_assertions::assert_eq!(
      conn
        .execute("SELECT * FROM users WHERE uuid = 'b907cf6e-7fb0-494d-b06b-bcf14cb68000'")
        .unwrap(),
      0
    )
  }

  #[test]
  fn delete_user_errors_if_user_does_not_exist() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);
    let uuid = Uuid::parse_str("b907cf6e-7fb0-494d-b06b-bcf14cb68000").unwrap();

    assert!(repo.delete(uuid).is_err());
  }
}
