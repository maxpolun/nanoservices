use super::{FakeUserRepository, RealUserRepository, User};
use crate::schema::*;
use diesel::prelude::*;
use serde::{Deserialize, Serialize};
use std::error::Error;
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize, Clone)]
pub enum SortCol {
  #[serde(rename = "c")]
  CreatedAt,
}

#[derive(Debug, Serialize, Deserialize, Clone, PartialEq, Eq)]
pub enum SortDir {
  #[serde(rename = "a")]
  Ascending,
  #[serde(rename = "d")]
  Decending,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct Sort {
  col: SortCol,
  dir: SortDir,
}

#[derive(Debug, Serialize, Deserialize, Clone)]
pub struct UserCursor {
  user_uuid: Option<Uuid>,
  sort: Sort,
  page_size: usize,
}

impl UserCursor {
  pub fn as_b64(&self) -> Result<String, Box<dyn Error>> {
    let bytes = serde_json::to_vec(self).map_err(Box::new)?;
    Ok(base64::encode(&bytes))
  }

  pub fn from_b64(s: &str) -> Result<Self, Box<dyn Error>> {
    let bytes = base64::decode(s)?;
    Ok(serde_json::from_slice(&bytes)?)
  }
}

impl Default for UserCursor {
  fn default() -> Self {
    Self {
      user_uuid: None,
      sort: Sort {
        col: SortCol::CreatedAt,
        dir: SortDir::Ascending,
      },
      page_size: 10,
    }
  }
}

#[derive(Debug)]
pub struct UserIndexData {
  pub items: Vec<User>,
  pub cursor: UserCursor,
  pub total_items: i64,
}

pub trait IndexUser {
  fn index(&self, cursor: &UserCursor) -> Result<UserIndexData, diesel::result::Error>;
}

impl<'a> IndexUser for RealUserRepository<'a> {
  fn index(&self, cursor: &UserCursor) -> Result<UserIndexData, diesel::result::Error> {
    let mut query = users::table.limit(cursor.page_size as i64).into_boxed();
    if let Some(user_uuid) = cursor.user_uuid {
      let user = users::table.find(user_uuid).get_result::<User>(self.conn)?;
      if cursor.sort.dir == SortDir::Ascending {
        query = query.filter(users::created_at.gt(user.created_at));
      } else {
        query = query.filter(users::created_at.lt(user.created_at));
      }
    };
    if cursor.sort.dir == SortDir::Ascending {
      query = query.order(users::created_at.asc())
    } else {
      query = query.order(users::created_at.desc())
    };
    let u = query.get_results(self.conn)?;
    let mut new_cursor = cursor.clone();
    new_cursor.user_uuid = u.iter().last().map(|user: &User| user.uuid);
    let len = users::table.count().get_result(self.conn)?;

    Ok(UserIndexData {
      items: u,
      cursor: new_cursor,
      total_items: len,
    })
  }
}

impl IndexUser for FakeUserRepository {
  fn index(&self, cursor: &UserCursor) -> Result<UserIndexData, diesel::result::Error> {
    self.users.borrow_mut().sort_unstable_by_key(|u| u.created_at);
    if cursor.sort.dir == SortDir::Decending {
      self.users.borrow_mut().reverse();
    }
    let mut new_cursor = cursor.clone();

    let start_index = match cursor.user_uuid {
      None => 0,
      Some(uuid) => self.users.borrow().iter().position(|u| u.uuid == uuid).unwrap_or(0),
    };

    let results = self.users.borrow()[start_index..start_index + cursor.page_size]
      .iter()
      .cloned()
      .collect::<Vec<User>>();
    new_cursor.user_uuid = results.last().map(|u| u.uuid);
    Ok(UserIndexData {
      items: results,
      cursor: new_cursor,
      total_items: self.users.borrow().len() as i64,
    })
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::util::{prepopulate_users, test_connection};

  #[test]
  fn index_gets_page_size_users() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let mut cursor = Default::default();
    pretty_assertions::assert_eq!(repo.index(&cursor).unwrap().items.len(), 10);
    cursor.page_size = 25;
    pretty_assertions::assert_eq!(repo.index(&cursor).unwrap().items.len(), 25);
  }

  #[test]
  fn index_sorts_by_created_at() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let mut cursor = Default::default();
    let output = repo.index(&cursor).unwrap();
    assert!(output.items[0].created_at < output.items[1].created_at);
    cursor.sort.dir = SortDir::Decending;
    let output = repo.index(&cursor).unwrap();
    assert!(output.items[0].created_at > output.items[1].created_at);
  }

  #[test]
  fn index_will_fetch_starting_at_a_user_uuid_if_given() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);

    let mut cursor: UserCursor = Default::default();
    cursor.page_size = 1;
    let old_output = repo.index(&cursor).unwrap();
    let mut output = repo.index(&old_output.cursor).unwrap();
    pretty_assertions::assert_eq!(
      output.items[0].credential_uuid,
      Uuid::parse_str("7d48abd0-c477-4af4-a98c-802c8c8d8001").unwrap()
    );
    output.cursor.sort.dir = SortDir::Decending;
    let output = repo.index(&output.cursor).unwrap();
    pretty_assertions::assert_eq!(
      output.items[0].credential_uuid,
      Uuid::parse_str("7d48abd0-c477-4af4-a98c-802c8c8d8000").unwrap()
    );
  }
}
