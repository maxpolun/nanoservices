use crate::util::Timestamp;
use diesel::PgConnection;
use serde::{Deserialize, Serialize};
use std::{cell::RefCell, rc::Rc};
use uuid::Uuid;

#[derive(Debug, Queryable, Clone)]
pub struct User {
  pub uuid: Uuid,
  pub credential_uuid: Uuid,
  pub email: String,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
  pub verified_at: Option<Timestamp>,
}

impl User {
  pub fn into_record(self) -> UserRecord {
    UserRecord {
      uuid: self.uuid,
      credential_uuid: self.credential_uuid,
      email: self.email,
      email_verified: self.verified_at.is_some(),
      created_at: self.created_at,
      updated_at: self.updated_at,
    }
  }
}

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct UserRecord {
  pub uuid: Uuid,
  pub credential_uuid: Uuid,
  pub email: String,
  pub email_verified: bool,
  pub created_at: Timestamp,
  pub updated_at: Timestamp,
}

pub struct RealUserRepository<'a> {
  pub(crate) conn: &'a PgConnection,
}

impl<'a> RealUserRepository<'a> {
  pub fn new(conn: &'a PgConnection) -> Self {
    Self { conn }
  }
}

pub fn email_is_valid(email: &str) -> bool {
  email.matches('@').count() == 1
}

#[derive(Debug)]
pub struct FakeUserRepository {
  pub(crate) users: Rc<RefCell<Vec<User>>>,
}

impl FakeUserRepository {
  #[cfg(test)]
  pub fn new() -> Self {
    Self {
      users: Rc::new(RefCell::new(vec![])),
    }
  }
}
