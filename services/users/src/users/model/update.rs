use super::{types::email_is_valid, FakeUserRepository, RealUserRepository, User};
use crate::schema::*;
use diesel::prelude::*;
use thiserror::Error;
use uuid::Uuid;

#[derive(Debug, Error, PartialEq)]
pub enum UpdateUserError {
  #[error("Invalid Email")]
  InvalidEmail,
  #[error("Duplicate Email")]
  DuplicateEmail,
  #[error("Other database error")]
  DbError(diesel::result::Error),
}

#[derive(Debug, AsChangeset)]
#[table_name = "users"]
#[primary_key("uuid")]
pub struct UpdateUserFields<'a> {
  pub uuid: Uuid,
  pub email: Option<&'a str>,
}

impl From<diesel::result::Error> for UpdateUserError {
  fn from(e: diesel::result::Error) -> UpdateUserError {
    use diesel::result::{DatabaseErrorKind, Error};
    if let Error::DatabaseError(DatabaseErrorKind::UniqueViolation, info) = &e {
      if let Some(col) = info.constraint_name() {
        if col == "users_email_key" {
          return UpdateUserError::DuplicateEmail;
        }
      }
    }
    UpdateUserError::DbError(e)
  }
}

pub trait UpdateUser {
  fn update(&self, fields: UpdateUserFields) -> Result<User, UpdateUserError>;
}

impl UpdateUser for RealUserRepository<'_> {
  fn update(&self, fields: UpdateUserFields) -> Result<User, UpdateUserError> {
    if let Some(email) = fields.email {
      if !email_is_valid(email) {
        return Err(UpdateUserError::InvalidEmail);
      }
    }

    Ok(diesel::update(users::table).set(&fields).get_result(self.conn)?)
  }
}

impl UpdateUser for FakeUserRepository {
  fn update(&self, fields: UpdateUserFields) -> Result<User, UpdateUserError> {
    if let Some(email) = fields.email {
      if !email_is_valid(email) {
        return Err(UpdateUserError::InvalidEmail);
      }
    }

    let mut users = self.users.borrow_mut();
    let u = users
      .iter_mut()
      .find(|u| u.uuid == fields.uuid)
      .ok_or(diesel::result::Error::NotFound)?;

    if let Some(email) = fields.email {
      u.email = email.to_owned();
    }
    Ok(u.clone())
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::util::test_connection;

  #[test]
  fn can_update() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);

    conn.execute("INSERT INTO USERS (uuid, credential_uuid, email) VALUES ('56f1b698-a73b-43fd-bf8f-2c6e051df8e4', '5d1a30e9-0d30-47e7-84cb-4a2146bb177c', 'test@example.com');").unwrap();
    let u = repo
      .update(UpdateUserFields {
        uuid: Uuid::parse_str("56f1b698-a73b-43fd-bf8f-2c6e051df8e4").unwrap(),
        email: Some("test2@example.com"),
      })
      .unwrap();

    pretty_assertions::assert_eq!(u.email, "test2@example.com");
  }

  #[test]
  fn update_with_none_works_but_no_change() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);

    conn.execute("INSERT INTO USERS (uuid, credential_uuid, email) VALUES ('56f1b698-a73b-43fd-bf8f-2c6e051df8e4', '5d1a30e9-0d30-47e7-84cb-4a2146bb177c', 'test@example.com');").unwrap();
    let u = repo
      .update(UpdateUserFields {
        uuid: Uuid::parse_str("56f1b698-a73b-43fd-bf8f-2c6e051df8e4").unwrap(),
        email: None,
      })
      .unwrap();

    pretty_assertions::assert_eq!(u.email, "test@example.com");
  }

  #[test]
  fn update_fails_with_bad_email() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);

    conn.execute("INSERT INTO USERS (uuid, credential_uuid, email) VALUES ('56f1b698-a73b-43fd-bf8f-2c6e051df8e4', '5d1a30e9-0d30-47e7-84cb-4a2146bb177c', 'test@example.com');").unwrap();
    let u = repo.update(UpdateUserFields {
      uuid: Uuid::parse_str("56f1b698-a73b-43fd-bf8f-2c6e051df8e4").unwrap(),
      email: Some("test2example.com"),
    });
    pretty_assertions::assert_eq!(u.unwrap_err(), UpdateUserError::InvalidEmail)
  }

  #[test]
  fn update_fails_with_missing_user() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);

    let u = repo.update(UpdateUserFields {
      uuid: Uuid::parse_str("56f1b698-a73b-43fd-bf8f-2c6e051df8e4").unwrap(),
      email: Some("test2@example.com"),
    });

    pretty_assertions::assert_eq!(
      u.unwrap_err(),
      UpdateUserError::DbError(diesel::result::Error::NotFound)
    )
  }
}
