use super::{types::email_is_valid, FakeUserRepository, RealUserRepository, User};
use crate::schema::*;
use chrono::Utc;
use diesel::prelude::*;
use serde::Deserialize;
use thiserror::Error;
use uuid::Uuid;

#[derive(Debug, Insertable, Clone, Deserialize)]
#[table_name = "users"]
pub struct NewUser<'a> {
  pub credential_uuid: Uuid,
  pub email: &'a str,
}

#[derive(Debug, Error)]
pub enum CreateUserError {
  #[error("Invalid Email")]
  InvalidEmail,
  #[error("Duplicate Credential UUID")]
  DuplicateCredential,
  #[error("Duplicate Email")]
  DuplicateEmail,
  #[error("Other database error")]
  DbError(diesel::result::Error),
}

impl From<diesel::result::Error> for CreateUserError {
  fn from(e: diesel::result::Error) -> CreateUserError {
    use diesel::result::{DatabaseErrorKind, Error};
    match &e {
      Error::DatabaseError(DatabaseErrorKind::UniqueViolation, info) => {
        if let Some(col) = info.constraint_name() {
          if col == "users_email_key" {
            CreateUserError::DuplicateEmail
          } else if col == "users_credential_uuid_key" {
            CreateUserError::DuplicateCredential
          } else {
            CreateUserError::DbError(e)
          }
        } else {
          CreateUserError::DbError(e)
        }
      }
      _ => CreateUserError::DbError(e),
    }
  }
}

pub trait CreateUser {
  fn create(&self, credential_uuid: Uuid, email: &str) -> Result<User, CreateUserError>;
}

impl<'a> CreateUser for RealUserRepository<'a> {
  fn create(&self, credential_uuid: Uuid, email: &str) -> Result<User, CreateUserError> {
    if !email_is_valid(email) {
      return Err(CreateUserError::InvalidEmail);
    };
    Ok(
      diesel::insert_into(users::table)
        .values(NewUser { credential_uuid, email })
        .get_result::<User>(self.conn)?,
    )
  }
}

impl CreateUser for FakeUserRepository {
  fn create(&self, credential_uuid: Uuid, email: &str) -> Result<User, CreateUserError> {
    if !email_is_valid(email) {
      return Err(CreateUserError::InvalidEmail);
    };
    if self.users.borrow().iter().find(|u| u.email == email).is_some() {
      return Err(CreateUserError::DuplicateEmail);
    }
    if self
      .users
      .borrow()
      .iter()
      .find(|u| u.credential_uuid == credential_uuid)
      .is_some()
    {
      return Err(CreateUserError::DuplicateCredential);
    }
    let u = User {
      uuid: Uuid::new_v4(),
      credential_uuid,
      email: email.to_owned(),
      created_at: Utc::now(),
      updated_at: Utc::now(),
      verified_at: None,
    };
    self.users.borrow_mut().push(u.clone());
    Ok(u)
  }
}

#[cfg(test)]
mod tests {
  use super::*;
  use crate::util::test_connection;
  #[test]
  fn can_create_user() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);
    let user = repo.create(Uuid::new_v4(), "test@example.com").unwrap();
    pretty_assertions::assert_eq!(user.email, "test@example.com");
  }

  #[test]
  fn create_requires_valid_email() {
    let conn = test_connection();
    let repo = RealUserRepository::new(&conn);
    for email in &["", "noatsign"] {
      assert!(repo.create(Uuid::new_v4(), email).is_err());
    }
  }
}
