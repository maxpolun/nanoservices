use super::{FakeUserRepository, RealUserRepository, User};
use crate::schema::*;
use diesel::{prelude::*, result::Error};
use std::collections::HashMap;
use uuid::Uuid;

pub trait GetUsersByUuid {
  fn get_by_uuids(&self, uuids: &[Uuid]) -> Result<HashMap<Uuid, Option<User>>, Error>;
}

impl<'a> GetUsersByUuid for RealUserRepository<'a> {
  fn get_by_uuids(&self, uuids: &[Uuid]) -> Result<HashMap<Uuid, Option<User>>, Error> {
    let mut query = users::table.into_boxed();
    for uuid in uuids {
      query = query.or_filter(users::uuid.eq(uuid))
    }
    let results = query.get_results::<User>(self.conn)?;

    Ok(
      uuids
        .iter()
        .map(|uuid| (uuid.clone(), results.iter().cloned().find(|u| u.uuid == *uuid)))
        .collect::<HashMap<_, _>>(),
    )
  }
}

impl GetUsersByUuid for FakeUserRepository {
  fn get_by_uuids(&self, uuids: &[Uuid]) -> Result<HashMap<Uuid, Option<User>>, Error> {
    let users = self.users.borrow();
    let results = uuids
      .iter()
      .map(|uuid| (uuid.clone(), users.iter().find(|u| u.uuid == *uuid).cloned()))
      .collect::<HashMap<_, _>>();
    return Ok(results);
  }
}

#[cfg(test)]
mod test {
  use super::*;
  use crate::util::{prepopulate_users, test_connection};

  #[test]
  fn can_get_a_user_by_uuid() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let uuid = Uuid::parse_str("b907cf6e-7fb0-494d-b06b-bcf14cb68000").unwrap();

    let users = repo.get_by_uuids(&[uuid]).unwrap();
    pretty_assertions::assert_eq!(users[&uuid].as_ref().unwrap().uuid, uuid)
  }

  #[test]
  fn can_get_many_users_by_uuid() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let mut uuids = Vec::new();
    for i in 0..100 {
      uuids.push(Uuid::parse_str(&format!("b907cf6e-7fb0-494d-b06b-bcf14cb68{:03}", i)).unwrap());
    }

    let users = repo.get_by_uuids(&uuids).unwrap();
    assert!(users.values().all(|u| u.is_some()));
    pretty_assertions::assert_eq!(users.len(), 100);
  }

  #[test]
  fn can_handle_missing_uuids() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let uuids = [
      Uuid::parse_str("b907cf6e-7fb0-494d-b06b-bcf14cb68007").unwrap(),
      Uuid::parse_str("c907cf6e-7fb0-494d-b06b-bcf14cb68007").unwrap(),
      Uuid::parse_str("b907cf6e-7fb0-494d-b06b-bcf14cb68003").unwrap(),
    ];
    let users = repo.get_by_uuids(&uuids).unwrap();

    assert!(users[&uuids[0]].is_some());
    assert!(users[&uuids[1]].is_none());
    assert!(users[&uuids[2]].is_some());
  }

  #[test]
  fn can_handle_no_uuids_passed_in() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let users = repo.get_by_uuids(&[]).unwrap();

    pretty_assertions::assert_eq!(users.len(), 0);
  }

  #[test]
  fn can_handle_all_missing() {
    let conn = test_connection();
    prepopulate_users(&conn);
    let repo = RealUserRepository::new(&conn);
    let uuid = Uuid::parse_str("c907cf6e-7fb0-494d-b06b-bcf14cb68007").unwrap();
    let users = repo.get_by_uuids(&[uuid]).unwrap();

    assert!(users[&uuid].is_none());
  }
}
