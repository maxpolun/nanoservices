mod create;
mod delete;
mod get;
mod index;
mod types;
mod update;

pub use create::{CreateUser, CreateUserError, NewUser};
pub use delete::DeleteUser;
pub use get::GetUsersByUuid;
pub use index::{IndexUser, UserCursor, UserIndexData};
pub use types::{FakeUserRepository, RealUserRepository, User, UserRecord};
pub use update::{UpdateUser, UpdateUserError, UpdateUserFields};

pub trait UserRepository: CreateUser + IndexUser + GetUsersByUuid + DeleteUser + UpdateUser {}

impl<'a> UserRepository for RealUserRepository<'a> {}
impl UserRepository for FakeUserRepository {}
