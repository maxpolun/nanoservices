use super::model::{
  CreateUserError, DeleteUser, GetUsersByUuid, UpdateUser, UpdateUserError, UpdateUserFields, UserIndexData,
};
use crate::{
  users::model::{RealUserRepository, UserCursor, UserRecord, UserRepository},
  Pool,
};
use actix_web::{
  error::{self, ErrorConflict, ErrorInternalServerError, ErrorUnprocessableEntity},
  web, HttpResponse, Result,
};
use diesel::NotFound;
use serde::{Deserialize, Serialize};
use std::collections::HashMap;
use uuid::Uuid;

#[derive(Debug, Deserialize)]
pub struct CreateUserRequest {
  credential_uuid: Uuid,
  email: String,
}

pub async fn create_user(pool: web::Data<Pool>, req_body: web::Json<CreateUserRequest>) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  create_user_impl(&user_repo, req_body.into_inner()).await
}

pub(crate) async fn create_user_impl(
  user_repo: &impl UserRepository,
  new_user: CreateUserRequest,
) -> Result<HttpResponse> {
  let user = user_repo
    .create(new_user.credential_uuid, &new_user.email)
    .map_err(|e| match e {
      CreateUserError::InvalidEmail => ErrorUnprocessableEntity(e),
      CreateUserError::DuplicateEmail | CreateUserError::DuplicateCredential => ErrorConflict(e),
      CreateUserError::DbError(e) => ErrorInternalServerError(e),
    })?;
  Ok(HttpResponse::Ok().json(user.into_record()))
}

#[cfg(test)]
mod test_create {
  use super::*;
  use crate::{
    users::model::{CreateUser, FakeUserRepository, UserRecord},
    util::parse_http_response,
  };

  #[actix_rt::test]
  async fn create_creates_a_user() {
    let repo = FakeUserRepository::new();
    create_user_impl(
      &repo,
      CreateUserRequest {
        credential_uuid: Uuid::new_v4(),
        email: "test@example.com".to_owned(),
      },
    )
    .await
    .unwrap();
    pretty_assertions::assert_eq!(repo.users.borrow().len(), 1);
  }

  #[actix_rt::test]
  async fn create_returns_a_user() {
    let repo = FakeUserRepository::new();
    let response = create_user_impl(
      &repo,
      CreateUserRequest {
        credential_uuid: Uuid::new_v4(),
        email: "test@example.com".to_owned(),
      },
    )
    .await
    .unwrap();

    let u = parse_http_response::<UserRecord>(&response);
    pretty_assertions::assert_eq!(u.email, "test@example.com");
  }

  #[actix_rt::test]
  async fn bad_email_gives_422() {
    let repo = FakeUserRepository::new();
    let response = create_user_impl(
      &repo,
      CreateUserRequest {
        credential_uuid: Uuid::new_v4(),
        email: "toatsign".to_owned(),
      },
    )
    .await;
    pretty_assertions::assert_eq!(response.unwrap_err().as_response_error().status_code(), 422);
  }

  #[actix_rt::test]
  async fn dupe_email_gives_409() {
    let repo = FakeUserRepository::new();
    repo.create(Uuid::new_v4(), "test@example.com").unwrap();
    let response = create_user_impl(
      &repo,
      CreateUserRequest {
        credential_uuid: Uuid::new_v4(),
        email: "test@example.com".to_owned(),
      },
    )
    .await;
    pretty_assertions::assert_eq!(response.unwrap_err().as_response_error().status_code(), 409);
  }

  #[actix_rt::test]
  async fn dupe_cred_gives_409() {
    let repo = FakeUserRepository::new();
    let uuid = Uuid::new_v4();
    repo.create(uuid, "test1@example.com").unwrap();
    let response = create_user_impl(
      &repo,
      CreateUserRequest {
        credential_uuid: uuid,
        email: "test2@example.com".to_owned(),
      },
    )
    .await;
    pretty_assertions::assert_eq!(response.unwrap_err().as_response_error().status_code(), 409);
  }
}

#[derive(Debug, Deserialize)]
pub struct IndexQueryParams {
  cursor: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct IndexResults {
  cursor: String,
  items: Vec<UserRecord>,
  total_items: i64,
}

pub async fn index_users(pool: web::Data<Pool>, query: web::Query<IndexQueryParams>) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  index_users_impl(&user_repo, query.into_inner()).await
}

pub async fn index_users_impl(user_repo: &impl UserRepository, query: IndexQueryParams) -> Result<HttpResponse> {
  let old_cursor = query
    .cursor
    .map(|c| UserCursor::from_b64(&c).unwrap_or_default())
    .unwrap_or_default();
  let UserIndexData {
    items,
    cursor,
    total_items,
  } = user_repo
    .index(&old_cursor)
    .map_err(|e| error::ErrorInternalServerError(e))?;
  Ok(HttpResponse::Ok().json(IndexResults {
    cursor: cursor.as_b64().map_err(|e| error::ErrorInternalServerError(e))?,
    items: items.iter().cloned().map(|user| user.into_record()).collect(),
    total_items,
  }))
}
#[cfg(test)]
mod test_index {

  use super::*;
  use crate::{
    users::model::{FakeUserRepository, User},
    util::parse_http_response,
  };
  use chrono::{TimeZone, Utc};

  fn test_users(repo: &FakeUserRepository) {
    let mut v = Vec::new();
    for i in 0..100 {
      v.push(User {
        uuid: Uuid::parse_str(&format!("857d3389-f9b5-484c-b2a5-155db538b{:03}", i)).unwrap(),
        credential_uuid: Uuid::parse_str(&format!("4da2ad38-9922-4be4-9472-cc023b4dc{:03}", i)).unwrap(),
        email: format!("test{}@example.com", i),
        verified_at: None,
        created_at: Utc.timestamp_millis(i * 1000),
        updated_at: Utc.timestamp_millis(i * 1000),
      })
    }
    repo.users.borrow_mut().extend_from_slice(&v);
  }

  #[actix_rt::test]
  async fn index_gets_10_items_by_default() {
    let repo = FakeUserRepository::new();
    test_users(&repo);
    let response = index_users_impl(&repo, IndexQueryParams { cursor: None })
      .await
      .unwrap();
    let users = parse_http_response::<IndexResults>(&response);
    pretty_assertions::assert_eq!(users.items.len(), 10);
  }
}

pub async fn get_user_by_uuid(pool: web::Data<Pool>, user_uuid: web::Path<Uuid>) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  let result = user_repo
    .get_by_uuids(&[user_uuid.into_inner()])
    .map_err(|e| error::ErrorInternalServerError(e))?;
  match result.values().next().cloned().flatten() {
    Some(user) => Ok(HttpResponse::Ok().json(user.into_record())),
    None => Ok(HttpResponse::NotFound().finish()),
  }
}

pub async fn delete_user(pool: web::Data<Pool>, user_uuid: web::Path<Uuid>) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  match user_repo.delete(*user_uuid) {
    Ok(_) => Ok(HttpResponse::NoContent().finish()),
    Err(diesel::result::Error::NotFound) => Ok(HttpResponse::NotFound().finish()),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}

pub async fn users_report(pool: web::Data<Pool>, report_uuids: web::Json<Vec<Uuid>>) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  let uuids = report_uuids.into_inner();
  let results = user_repo
    .get_by_uuids(&uuids)
    .map_err(|e| error::ErrorInternalServerError(e))?;
  Ok(
    HttpResponse::Ok().json(
      results
        .iter()
        .map(|(uuid, u_opt)| (uuid, u_opt.clone().map(|u| u.into_record())))
        .collect::<HashMap<_, _>>(),
    ),
  )
}

#[derive(Debug, Deserialize)]
pub struct UserPatch {
  email: Option<String>,
}

pub async fn patch_user(
  pool: web::Data<Pool>,
  user_uuid: web::Path<Uuid>,
  patch: web::Json<UserPatch>,
) -> Result<HttpResponse> {
  let conn = pool.get().map_err(|e| error::ErrorInternalServerError(e))?;
  let user_repo = RealUserRepository::new(&conn);
  let patch = patch.into_inner();

  match user_repo.update(UpdateUserFields {
    uuid: user_uuid.into_inner(),
    email: patch.email.as_deref(),
  }) {
    Ok(user) => Ok(HttpResponse::Ok().json(user.into_record())),
    Err(UpdateUserError::DbError(NotFound)) => Ok(HttpResponse::NotFound().finish()),
    Err(_) => Ok(HttpResponse::InternalServerError().finish()),
  }
}
