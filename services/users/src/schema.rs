table! {
    users (uuid) {
        uuid -> Uuid,
        credential_uuid -> Uuid,
        email -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
        verified_at -> Nullable<Timestamptz>,
    }
}
