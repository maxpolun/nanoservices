#[cfg(test)]
use actix_web::{dev::Body, HttpResponse};
#[cfg(test)]
use diesel::{pg::PgConnection, Connection};
#[cfg(test)]
use serde::Deserialize;
#[cfg(test)]
use std::env;

#[cfg(test)]
pub fn test_connection() -> PgConnection {
  dotenv::dotenv().ok();
  let database_url = env::var("TEST_DB_TEMPLATE").expect("TEST_DB_TEMPLATE must be set");
  let conn = PgConnection::establish(&database_url).expect(&format!("Error connecting to {}", database_url));
  conn.begin_test_transaction().unwrap();
  conn
}

pub type Timestamp = chrono::DateTime<chrono::Utc>;

#[cfg(test)]
pub fn parse_http_response<'a, T: Deserialize<'a>>(response: &'a HttpResponse) -> T {
  let body = response.body();
  let bytes = match body.as_ref().unwrap() {
    Body::Bytes(b) => b,
    _ => panic!("expected bytes"),
  };
  serde_json::from_slice(bytes.as_ref()).unwrap()
}

#[cfg(test)]
pub fn prepopulate_users(conn: &PgConnection) {
  use diesel::connection::SimpleConnection;
  let mut query = String::new();
  for i in 0..100 {
    query += &format!(
      "INSERT INTO users
            (uuid, credential_uuid, email, created_at)
        VALUES
            ('b907cf6e-7fb0-494d-b06b-bcf14cb68{:03}',
            '7d48abd0-c477-4af4-a98c-802c8c8d8{:03}',
            'test_{}@example.com',
            'J{}');",
      i,
      i,
      i,
      2446225 + i
    );
  }
  conn.batch_execute(&query).unwrap();
}
