use actix_auth_service::JwtVerify;
use actix_web::{App, HttpServer};
use dotenv::dotenv;
use userslib::{config_api, StaticConfig};

#[actix_rt::main]
async fn main() {
  dotenv().ok();
  let _uninstall_tracer = nano_trace::init();
  let verify = JwtVerify::default();
  HttpServer::new(move || {
    App::new()
      .wrap(verify.clone())
      .wrap(nano_trace::TracingLogger)
      .wrap(nano_trace::RequestTracing::new())
      .configure(|cfg| {
        let database_url = std::env::var("DATABASE_URL").expect("DATABASE_URL");
        config_api(cfg, StaticConfig { database_url })
      })
  })
  .bind((
    "127.0.0.1",
    std::env::var("PORT")
      .and_then(|port_str| port_str.parse::<u16>().map_err(|_| std::env::VarError::NotPresent))
      .unwrap_or(8501),
  ))
  .unwrap()
  .run()
  .await
  .unwrap();
}
