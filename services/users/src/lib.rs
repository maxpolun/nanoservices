#[macro_use]
extern crate diesel;
use actix_web::web;
use diesel::{
  r2d2::{self, ConnectionManager},
  PgConnection,
};

use crate::users::handlers::{create_user, delete_user, get_user_by_uuid, index_users, patch_user, users_report};

mod schema;
mod users;
mod util;

type Pool = r2d2::Pool<ConnectionManager<PgConnection>>;

#[derive(Debug, Clone)]
pub struct StaticConfig {
  pub database_url: String,
}

pub fn config_api(cfg: &mut web::ServiceConfig, static_config: StaticConfig) {
  let manager = ConnectionManager::<PgConnection>::new(&static_config.database_url);
  let pool = r2d2::Pool::builder().build(manager).expect("Failed to create pool.");
  cfg.service(
    web::scope("/").data(pool).data(static_config).service(
      web::scope("/users")
        .route("", web::post().to(create_user))
        .route("", web::get().to(index_users))
        .route("/report", web::post().to(users_report))
        .service(
          web::scope("/{user_uuid}")
            .route("", web::get().to(get_user_by_uuid))
            .route("", web::patch().to(patch_user))
            .route("", web::delete().to(delete_user)),
        ),
    ),
  );
}
