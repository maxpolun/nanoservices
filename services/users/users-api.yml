openapi: '3.0.2'
info:
  title: Disco
  version: '1.0'
servers:
  - url: https://users.maxpolun.com

components:
  securitySchemes:
    jwt:
      type: http
      scheme: bearer
      bearerFormat: JWT
  schemas:
    User:
      type: object
      properties:
        uuid:
          type: string
          format: uuid
        credential_uuid:
          type: string
          format: uuid
        email:
          type: string
          format: email
        email_verified:
          type: boolean
        created_at:
          type: string
          format: date-time
        updated_at:
          type: string
          format: date-time
    NewUser:
      type: object
      properties:
        credential_uuid:
          type: string
          format: uuid
        email:
          type: string
          format: email
    ActivationCode:
      type: object
      properties:
        code:
          type: string
        created_at:
          type: string
          format: date-time
        expires_at:
          type: string
          format: date-time

paths:
  /users:
    post:
      operationId: users.create
      description: create a user
      requestBody:
        required: true
        content:
          application/json:
            schema:
              $ref: '#/components/schemas/NewUser'
      responses:
        200:
          description: successfully created user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        422:
          description: invalid credential_uuid or email is in the incorrect format
    get:
      operationId: users.index
      description: get a list of users
      parameters:
        - name: cursor
          in: query
          description: pagination cursor
          schema:
            type: string
        - name: verified
          in: query
          description: true means return only verified, false means return only unverified. null or not given means return both
          schema:
            type: boolean
      responses:
        200:
          description: successfully fetched users
          content:
            application/json:
              schema:
                type: object
                properties:
                  cursor:
                    type: string
                  total_items:
                    type: number
                  items:
                    type: array
                    items:
                      $ref: '#/components/schemas/User'
  /users/report:
    post:
      operationId: users.report
      description: get users by uuid
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: array
              items:
                type: string
                format: uuid
      responses:
        200:
          description: successfully got at least one of the users requested
          content:
            application/json:
              schema:
                type: object
                additionalProperties:
                  $ref: '#/components/schemas/User'
  /users/{user_uuid}:
    get:
      operationId: users.show
      description: get a single user by uuid
      parameters:
        - name: user_uuid
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        200:
          description: found user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
        404:
          description: no user found
    patch:
      operationId: users.patch
      description: update a user
      parameters:
        - name: user_uuid
          in: path
          required: true
          schema:
            type: string
            format: uuid
      requestBody:
        required: true
        content:
          application/json:
            schema:
              type: object
              properties:
                email:
                  type: string
                  format: email
      responses:
        200:
          description: successfully updated user
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/User'
    delete:
        operationId: users.delete
        description: delete a user
        parameters:
          - name: user_uuid
            in: path
            required: true
            schema:
              type: string
              format: uuid
        responses:
          200:
            description: successfully deleted a user
  /users/{user_uuid}/activation_codes:
    post:
      operationId: users.create_activation_code
      description: create an activation code for a user
      parameters:
        - name: user_uuid
          in: path
          required: true
          schema:
            type: string
            format: uuid
      responses:
        200:
          description: successfully created activation code
          content:
            application/json:
              schema:
                $ref: '#/components/schemas/ActivationCode'
  /users/activate/{code}:
    post:
      operationId: users.activate
      description: activate a user with the activation code
      parameters:
        - name: code
          in: path
          required: true
          schema:
            type: string
      responses:
        200:
          description: successfully activated user
        410:
          description: activation code is expired or invalid
security:
  - jwt: []
