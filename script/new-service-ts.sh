#!/usr/bin/env bash

set -euo pipefail

npm init -y
npm install --save koa koa-body koa-router pg
npm install --save-dev typescript typesync jest ts-jest
./node_modules/.bin/typesync
npm install

./node_modules/.bin/ts-jest config:init
