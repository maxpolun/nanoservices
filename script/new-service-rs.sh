#!/usr/bin/env bash

set -euo pipefail

cargo init --vcs none --bin
cargo add actix-web chrono diesel dotenv serde serde_json log r2d2
cargo add --dev actix-http-test
