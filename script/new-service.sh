#!/usr/bin/env bash

set -euo pipefail

if [ $# -lt 1 -o $# -gt 2 ]
then
  echo "new-service.sh service-name [stack]"
fi

name="$1"
stack="${2:-ts}"

scriptdir="$( cd "$(dirname "$0")" ; pwd -P )"
root="$(dirname $scriptdir)"
servicedir="$root/services/$name"

echo "CREATING $servicedir"

mkdir "$servicedir"
cd "$servicedir"

exec bash "$root/script/new-service-${stack}.sh" "$name"
