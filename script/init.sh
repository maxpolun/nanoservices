#!/usr/bin/env bash

set -euo pipefail

echo "Creating user -- when prompted, set password to 'nanoservices'"
createuser -s -P nanoservices
